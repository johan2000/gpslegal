const casos = require("../models/casos.model.js");

exports.dashUser = (req, res) => {
  casos.dashUser(req.body, (err, data) => {
    console.log(req.body)
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: 'elemento no encontrado'
        });
      } else {
        res.status(500).send({
          message: 'error al encontrar la consulta'
        });
      }
    } else res.send(data);
  });
};

exports.listCbo = (req, res) => {
  casos.listCbo(req.body, (err, data) => {
    console.log(req.body)
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: 'elemento no encontrado'
        });
      } else {
        res.status(500).send({
          message: 'error al encontrar la consulta'
        });
      }
    } else res.send(data);
  });
};

exports.listGeneral = (req, res) => {
  casos.listGeneral(req.body, (err, data) => {
    console.log(req.body)
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: 'elemento no encontrado'
        });
      } else {
        res.status(500).send({
          message: 'error al encontrar la consulta'
        });
      }
    } else res.send(data);
  });
};

