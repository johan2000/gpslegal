const Maestros = require("../models/maestros.model.js");

exports.buscarPorId = (req, res) => {
  Maestros.buscarPorId(req.body, (err, data) => {
    console.log(req.body)
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Maestro no encontrado con el id: ${req.body.id}.`
        });
      } else {
        res.status(500).send({
          message: "error al buscar información del id: " + req.body.id
        });
      }
    } else res.send(data);
  });
};

exports.listCbo = (req, res) => {
    Maestros.listCbo(req.body, (err, data) => {
      console.log(req.body)
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: 'Lista no encontrada'
          });
        } else {
          res.status(500).send({
            message: 'el servicio esta mantenimiento por favor intentalo mas tarde.'
          });
        }
      } else res.send(data);
    });
  };

