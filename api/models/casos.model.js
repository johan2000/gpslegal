const sql = require("./db.model.js");

const dashUser = (data, result) => {
  const bodyData = []
  sql.query('Call ca_sp_Listar_CasosByDashUsuario()', [bodyData],(err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res[0]);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};


const listCbo = (data, result) => {
  const bodyData = []
  sql.query('Call ca_sp_ListarCombo_Casos()', [bodyData],(err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res[0]);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

const listGeneral = (data, result) => {
  const bodyData = []
  sql.query('Call ca_sp_Listar_Casos()', [bodyData],(err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res[0]);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

module.exports = {
  dashUser,
  listCbo,
  listGeneral
};
