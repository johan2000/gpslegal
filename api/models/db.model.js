const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");

var connection = mysql.createPool({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  multipleStatements: true,
  typeCast: function castField(field, useDefaultTypeCasting) {

    if ((field.type === "BIT") && (field.length === 1)) {

      var bytes = field.buffer();
      return (bytes[0] === 1);

    }

    return (useDefaultTypeCasting());

  }
});

module.exports = connection;
