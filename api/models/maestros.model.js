const sql = require("./db.model.js");

const buscarPorId = (data, result) => {
  const bodyData = [data.id]
  sql.query('Call Ma_Combo_Maestro(?)', [bodyData],(err, res) => {
    if (err) {
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res[0]);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

const listCbo = (data, result) => {
    const bodyData = []
    sql.query('Call Ma_sp_Listar_cbo_Maestro()', [bodyData],(err, res) => {
      if (err) {
        result(err, null);
        console.table(result)
        return;
      }
      if (res.length) {
        result(null, res[0]);
        console.table(result)
        return;
      }
      result({ kind: "not_found" }, null);
      console.table(result)
    });
  };
  
module.exports = {
  buscarPorId,
  listCbo
};
