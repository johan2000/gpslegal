const MailCtrl = {};
const nodemailer = require("nodemailer");
const moment = require("moment-timezone");

const jsonwebtoken = require('jsonwebtoken');
const SECRET_KEY_JWT = "secretkey5353";

const urlAws = "ec2-3-134-98-229.us-east-2.compute.amazonaws.com";
const urlLocalHost = "localhost:81";
var ical = require('ical-generator');


const sql = require("../../models/db.model");
const { format } = require("mysql");
const connection = sql

function convertir(nombre) {
      
  var cadena = nombre.toLowerCase().split(' ');
  
  for (var i = 0; i < cadena.length; i++) {
   cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
  }
  nombre = cadena.join(' ');
  
  return nombre
}

function FormatoMoneda (moneda){


  return new Intl.NumberFormat('en-IN', { minimumFractionDigits: 2 }).format(moneda);

}

MailCtrl.sendMail = async (req, res) => {

  /***********  Codigo de Hernan Rios ******** */
  let parametro = req.body.parametro;

  /*
    let transporter = nodemailer.createTransport({
      host: "email-smtp.us-east-2.amazonaws.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: 'AKIAT5BVLBHYSRSUMFOT', // generated ethereal user
        pass: 'BIYDEdxK8/e0s1XvXOhbw6a2/IOodhiKX0g6FUK/Dc6H', // generated ethereal password
      },
      tls: {
        ciphers: 'SSLv3'
      }
    });
  */

  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
      pass: 'aybcwyjqemoauboy', // generated ethereal password
    },
  });


  if (parametro.tpoTbl == 'evento') {  // <<<<<<  parametro referente a evento >>>>>

    console.log(req.body);

    let arrayParticipantes = req.body.arrayParticipantes;
    let evento = req.body.dataEvent;
    const nCli_Id = req.body.cliente;
    var nomCli = "";
    var ttlNotEve = "";
    var subTtlNotEve = "";

    evento.cEve_Descripcion = evento.cEve_Descripcion.substr(0, 150)

    var fechaI2 = "";
    var fechaF2 = "";
    var ttlCaso = req.body.ttlCaso;

    var idEvent = "";
    var idCaso = "";

    let fechaIni = req.body.dataEvent.dEve_FechaIni.replace(/-/g, '');
    let horaIni = req.body.dataEvent.tEve_HoraIni.replace(/:/g, '');
    let formatFchIni = fechaIni + "T" + horaIni;


    let fechaFin = req.body.dataEvent.dEve_FechaFin.replace(/-/g, '');
    let horaFin = req.body.dataEvent.tEve_HoraFin.replace(/:/g, '');
    let formatFchFin = fechaFin + "T" + horaFin;
 

    if (formatFchIni.length == 13) {
      formatFchIni = formatFchIni + "00";
    }
    if (formatFchFin.length == 13) {
      formatFchFin = formatFchFin + "00";
    }

    var d = new Date(formatFchFin);
    var myTimezone = "America/Bogota";
    var myDatetimeFormat = "YYYY-MM-DD hh:mm:ss a z";
    var myDatetimeString = moment(d).tz(myTimezone).format(myDatetimeFormat);


    var content = 'BEGIN:VCALENDAR\n' +
      'VERSION:2.0\n' +
      'BEGIN:VEVENT\n' +
      `SUMMARY:${evento.cEve_Titulo}\n` +
      `DTSTART;TZID=America/Bogota:${formatFchIni}\n` +
      `DTEND;TZID=America/Bogota:${formatFchFin}\n` +
      `LOCATION:${evento.cEve_Ubicacion}\n` +
      `DESCRIPTION:${evento.cEve_Descripcion}\n` +
      'ORGANIZER;CN=name:mailto:hernanriosvalencia92@gmail.com\n' +
      'STATUS:CONFIRMED\n' +
      'SEQUENCE:3\n' +
      'BEGIN:VALARM\n' +
      'TRIGGER:-PT10M\n' +
      `ORGANIZER;CN=Me:MAILTO::hernanriosvalencia92@gmail.com\n` +
      'ACTION:DISPLAY\n' +
      'END:VALARM\n' +
      'END:VEVENT\n' +
      'END:VCALENDAR';

    if (parametro.tpoAct == 'insertar') {
      ttlNotEve = "Tenemos para usted un nuevo evento.";
      subTtlNotEve = "Se le ha incluido en el siguiente evento.";
      idEvent = req.body.idEvent;
      idCaso = req.body.dataEvent.nCas_Id;

      req.body.dataEvent.nEve_Id = req.body.idEvent;


      const payload = {
        nEve_Id: req.body.dataEvent,
        nCas_Id: req.body.idCaso,
        nCli_Id: nCli_Id,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);


    }
    else if (parametro.tpoAct == 'actualizar') {
      ttlNotEve = "Se ha actualizado un evento.";
      subTtlNotEve = "Se ha actualizado un evento donde usted participa.";

      const payload = {
        nEve_Id: req.body.dataEvent,
        nCas_Id: req.body.idCaso,
        nCli_Id: nCli_Id,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
      console.log(token);


      var ttlCaso = "";
      idEvent = req.body.dataEvent.nEve_Id;
      idCaso = req.body.idCaso;
      evento.tEve_HoraIni = moment(evento.tEve_HoraIni, ["HH.mm"]).format("hh:mm a");
      evento.tEve_HoraFin = moment(evento.tEve_HoraFin, ["HH.mm"]).format("hh:mm a");
    }

    console.log(idEvent);

    if (arrayParticipantes.length == 0) {
      console.log("no hay participantes");
      res.json({ status: 1, msj: "no existen participantes, no se envio ningun correo" });
    }
    else {

      console.log("numero de participantes : ",arrayParticipantes.length );
      // convert the invite to be base64 for attachment
      const buf = Buffer.from(ical.toString(), 'utf-8');
      const base64Cal = buf.toString('base64');

      const sql = `SELECT DATE_FORMAT(dEve_FechaIni, '%d/%m/%Y') as 'dEve_FechaIni2',
          DATE_FORMAT(dEve_FechaIni, '%Y-%m-%d') as 'dEve_FechaIni',
          DATE_FORMAT(dEve_FechaFin, '%d/%m/%Y') as 'dEve_FechaFin2',
          DATE_FORMAT(dEve_FechaFin, '%Y-%m-%d') as 'dEve_FechaFin',
          DATE_FORMAT(tEve_HoraIni, '%h:%i %p') as 'tEve_HoraIni2',
          DATE_FORMAT(tEve_HoraFin, '%h:%i %p') as 'tEve_HoraFin2'
          FROM Eventos 
          where nEve_Id=?`;
      connection.query(sql, [idEvent], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {

          console.log("resultado fecha : ",result[0] );
          fechaI2 = result[0].tEve_HoraIni2;
          fechaF2 = result[0].tEve_HoraFin2;
          evento.tEve_HoraIni = fechaI2;
          evento.tEve_HoraFin = fechaF2;

          const sqlcs = `select cCas_Titulo from Casos where nCas_Id=?`;
          connection.query(sqlcs, [idCaso], (error, result) => {
            if (error) throw error;
            if (result.length > 0) {

              console.log("Titulo caso : ",result[0].cCas_Titulo );
              ttlCaso = result[0].cCas_Titulo;
    

              const sql = " Call cli_sp_nombreCliente(?);";
              connection.query(sql,[nCli_Id], (error, result) => {
                if (error) throw error;
                console.log(result);
                if (result.length > 0) {
                  console.log("Nivel 3");
                  console.log(result[0].cCli_NombreCompleto);
                  nomCli = result[0].cCli_NombreCompleto;
                  //arrayParticipantes[2].email="hernanriosvalencia92@gmail.com";
                  
                  var htmlElements = "";

                  for (var i = 0; i < 4; i++) {
                    htmlElements += '<div class="box">'+i+'</div>';
                 }

                 console.log( arrayParticipantes);
                  for (let i = 0; i < arrayParticipantes.length; i++) {


                    var mailOptions = {
                      from: '📨 "GPS Legal" <hernanriosvalencia92@gmail.com>',
                      to: arrayParticipantes[i].email,
                      subject: 'NOTIFICACIÓN DE EVENTO',
                      text: 'GPS Legal',
                      icalEvent: {
                        filename: "invitation.ics",
                        method: 'PUBLISH',
                        content: content,
                        contentType: "text/calendar",
                      },
                      attachments: [
                        {
                          name: 'invite.ics',
                          type: 'text/calendar;method=REQUEST;name=\"invite.ics\"',
                          data: base64Cal,
                        },
                      ],
                      html:
                        `<div style="width: 100%; height: 700px;margin:auto">
              
                          <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                          <div style="width: 60%;background-color: white;float: left;">
                            <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                              <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                            </div>
                            <div style="width: 100%; background-color: white;margin-top: 10px;">
                              <h3 style="font-size: 24px;color: black;">
                                <strong style="margin-left: 40px;padding-left: 0px;">${ttlNotEve}</strong> 
                              </h3>
                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 5px;">
                              <h5 style="font-size: 14px;margin-left: 40px;">
                                <strong>${subTtlNotEve}</strong>

                              </h5>
                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
                        
                              <div style="width: 100%;background-color: white;">
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                  <strong>Título</strong> 
                                </span>
                                <span style="font-size: 14px;margin-left: 20px;margin-right: 20px;">:</span>
                                <span style="font-size: 14px;">
            
                                <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/Evento/${token}">${evento.cEve_Titulo}</a> 
            
                                </span>
                              </div>
            
                              <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                                <strong>Cliente</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 20px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${nomCli}
                              </span>
                            </div>
            
                              <div style="width: 100%;background-color: white;margin-top: 10px;">
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                  <strong>Caso</strong> 
                                </span>
                                <span style="font-size: 14px;margin-left: 23px;margin-right: 20px;">:</span>
                                <span style="font-size: 14px;">
                                <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/dashboard-Casos/${token}">${ttlCaso}</a> 
                           
                                </span>
                              </div>
            
                              <div style="width: 100%;background-color: white;margin-top: 10px;">
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 32px;">
                                  <strong>Categoría </strong> 
                                </span>
                                <span style="font-size: 14px;margin-left: 0px;margin-right: 19px;">:</span>
                                <span style="font-size: 14px;">
                                  ${req.body.categoria}
                                </span>
                              </div>
                        
                           
                        
                              <div style="width: 100%;background-color: white; margin-top: 10px;">
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                  <strong>Fecha inicio</strong> 
                                </span>
                          
                                <span style="font-size: 14px;margin-left: 16px;margin-right: 20px;">:</span>
                          
                                <span style="font-size: 14px;">
                                  ${moment(evento.dEve_FechaIni).format('DD/MM/YYYY')} &nbsp; ${evento.tEve_HoraIni}
                                </span>
                              </div>
              
                              <div style="width: 100%;background-color: white; margin-top: 10px;">
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                  <strong>Fecha fin</strong> 
                                </span>
                          
                                <span style="font-size: 14px;margin-left: 35px;margin-right: 20px;">:</span>
                          
                                <span style="font-size: 14px;">
                                  ${moment(evento.dEve_FechaFin).format('DD/MM/YYYY')} &nbsp; ${evento.tEve_HoraFin}
                                </span>
                              </div>
              
                              <div style="width: 100%;background-color: white; margin-top:10px;display: block;">
                        
                                <div style=" width: 26%;background-color: white;float: left;">
                                  <span style="font-size: 14px;margin-left: 39px;margin-right: 0px;">
                                  <strong>Ubicación</strong>  
                                  </span>
                                  <span style="font-size: 14px;margin-left:31px">:</span>
                                </div>
                          
                                <div style="width: 60%;min-width: 60px;background-color: white !important; margin-top: 0px;float: left;margin-left:0px">
                                  <span style="font-size: 14px; margin-left:0px;min-width: 200px !important;min-height:150px !important">
                                    ${evento.cEve_Ubicacion}
                                  </span>
                                </div>
                                
                              </div>
                        
                              <div style="width: 100%;background-color: white; margin-top: 10px;display: block;float: left;">
                        
                                <div style="width: 26%;background-color: white; float: left;">
                                
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                  <strong>Descripción</strong>  
                                  </span>
                                  <span style="font-size: 14px;margin-left:17px">
                                  :
                                  </span>
                                
                                </div>
                                
                          
                                <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;float: left;">
                                  <label style="font-size: 14px; margin-left:0px" maxlength="50">
                                    ${evento.cEve_Descripcion}
                                  </label>
                                </div>
                                
                              </div>

                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 150px;text-align: center;">
                              <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                                <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                              </div>
                              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                                <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                              </div>
                              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                                <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                              </div>
                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                                <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                              <strong>Nuestra dirección de correo es:</strong><br>
                                <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                            </div>
                        
                            <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
                        
                              <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                              <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                                <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                              </div>
                              
                            </div>
                          </div>
                          <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                            </div>`
                    }


                    transporter.sendMail(mailOptions, (error, info) => {
                      if (error) {
                        return console.log(error);
                      }
                      console.log('Mensaje enviajo');
                      res.json({ message: 'sended successfully' })
                    });
                  }
                } else {
                  console.log("succes");
                  res.send('success');
                }
              });
            } else {

            }
          });
        } else {
          console.log("entro al else");
        }
      });
    }
  }
  else if (parametro.tpoTbl == 'tarea') {  // <<<<<<  parametro referente a tareas >>>>>

    console.log(req.body);
    let arrayParticipantes = req.body.arrayParticipantes;
    let tarea = req.body.dataTarea;
    const nCli_Id = req.body.cliente;
    var nomCli = "";

    var ttlNotTar = "";
    var subTtlNotTar = "";
    var ttlCaso = "";

    let fechaFin = req.body.dataTarea.dTar_Fec_Vence.replace(/-/g, '');
    let horaFin = req.body.dataTarea.tTar_Hora_Vence.replace(/:/g, '');
    let formatFchFin = fechaFin + "T" + horaFin;
    console.log(formatFchFin);

    console.log("cuantas letras tiene la fecha");
    console.log(formatFchFin.length);
    if (formatFchFin.length == 13) {
      formatFchFin = formatFchFin + "00";
    }


    if (arrayParticipantes.length == 0) {
      res.json({ status: 1, msj: "no existen participantes, no se envio ningun correo" });
    }
    else {
      if (parametro.tpoAct == 'insertar') {
        ttlNotTar = "Tenemos para usted una nueva tarea.";
        subTtlNotTar = "Se le ha incluido en la siguiente tarea.";
        ttlCaso = req.body.ttlCaso;


        console.log(formatFchFin);

        var idTar = req.body.idTar;
        console.log("--------- consulta --------------------");
        var fechaF2 = "";

        req.body.dataTarea.nTar_Id = req.body.idTar;

        const payload = {
          tarea: req.body.dataTarea,
          nCas_Id: req.body.dataTarea.nCas_Id,
          nCli_Id: nCli_Id,
          iat: moment().unix(),
          exp: moment().add(1, 'days').unix(),
        }
        var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
        console.log(token);


        const sql = `SELECT 
                        DATE_FORMAT(tTar_Hora_Vence, '%h:%i %p') AS tTar_Hora_Vence2
                        FROM Tareas 
                        where nTar_Id=?`;
        connection.query(sql, [idTar], (error, result) => {
          if (error) throw error;
          if (result.length > 0) {
            console.log(result[0]);
            fechaF2 = result[0].tTar_Hora_Vence2;
            console.log("+-+++++++++++++++++++++++++++++++");
            console.log(fechaF2);
            tarea.tTar_Hora_Vence = fechaF2;

          } else {
            res.send('success');
          }
        });

        console.log("--------- consulta --------------------");

      }
      else if (parametro.tpoAct == 'actualizar') {

        console.log(req.body);

        const payload = {
          tarea: req.body.dataTarea,
          nCas_Id: req.body.dataTarea.nCas_Id,
          nCli_Id: nCli_Id,
          iat: moment().unix(),
          exp: moment().add(1, 'days').unix(),
        }
        var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
        console.log(token);


        ttlNotTar = "Se ha actualizado una tarea.";
        subTtlNotTar = "Se ha actualizado una tarea donde usted participa.";
        tarea.tTar_Hora_Vence = moment(tarea.tTar_Hora_Vence, ["HH.mm"]).format("hh:mm a");
        console.log(tarea.tTar_Hora_Vence);
      }




      const sql = `SELECT nCli_Id,CASE bCli_Tipo WHEN 1 THEN cCli_RazonSocial ELSE CONCAT(cCli_NombreCorto, ' ', cCli_RazonSocial, ' ', cCli_Materno) END AS 'cCli_NombreCompleto' 
        FROM Clientes where nCli_Id=${nCli_Id}`;



      const sqlcs = `select cCas_Titulo from Casos where nCas_Id=?`;
      connection.query(sqlcs, [req.body.dataTarea.nCas_Id], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          console.log(result[0]);
          ttlCaso = result[0].cCas_Titulo;
          console.log(ttlCaso);

          const sql = `SELECT nCli_Id,CASE bCli_Tipo WHEN 1 THEN cCli_RazonSocial ELSE CONCAT(cCli_NombreCorto, ' ', cCli_RazonSocial, ' ', cCli_Materno) END AS 'cCli_NombreCompleto' 
            FROM Clientes where nCli_Id=?`;

          connection.query(sql, [nCli_Id], (error, result) => {
            if (error) throw error;
            if (result.length > 0) {
              console.log(result[0].cCli_NombreCompleto);
              nomCli = result[0].cCli_NombreCompleto;



              var content = 'BEGIN:VCALENDAR\n' +
                'VERSION:2.0\n' +
                'BEGIN:VEVENT\n' +
                `SUMMARY:${tarea.cTar_Titulo}\n` +
                `DTEND;VALUE=DATE:${formatFchFin}\n` +
                `LOCATION:\n` +
                `DESCRIPTION:\n` +
                'STATUS:CONFIRMED\n' +
                'SEQUENCE:3\n' +
                'BEGIN:VALARM\n' +
                'TRIGGER:-PT10M\n' +
                `DESCRIPTION:\n` +
                'ACTION:DISPLAY\n' +
                'END:VALARM\n' +
                'END:VEVENT\n' +
                'END:VCALENDAR';


              for (let i = 0; i < arrayParticipantes.length; i++) {

                var mailOptions = {
                  from: '📨 "GPS Legal" <notificador@concursalperu.com>',
                  to: arrayParticipantes[i].email,
                  subject: 'NOTIFICACIÓN DE TAREA',
                  text: 'GPS Legal',
                  icalEvent: {
                    filename: "invitation.ics",
                    method: 'request',
                    content: content
                  },
                  html:
                    `<div style="width: 100%; height: 700px;margin:auto">
          
                      <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                      <div style="width: 60%;background-color: white;float: left;">
                        <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                          <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                        </div>
                        <div style="width: 100%; background-color: white;margin-top: 10px;">
                          <h3 style="font-size: 24px;color: black;">
                            <strong style="margin-left: 40px;padding-left: 0px;">${ttlNotTar}</strong> 
                          </h3>
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 5px;">
                          <h5 style="font-size: 14px;margin-left: 40px;">
                            <strong>${subTtlNotTar}</strong> 
                          </h5>
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
                    
                          <div style="width: 100%;background-color: white;">
                            <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                              <strong>Título</strong> 
                            </span>
                            <span style="font-size: 14px;margin-left: 50px;margin-right: 20px;">:</span>
                            <span style="font-size: 14px;">
                            <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/Tareas/${token}">${tarea.cTar_Titulo}</a> 
                        
                            </span>
                          </div>
      
                          <div style="width: 100%;background-color: white; margin-top: 10px;">
                            <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                              <strong>Cliente</strong> 
                            </span>
                      
                            <span style="font-size: 14px;margin-left: 50px;margin-right: 20px;">:</span>
                      
                            <span style="font-size: 14px;">
                              ${nomCli}
                            </span>
                          </div>
      

                          <div style="width: 100%;background-color: white; margin-top: 10px; margin-bottom:15px;display: block;float: left;">
                        
                          <div style="width: 30%;background-color: white; float: left;">
                          
                            <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                            <strong>Caso</strong>  
                            </span>
                            <span style="font-size: 14px;margin-left:92px">
                            :
                            </span>
                          
                          </div>
                          
                    
                          <div style="width: 68%;padding-left:3px;background-color: white; margin-top: 0px;float: left;">
                            <label style="font-size: 14px; " maxlength="50">
                            <a style="font-size:14px;text-decoration: none" href="http://${urlAws}/dashboard-Casos/${token}">${ttlCaso}</a> 
                            </label>
                          </div>
                          
                        </div>
                    
                    
                          <div style="width: 100%;background-color: white; margin-top: 30px;">
                            <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                              <strong>Fecha vencimiento</strong> 
                            </span>
                      
                            <span style="font-size: 14px;margin-left: 0px;margin-right: 20px;">:</span>
                      
                            <span style="font-size: 14px;">
                              ${moment(tarea.dTar_Fec_Vence).format('DD/MM/YYYY')} &nbsp; ${tarea.tTar_Hora_Vence}
                            </span>
                          </div>
                    
                          
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 50px;text-align: center;">
                          <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                          <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                            <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                          </div>
                          <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                            <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                          </div>
                          <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                            <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                          </div>
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                            <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                          <strong>Nuestra dirección de correo es:</strong><br>
                            <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                        </div>
                    
                        <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
                    
                          <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                          <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                            <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                          </div>
                          
                        </div>
                      </div>
                      <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                        </div>`
                }

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  }
                  console.log('Mensaje enviajo');
                  res.json({ message: 'sended successfully' })
                });
              }

            } else {
              res.send('success');
            }
          });
        } else {
          res.send('success');
        }
      });
    }
  }
  else if (parametro.tpoTbl == 'ingreso') {


    let parametro = req.body.parametro;
    if (parametro.tpoAct == "insertar") {
      let tpoIngreso = undefined;
   

      if (req.body.dataIngreso.cIng_Tab_Tipo == "KP01") {
        tpoIngreso = "Honorarios"
      }
      else if (req.body.dataIngreso.cIng_Tab_Tipo == "KP02") {
        tpoIngreso = "Gastos"
      }

      var nomCli = "";
      var moneda = "";
      var _idCaso = req.body.dataIngreso.nCas_Id;

      let gp = {
        _idCli: req.body.idClient,
        _nroCaso: req.body.casoSlt[0]["cCas_Cod_Externo"],
        _motivo: tpoIngreso,
        _fecha: moment(req.body.dataIngreso.dIng_Fecha).format('DD/MM/YYYY'),
        _monto: req.body.dataIngreso.nIng_Monto,
        _titulo: "",
        _descripcion: req.body.dataIngreso.cIng_Detalle.substr(0, 150)
      }


      const payload = {
        nCas_Id: req.body.casoSlt,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);

      const simbolo = req.body.dataIngreso.cIng_Tab_Moneda;

      let formatMoney = parseInt(gp._monto);
      gp._monto = new Intl.NumberFormat("de-DE").format(formatMoney);


      const sqlMoneda = `select * from Maestros where cTabCodigo=?`;
      connection.query(sqlMoneda, [simbolo.toString()], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          moneda = result[0]["cTabSimbolo"]
        } else {
          res.send('success');
        }
      });



      const sql = `SELECT * FROM Clientes where nCli_Id=?`;
      connection.query(sql, [gp._idCli], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          let nombre = result[0].cCli_NombreCorto;
          let razon = result[0].cCli_RazonSocial;
          nomCli = nombre + " " + razon;

          const sqlCas = `SELECT cCas_Titulo FROM Casos where nCas_Id=?`;
          connection.query(sqlCas, [req.body.dataIngreso.nCas_Id], (error, result) => {
            if (error) throw error;
            if (result.length > 0) {

              let titulo = result[0]["cCas_Titulo"];
              gp._titulo = titulo;

    

              const sql = 'Call ca_sp_fltAboResponsable(?)';
              connection.query(sql, [_idCaso], (error, result) => {
                if (error) throw error;
                if (result.length > 0) {
            

                  var arrayResult = result[0];

                  // aqui va el metodo para enviar mensaje
                  for (let i = 0; i < arrayResult.length; i++) {

                    if (arrayResult[i].cCas_Tab_TipoEquipo == "Abogado Responsable") {
          
                      let mailResponsable = arrayResult[i].email;
            

                      var mailOptions = {
                        from: '📨 "GPS Legal" <notificador@concursalperu.com>',
                        to: mailResponsable,
                        subject: 'NOTIFICACIÓN DE INGRESO',
                        text: 'GPS Legal',
                        html:
                          `<div style="width: 100%; height: 700px;margin:auto">
            
                        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                        <div style="width: 60%;background-color: white;float: left;">
                          <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                            <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                          </div>
                          <div style="width: 100%; background-color: white;margin-top: 10px;">
                            <h3 style="font-size: 24px;color: black;">
                              <strong style="margin-left: 40px;padding-left: 0px;"> Se ha registrado un nuevo ingreso</strong> 
                            </h3>
                          </div>
  
                          <div style="width: 100%;background-color: white;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                <strong>Título</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 21px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/dashboard-Casos/${token}"> ${gp._titulo}</a>
                              </span>
                          </div>
  
                          <div style="width: 100%;background-color: white;margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                <strong>Cliente</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 12px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                ${nomCli}
                              </span>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 10px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
                      
                            <div style="width: 100%;background-color: white;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 35px;">
                                <strong>Caso</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 27px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                ${gp._nroCaso}
                              </span>
                            </div>
                      
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                                <strong>Motivo</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 22px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${gp._motivo}
                              </span>
                            </div>
                      
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                <strong>Fecha</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 56px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${gp._fecha}
                              </span>
                            </div>
            
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                <strong>Monto</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 55px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${moneda} ${gp._monto}.00
                              </span>
                            </div>
  
                            <div style="width: 100%;background-color: white; margin-top: 10px;display: block;float: left;">
                    
                              <div style="width: 26%;background-color: white; float: left;">
                              
                                <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                <strong>Descripción</strong>  
                                </span>
                                <span style="font-size: 14px;margin-left:17px">:</span>
                              
                              </div>
                        
                              <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;float: left;">
                                <label style="font-size: 14px; margin-left:0px" maxlength="50">
                                ${gp._descripcion}
                                </label>
                              </div>
                            
                            </div>
                      
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 85px;text-align: center;">
                            <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                              <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                            <strong>Nuestra dirección de correo es:</strong><br>
                              <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
                      
                            <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                            <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                              <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                            </div>
                            
                          </div>
                        </div>
                        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                          </div>`
                      }

                      transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                          return console.log(error);
                        }
           
                        res.json({ message: 'sended successfully' })
                      });
                    }
                  };
                } else {
                  res.send('success');
                }
              });
            }
          });
 

        } else {
          res.send('success');
        }
      });
    }
    else if (parametro.tpoAct == "actualizar") {
      console.log("Este es el cuerpo");
      console.log("cuerpo",req.body);


      const payload = {
        nCas_Id: req.body.casoSlt,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);


      let tpoIngreso = undefined;

      var moneda="";

      if (req.body.dataIngreso.cIng_Tab_Tipo == "KP01") {
        tpoIngreso = "Honorarios"
      }
      else if (req.body.dataIngreso.cIng_Tab_Tipo == "KP02") {
        tpoIngreso = "Gastos"
      }

      var nomCli = "";
      var _idCaso = req.body.dataIngreso.nCas_Id;
      const simbolo = req.body.dataIngreso.cIng_Tab_Moneda;

      let gp = {
        _idCli: req.body.idClient,
        _nroCaso: "",
        _motivo: tpoIngreso,
        _fecha: moment(req.body.dataIngreso.dIng_Fecha).format('DD/MM/YYYY'),
        _monto: req.body.dataIngreso.nIng_Monto,
        _titulo: ""
      }

      const sqlMoneda = `select * from Maestros where cTabCodigo=?`;
      connection.query(sqlMoneda, [simbolo.toString()], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          moneda = result[0]["cTabSimbolo"]
        } else {
          res.send('success');
        }
      });

      const sql = `SELECT * FROM Clientes where nCli_Id=?`;
      connection.query(sql, [gp._idCli], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          let nombre = result[0].cCli_NombreCorto;
          let razon = result[0].cCli_RazonSocial;
          nomCli = nombre + " " + razon;

          const sqlCas = `SELECT cCas_Titulo, cCas_Cod_Externo FROM Casos where nCas_Id=?`;
          connection.query(sqlCas, [req.body.dataIngreso.nCas_Id], (error, result) => {
            if (error) throw error;
            if (result.length > 0) {

              let titulo = result[0]["cCas_Titulo"];
              let nroCaso = result[0]["cCas_Cod_Externo"];
              gp._titulo = titulo;
              gp._nroCaso = nroCaso;

              const sql = 'Call ca_sp_fltAboResponsable(?)';
              connection.query(sql, [_idCaso], (error, result) => {
                if (error) throw error;
                if (result.length > 0) {
                  console.log(result[0]);

                  var arrayResult = result[0];

                  // aqui va el metodo para enviar mensaje
                  for (let i = 0; i < arrayResult.length; i++) {

                    if (arrayResult[i].cCas_Tab_TipoEquipo == "Abogado Responsable") {
          
                      let mailResponsable = arrayResult[i].email;

                      var mailOptions = {
                        from: '📨 "GPS Legal" <notificador@concursalperu.com>',
                        to: mailResponsable,
                        subject: 'NOTIFICACIÓN DE INGRESO',
                        text: 'GPS Legal',
                        html:
                          `<div style="width: 100%; height: 700px;margin:auto">
            
                        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                        <div style="width: 60%;background-color: white;float: left;">
                          <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                            <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                          </div>
                          <div style="width: 100%; background-color: white;margin-top: 10px;">
                            <h3 style="font-size: 24px;color: black;">
                              <strong style="margin-left: 40px;padding-left: 0px;"> Se modificó un ingreso </strong> 
                            </h3>
                          </div>
  
                          <div style="width: 100%;background-color: white;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                <strong>Título</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 21px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                ${gp._titulo}
                              </span>
                          </div>
  
                          <div style="width: 100%;background-color: white;margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                <strong>Cliente</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 12px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                ${nomCli}
                              </span>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 10px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
                      
                            <div style="width: 100%;background-color: white;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 35px;">
                                <strong>Caso</strong> 
                              </span>
                              <span style="font-size: 14px;margin-left: 28px;margin-right: 20px;">:</span>
                              <span style="font-size: 14px;">
                                <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/dashboard-Casos/${token}">${gp._nroCaso}</a>
                              </span>
                            </div>
                      
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                                <strong>Motivo</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 22px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${gp._motivo}
                              </span>
                            </div>
                      
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                <strong>Fecha</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 56px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                                ${gp._fecha}
                              </span>
                            </div>
            
                            <div style="width: 100%;background-color: white; margin-top: 10px;">
                              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                <strong>Monto</strong> 
                              </span>
                        
                              <span style="font-size: 14px;margin-left: 55px;margin-right: 20px;">:</span>
                        
                              <span style="font-size: 14px;">
                              ${moneda} ${FormatoMoneda(gp._monto)}
                              </span>
                            </div>
                      
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 50px;text-align: center;">
                            <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                              <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                            </div>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                              <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                            <strong>Nuestra dirección de correo es:</strong><br>
                              <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                          </div>
                      
                          <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
                      
                            <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                            <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                              <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                            </div>
                            
                          </div>
                        </div>
                        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                          </div>`
                      }

                      transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                          return console.log(error);
                        }
        
                        res.json({ message: 'sended successfully' })
                      });

                    }

                  };

                } else {
                  res.send('success');
                }
              });


            } else {
              res.send('success');
            }

          });

        } else {
          res.send('success');
        }
      });


    }
  }
  else if (parametro.tpoTbl == 'egreso') {
    console.log(req.body);

    var _idCaso = req.body.dataEgreso.nCas_Id;
    var tipoEgreso = undefined;
    console.log(" codigo de caso :", _idCaso);

    let gp = {
      _idCli: req.body.idClient,
      _nroCaso: req.body.codExt,
      _motivo: tipoEgreso,
      _fecha: moment(req.body.dataEgreso.dEge_Fecha).format('DD/MM/YYYY'),
      _monto: req.body.dataEgreso.nEge_Monto,
      _titulo: req.body.ttlCaso,
      _descripcion: req.body.dataEgreso.cEge_Detalle.substr(0, 150)
    }

    var nomCli = undefined;
    const simbolo = req.body.dataEgreso.cIng_Tab_Moneda;

    let formatMoney = parseInt(gp._monto);
    gp._monto = Intl.NumberFormat("de-DE").format(formatMoney);

    const sqlMotivo = `select * from Maestros where cTabCodigo=?`;
    connection.query(sqlMotivo, [req.body.dataEgreso.cEge_Tab_Tipo.toString()], (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        console.log(result[0]);
        tipoEgreso = result[0]["cTabNombre"];
        gp._motivo = tipoEgreso;
      } else {
        res.send('success');
      }
    });

    console.log("Esto es el codigo del simbolo : ", simbolo);
    const sqlMoneda = `select * from Maestros where cTabCodigo=?`;
    connection.query(sqlMoneda, [simbolo.toString()], (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        console.log(result[0]);
        moneda = result[0]["cTabSimbolo"]
      } else {
        res.send('success');
      }
    });

    const sql = `SELECT * FROM Clientes where nCli_Id=?`;
    connection.query(sql, [gp._idCli], (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        let nombre = result[0].cCli_NombreCorto;
        let razon = result[0].cCli_RazonSocial;
        nomCli = nombre + " " + razon;

        const sql = 'Call ca_sp_fltAboResponsable(?)';
        connection.query(sql, [_idCaso], (error, result) => {
          if (error) throw error;
          if (result.length > 0) {

            var arrayResult = result[0];
            console.log(arrayResult)
            console.log(arrayResult.length)

            // aqui va el metodo para enviar mensaje
            for (let i = 0; i < arrayResult.length; i++) {

              if (arrayResult[i].cCas_Tab_TipoEquipo == "Abogado Responsable") {
                console.log("entro al if")
                let mailResponsable = arrayResult[i].email;
                console.log("Encargado :", mailResponsable)

                var mailOptions = {
                  from: '📨 "GPS Legal" <notificador@concursalperu.com>',
                  to: mailResponsable,
                  subject: 'NOTIFICACIÓN DE EGRESO',
                  text: 'GPS Legal',
                  html:
                    `<div style="width: 100%; height: 700px;margin:auto">
  
                    <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                    <div style="width: 60%;background-color: white;float: left;">
                      <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                        <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                      </div>
                      <div style="width: 100%; background-color: white;margin-top: 10px;">
                        <h3 style="font-size: 24px;color: black;">
                          <strong style="margin-left: 40px;padding-left: 0px;"> Usted tiene un nuevo egreso registrado</strong> 
                        </h3>
                      </div>
  
                      <div style="width: 100%;background-color: white;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                    <strong>Título</strong> 
                                  </span>
                                  <span style="font-size: 14px;margin-left: 21px;margin-right: 20px;">:</span>
                                  <span style="font-size: 14px;">
                                    ${gp._titulo}
                                  </span>
                              </div>
  
                              <div style="width: 100%;background-color: white;margin-top: 10px;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                                    <strong>Cliente</strong> 
                                  </span>
                                  <span style="font-size: 14px;margin-left: 12px;margin-right: 20px;">:</span>
                                  <span style="font-size: 14px;">
                                    ${nomCli}
                                  </span>
                              </div>
                          
                              <div style="width: 100%;background-color: white;margin-top: 10px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
                          
                                <div style="width: 100%;background-color: white;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 35px;">
                                    <strong>Caso</strong> 
                                  </span>
                                  <span style="font-size: 14px;margin-left: 28px;margin-right: 20px;">:</span>
                                  <span style="font-size: 14px;">
                                    ${gp._nroCaso}
                                  </span>
                                </div>
                          
                                <div style="width: 100%;background-color: white; margin-top: 10px;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                                    <strong>Motivo</strong> 
                                  </span>
                            
                                  <span style="font-size: 14px;margin-left: 22px;margin-right: 20px;">:</span>
                            
                                  <span style="font-size: 14px;">
                                    ${gp._motivo}
                                  </span>
                                </div>
                          
                                <div style="width: 100%;background-color: white; margin-top: 10px;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                    <strong>Fecha</strong> 
                                  </span>
                            
                                  <span style="font-size: 14px;margin-left: 56px;margin-right: 20px;">:</span>
                            
                                  <span style="font-size: 14px;">
                                    ${gp._fecha}
                                  </span>
                                </div>
                
                                <div style="width: 100%;background-color: white; margin-top: 10px;">
                                  <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                    <strong>Monto</strong> 
                                  </span>
                            
                                  <span style="font-size: 14px;margin-left: 55px;margin-right: 20px;">:</span>
                            
                                  <span style="font-size: 14px;">
                                    ${moneda} ${gp._monto}.00
                                  </span>
                                </div>
  
                                <div style="width: 100%;background-color: white; margin-top: 10px;display: block;float: left;">
                  
                                  <div style="width: 26%;background-color: white; float: left;">
                                  
                                    <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                                    <strong>Descripción</strong>  
                                    </span>
                                    <span style="font-size: 14px;margin-left:17px">:</span>
                                  
                                  </div>
                            
                                  <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;float: left;">
                                    <label style="font-size: 14px; margin-left:0px" maxlength="50">
                                    ${gp._descripcion}
                                    </label>
                                  </div>
                                
                                </div>
  
                              </div>
                  
                      <div style="width: 100%;background-color: white;margin-top: 100px;text-align: center;">
                        <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                        <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                          <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                        </div>
                        <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                          <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                        </div>
                        <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                          <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                        </div>
                      </div>
                  
                      <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                          <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                      </div>
                  
                      <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                        <strong>Nuestra dirección de correo es:</strong><br>
                          <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                      </div>
                  
                      <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
                  
                        <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                        <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                          <span>Este correo electrónico fue enviado a su cuenta de correo electrónico<span>&nbsp;</span><a href="mailto:XXXX@gmail.com" style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" target="_blank">XXXX@gmail.com</a><span>&nbsp;</span>por<wbr>que
                            es suscriptor de GPS Legal y ha configurado su cuenta para recibir notificaciones. Si desea cambiar sus preferencias de notificación, ingrese al sistema al modulo de notificaciones..</span>
                        </div>
                        
                      </div>
                    </div>
                    <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                      </div>`
                }

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  }
                  console.log('Mensaje enviajo');
                  res.json({ message: 'sended successfully' })
                });

                console.log(nomCli);
                console.log("entro a egreso");

              }

            };

          } else {
            res.send('success');
          }
        });

      } else {
        res.send('success');
      }
    });
    console.log("entro a egreso");
  }
  else if(parametro.tpoTbl == 'caso'){

    var ttlMensaje="";
    var nomCli="";

    if (parametro.tpoAct == 'insertar') {
      const payload = {
        nCas_Id: req.body.idCaso,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
      ttlMensaje="Usted tiene una invitación a participar en un nuevo caso";
    } 
    else if(parametro.tpoAct == 'actualizar'){
      console.log("Cuerpo");
      console.log(req.body);

      const payload = {
        nCas_Id: req.body.idCaso,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix(),
      }
      var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
      ttlMensaje="Usted tiene una invitación a participar en un nuevo caso";
    }

    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
        pass: 'aybcwyjqemoauboy', // generated ethereal password
      },
    });

    var arrayParticipantes=req.body.arrayParticipantes;

    const sql = `SELECT nCli_Id,CASE bCli_Tipo WHEN 1 THEN cCli_RazonSocial ELSE CONCAT(cCli_NombreCorto, ' ', cCli_RazonSocial, ' ', cCli_Materno) END AS 'cCli_NombreCompleto' 
    FROM Clientes where nCli_Id=${req.body.cliente}`;
    connection.query(sql, (error, result) => {
      console.log(result[0].cCli_NombreCompleto);
      nomCli = result[0].cCli_NombreCompleto;

      for (let i = 0; i < arrayParticipantes.length; i++) {

        var mailOptions = {
          from: '📨 "GPS Legal" <hernanriosvalencia92@gmail.com>',
          to: arrayParticipantes[i].email,
          subject: 'INVITACION A PARTICIPAR EN UN NUEVO CASO',
          text: 'GPS Legal',
          html:
            `<div style="width: 100%; height: 700px;margin:auto">
  
              <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                <div style="width: 60%;background-color: white;float: left;">
                  <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                    <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                  </div>


                  <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;">
                    <h3 style="font-size: 24px; margin-left:40px" maxlength="50">
                      ${ttlMensaje}
                    </h3>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 5px;">
                    <h5 style="font-size: 14px;margin-left: 40px;">
                    
  
                    </h5>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 10px;"> <!-- cuerpo de datos -->
              
                    <div style="width: 100%;background-color: white;">
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                        <strong>Título</strong> 
                      </span>
                      <span style="font-size: 14px;margin-left: 27px;margin-right: 20px;">:</span>
                      <span style="font-size: 14px;">
  
                      <a style="margin-left: 0px;font-size:14px;text-decoration: none" href="http://${urlAws}/dashboard-Casos/${token}">${req.body.casNom}</a>
  
                      </span>
                    </div>
  
                    <div style="width: 100%;background-color: white; margin-top: 10px;">
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                        <strong>Cliente</strong> 
                      </span>
                
                      <span style="font-size: 14px;margin-left: 27px;margin-right: 20px;">:</span>
                
                      <span style="font-size: 14px;">
                        ${nomCli}
                      </span>
                    </div>
                  </div>
  
                <div style="width: 100%;background-color: white;margin-top: 0px;">
                  <span style="font-size: 14px;margin-left: 40px;margin-right: 22px;">
                    <strong>Cod externo</strong> 
                  </span>
                  <span style="font-size: 14px;margin-left: 0px;margin-right: 20px;">:</span>
                  <span style="font-size: 14px;">
                    ${req.body.codExt}
                  </span>
                </div>
            
            
                <div style="width: 100%;background-color: white;margin-top: 100px;text-align: center;">
                  <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                    <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                  <strong>Nuestra dirección de correo es:</strong><br>
                    <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
            
                  <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                  <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                    <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                  </div>
                  
                </div>
              </div>
              <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
              </div>`
        }
  
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }

        /** Insertar en la tabla correos enviados 
        let emp = {
          nCas_Id: req.body.idCaso,
          nUsu_Id: req.body.nUsu_Id,
          cCor_Tab_Tipo:'CR01',
          cCor_Asunto: 'Usted forma parte de un nuevo caso',
        };
        const sql = 'Call co_sp_Insertar_Correo(?,?,?,?);';

        connection.query(sql, [
          emp.nCas_Id,
          emp.nUsu_Id,
          emp.cCor_Tab_Tipo,
          emp.cCor_Asunto,
        ], error => {
          if (error) throw error;
          res.send('success');
        });
        *************************************** */

          console.log('Mensaje enviajo');
          res.json({ message: 'sended successfully' })
        });
      }

    });



    

  }
  else if(parametro.tpoTbl == 'cliente'){

    console.log("Entro a cliente");
    console.log(req.body);
    var ttlMensaje="";



    if (parametro.tpoAct == 'insertar') {
      ttlMensaje= "Usted ha sido invitado a participar en GPS LEGAL"

    } 
    if (parametro.tpoAct == 'actualizar') {
      ttlMensaje= "Se modificó su registro en GPS LEGAL"

    } 

    const payload = {
      id: req.body.nUsu_Id,
      email: req.body.email,
      iat: moment().unix(),
      exp: moment().add(1, 'days').unix(),
    }
    var token = jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);

    const dataUser = {
      id: req.body.nUsu_Id,
      accessToken: token
    }


    var cliente = req.body.cliente;


    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
        pass: 'aybcwyjqemoauboy', // generated ethereal password
      },
    });

    var mailOptions = {
      from: '📨 "GPS Legal" <hernanriosvalencia92@gmail.com>',
      to: req.body.email,
      subject: 'INVITACION A PARTICIPAR EN GPS LEGAL',
      text: 'GPS Legal',
      html:
        `<div style="width: 100%; height: 700px;margin:auto">

          <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
          <div style="width: 60%;background-color: white;float: left;">
            <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
              <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
            </div>
            <div style="width: 100%; background-color: white;margin-top: 10px;">
              <h3 style="font-size: 24px;color: black;">
                <strong style="margin-left: 40px;padding-left: 0px;">${ttlMensaje}</strong> 
              </h3>
            </div>
        
            <div style="width: 100%;background-color: white;margin-top: 5px;">
              <h5 style="font-size: 14px;margin-left: 40px;">
              

              </h5>
            </div>
        
            <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
        
              <div style="width: 100%;background-color: white;">
                <span style="font-size: 14px;margin-left: 40px;margin-right: 15px;">
                  <strong>Estimado(a)</strong> 
                </span>
                <span style="font-size: 14px;margin-left: 0px;margin-right: 20px;">:</span>
                <span style="font-size: 14px;">

                <a style="margin-left: 0px;font-size:14px;text-decoration: none">${convertir(cliente)}</a>

                </span>
              </div>


              <div style="width: 100%;background-color: white;margin-left: 40px;; margin-top: 10px;display: block;float: left;">
                      
          
                <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;float: left;">
                  <label style="font-size: 14px; margin-left:0px" maxlength="50">
                  Bienvenido.  Se le ha invitado a participar en GPS LEGAL 
                  <br/>
                  <br/>
                  Complete tu registro dando clic a este enlace:

                  </label>
                </div>
                
              </div>

              <div style="width: 100%;background-color: white; margin-top: 10px;">

                <div style="width: 100%;background-color: white; margin-top: 20px;margin-left:0px">
                  <a style="margin-left:40px;font-size:16px" href="http://ec2-3-19-237-66.us-east-2.compute.amazonaws.com/CambiarContraseña/${dataUser.accessToken}">Confirmar invitación</a>
                </div>

              </div>



                        
           
            </div>

      
        
            <div style="width: 100%;background-color: white;margin-top: 150px;text-align: center;">
              <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
            </div>
        
            <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
            </div>
        
            <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
              <strong>Nuestra dirección de correo es:</strong><br>
                <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
            </div>
        
            <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
        
              <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
              <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
              </div>
              
            </div>
          </div>
          <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
          </div>`
    }

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('Mensaje enviajo');



      res.json({ message: 'sended successfully' })
    });

  }

};


module.exports = MailCtrl;