const jsonwebtoken = require('jsonwebtoken');
const SECRET_KEY_JWT =  "secretkey5353";

const nodemailer = require("nodemailer");
const moment = require("moment");

const sql = require("../../models/db.model");
const connection = sql

const RecoveryPassCtrl = {};

function createToken(user) {

    const payload = {
      id: user.nUsu_Id,
      email: user.cUsu_email,
      iat: moment().unix(),
      exp: moment().add(1, 'days').unix(),
    }
    return jsonwebtoken.sign({ payload }, SECRET_KEY_JWT);
};

RecoveryPassCtrl.sendMail = async (req, res) => {
   
    let usuario = req.body._usuario;
  var email = "";
  var id = "";
  var nombre = "";
  console.log(usuario);
  console.log(req.body);

  const sql = `select * from Usuarios where cUsu_email=?`;

  connection.query(sql, [usuario], (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      console.log(result[0]);
      email = result[0]["cUsu_email"];
      id = result[0]["nUsu_Id"];
      nombre = result[0]["cUsu_Nombres"];

      let obj = {
        nUsu_Id: id,
        cUsu_email: email
      }

      const dataUser = {
        id: id,
        accessToken: createToken(obj)
      }


      console.log(dataUser.accessToken);
      console.log(id);


      let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
          pass: 'aybcwyjqemoauboy', // generated ethereal password
        },
      });

      var mailOptions = {
        from: '📨 "GPS Legal" <notificador@concursalperu.com>',
        to: email,
        subject: 'RECUPERAR CONTRASEÑA',
        text: 'GPS Legal',
        html:
          ` <div style="width: 100%; height: 700px;margin:auto">
   
        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
        <div style="width: 60%;background-color: white;float: left;">
          <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
            <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
          </div>
          <div style="width: 100%; background-color: white;margin-top: 10px;">
            <h3 style="font-size: 24px;color: black;">
              <strong style="margin-left: 40px;padding-left: 0px;">Cambio de contraseña de GPS Legal</strong> 
            </h3>
          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 5px;">
            <h4 style="font-size: 20px;margin-left: 40px;">
              <strong>Hola ${nombre}</strong> 
            </h4>
          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 25px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
      
            <div style="width: 100%;background-color: white;">
              <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                <label>Hemos recibido tu solicitud de restablecimiento de contraseña.</label> 
              </span>
            </div>

            <div style="width: 100%;background-color: white;">
              <span style="font-size: 14px;margin-left: 39px;margin-right: 39px;">
                <label><b>Si usted no solicito restablecer su contraseña ignore este correo.</b></label> 
              </span>
            </div>
      
            <div style="width: 100%;background-color: white; margin-top: 20px;margin-left:0px">
              <a style="margin-left:40px;font-size:16px" href="http://localhost:81/CambiarContraseña/${dataUser.accessToken}">En este link puede restablecer su contraseña</a>
            </div>

            <div style="width: 100%;background-color: white; margin-top: 10px;">
              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                <label>Después de dar clic al enlace te derivaremos a la página  con las </label> 
              </span>
            </div>
            <div style="width: 100%;background-color: white; margin-top: -5px;">
              <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                <label>instrucciones para reestablecer tu contraseña. </label> 
              </span>
            </div>

            <div style="width: 100%;background-color: white; margin-top:10px;display: block;">
                <h3 style="font-size: 24px;margin-left: 40px;">
                  <strong>¿Necesitas más información?</strong>  
                </h3>
            </div>

            <div style="width: 100%;background-color: white; margin-top:0px;display: block;">

              <div style="width: 100%;height:25px;background-color: white; margin-left:0px; padding-top:0px;margin-top:0px;display: block;">
      
                    <strong style="font-size:15px;margin-left: 40px;margin-top:-10px;padding-top:-10px;">Visita nuestra página en http://gpslegal.pe </strong>  
                 
              </div>
              <div style="width: 100%;height:25px;background-color: white; margin-left:0px; padding-top:0px; display: block;">
     
                    <strong style="font-size:15px;margin-left: 40px;margin-top:-10px;padding-top:-10px;">Envía un e-mail a nuestro: soporte@gpslegal.pe</strong>  
                 
              </div>
              <div style="width: 100%;height:25px;background-color: white; margin-left:0px; padding-top:-20px; display: block;">
                
                    <strong style="font-size:15px;margin-left: 40px;margin-top:-10px;padding-top:-10px;">Envía un mensaje al WhatsApp: +51 998440020</strong>  
                
              </div>
            </div>

          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 40px;margin-left:85px">
            <div style="width: 20%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
              <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
            </div>
            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
              <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
            </div>
            <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
              <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
            </div>
          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 30px;margin-left:95px;display: inline-block;">
              <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 25px;margin-left:160px">
            <strong>Nuestra dirección de correo es:</strong><br>
              <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline;margin-left:9px" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
          </div>
      
          <div style="width: 100%;background-color: white;margin-top: 15px;">
      
            <div style="width: 6%;background-color: white;margin-top: 5px;float: left;">
            </div>
            <div style="width: 90%;background-color: white;margin-top: 5px;float: left;">
              <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. 
              </span>
            </div>
            <div style="width: 6%;background-color: white;margin-top: 5px;float: left;">
            </div>
            <div style="width: 90%;background-color: white;float: left;">
              <span>Si desea cambiar sus preferencias de notificación, ingrese al sistema en el 
              </span>
            </div>

            <div style="width: 6%;background-color: white;margin-top: 5px;float: left;">
            </div>
            <div style="width: 90%;background-color: white;float: left;">
              <span>módulo de notificaciones.
              </span>
            </div>
            
          </div>
        </div>
        <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
          </div>
        `

      }

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          res.send('error');
          return console.log(error);

        }
        console.log('Mensaje enviajo');
        res.json({ message: 'sended successfully' })
      });

      return res.send('success');

    } else {
      res.send('error');
    }


  });

}


module.exports = RecoveryPassCtrl;


