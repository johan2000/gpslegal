const TareasCtrl = {};

const sql = require("../../models/db.model");
const connection = sql



TareasCtrl.tareasBuscar = async (req, res) => {

    let e = req.body;
    const sql = `Call ta_sp_Listar_TareaById(?)`;
    connection.query(sql, [e.id], (error, result) => {
      if (error) throw error;
  
      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('empty');
      }
    });

}

TareasCtrl.tareasParticipantes = async (req, res) => {
    let emp = req.body;
    const sql = 'Call ta_sp_Listar_Participantes()';
    connection.query(sql, (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('Error');
      }
    });

}


TareasCtrl.tareasDashUsuario = async (req, res) => {
    let e = req.body;
    const sql = `Call ta_usp_Listar_Dash_Usuario()`;
    connection.query(sql, [e.id], (error, result) => {
      if (error) throw error;
  
      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('empty');
      }
    });

}


module.exports = TareasCtrl;