const express=require('express');
const router=express.Router();

const fileUpload = require('../Controllers/fileUpload.controller');

router.post('/',fileUpload.fileUpload);

module.exports=router;  