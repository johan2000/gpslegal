const express=require('express');
const router=express.Router();

const tareas = require('../Controllers/tareas.controller');

router.post('/Buscar', tareas.tareasBuscar);
router.post('/Participantes', tareas.tareasParticipantes);
router.post('/Dash/Usuario', tareas.tareasDashUsuario);

module.exports=router;  