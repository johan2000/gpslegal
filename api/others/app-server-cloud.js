
const express = require('express');
const mysql = require('mysql');
var cors = require('cors');
const multer = require('multer');
const mimeType = require('mime-types');
//const puppeteer = require('puppeteer');
//const bodyParser  = require('body-parser');
const path = require('path');

const PUBLISHABLE_KEY = "pk_test_51H7taZUANQ3Xx5TzfzQYLqKiRQRopK6opzpDFgHh2T80YSf6VIl6s7P5ZmlVEDb4ep7qE9vvVmsGazW7wrH2Fowt100cB1fWZq7";

const SECRET_KEY = "sk_test_51H7taUANQ3Xx5Tzf6Q7UXUOwJviGZvUmOuUjIWr48KIMXv5RD7G84DJcLkMUqdIhQbfD7Oa2Ee9WEpcw53Xbmi3C00LJ49hbm3";

const stripe = require('stripe')(SECRET_KEY);


const imageToBase64 = require('image-to-base64');

const storage = multer.diskStorage({
  destination: './files',
  filename: function (req, file, cb) {
    cb("", file.originalname + Date.now() + "." + mimeType.extension(file.mimetype));

  }
})





const upload = multer({ storage: storage });


const corsOptions = {
  origin: '*',
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200
}



const jwt = require('jsonwebtoken')

//app.user(express.json())

const bodyParser = require('body-parser');

const e = require('express');
const PORT = process.env.PORT || 9000;
const app = express();

const nodemailer = require('nodemailer');

const { restart } = require('nodemon');
const { errorMonitor } = require('events');
const { throws, deepEqual } = require('assert');
const { response, json } = require('express');
const { Console } = require('console');

const connection = mysql.createConnection({
  host: '35.199.109.14',
  //host: 'localhost',
  user: 'kokixx19',
  password: 'kokixx19',
  database: '_GPSLegal',
  multipleStatements: true,
  typeCast: function castField(field, useDefaultTypeCasting) {

    if ((field.type === "BIT") && (field.length === 1)) {

      var bytes = field.buffer();
      return (bytes[0] === 1);

    }

    return (useDefaultTypeCasting());

  }
});

if (!connection) { console.log("error en la base de datos") } else { console.log("success") }




app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(cors(corsOptions));


app.set("view engine", 'ejs')


app.get('/', cors(), (req, res) => {



  res.send('hellow')
});




app.post('/File/Upload', upload.single('avatar'), cors(), (req, res) => {

  //console.log(req.file);

  console.log("nombre archivo: ", req.file.filename);


  let e = req.body;
  let data = [
    e.nCdo_Id,
    req.file.filename,
    '2021-06-06',
    0,
    '',
    1,
    '',
    0,
    '2021-01-01',
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_SubirDoc(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json({ status: 1, name: req.file.filename });
    } else {
      res.json({ status: 1, name: req.file.filename });
    }
  });




})








/*--------------------- TAREAS--------------------- */


app.post('/Tareas/Buscar', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_sp_Listar_TareaById(?)`;
  connection.query(sql, [e.id], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Tareas/Participantes', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_sp_Listar_Participantes(?)`;
  connection.query(sql, [e.nTar_Id], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Tareas/Dash/Usuario', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_usp_Listar_Dash_Usuario()`;
  connection.query(sql, [e.id], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Login/App', cors(), (req, res) => {

  const user = {
    name: "Augusto",
    email: "juancarlos@gmail.com "

  }
  jwt.sign({
    user
  }, 'secretkey', {
    expiresIn: '3600s'
  }, (err, token) => {
    res.json({
      token
    })
  })

})

app.post("/api/post", verifyToken, (req, res) => {

  jwt.verify(req.token, 'secretkey', (err, data) => {
    if (err) {

      res.sendStatus(403)
    } else {

      ;
      res.json({
        data: data["user"]
      })

    }

  })
})

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization']
  if (typeof bearerHeader !== 'undefined') {
    const bearerToken = bearerHeader.split(" ")[1]
    req.token = bearerToken
    next()
  } else {
    res.sendStatus(403)
  }

};

app.post('/Maestros/Data', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_Combo_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Listar', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_sp_Listar_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/ListarCBO', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_sp_Listar_cbo_Maestro()';
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Correlativo', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Correlativo_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Insertar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Insertar_Maestro(?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabSimbolo,
    emp.cTabNombre
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Actualizar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Actualizar_Maestro(?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabSimbolo,
    emp.cTabNombre
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Eliminar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Eliminar_Maestro(?)';
  connection.query(sql, [emp.cTabCodigo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.get('/Tarea/Listar', cors(), (req, res) => {


  const sql = 'Call ta_sp_Listar_Tareas()';

  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });



});


app.post('/Tarea/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = " Call ta_sp_Insertar_Tareas(?,?,?,?,?,?,?,?,?,?);";


  connection.query(sql, [
    emp.nCas_Id, emp.cTar_Tab_Tipo, emp.cTar_Tab_Categoria, emp.cTar_Titulo, emp.nUsu_Asignado, emp.nUsu_Relacionado, emp.dTar_Fec_Vence, emp.tTar_Hora_Vence, emp.cTar_tab_recordar, emp.bTar_Estado


  ], error => {
    if (error) throw error;
    res.send('success');

  });
});


app.post('/Tarea/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = "Call ta_sp_Actualizar_Tareas (?,?,?,?,?,?,?,?,?,?,?)";
  connection.query(sql, [
    emp.nTar_Id, emp.nCas_Id, emp.cTar_Tab_Tipo, emp.cTar_Tab_Categoria, emp.cTar_Titulo, emp.nUsu_Asignado, emp.nUsu_Relacionado, emp.dTar_Fec_Vence, emp.tTar_Hora_Vence, emp.cTar_tab_recordar, emp.bTar_Estado


  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Tarea/Eliminar', (req, res) => {
  let emp = req.body;
  const sql = "Call ta_sp_Eliminar_Tareas(?)";

  connection.query(sql, [emp.nTar_Id], error => {
    if (error) throw error;
    res.send('success');
  });
});
/*--------------------- FIN TAREAS --------------------- */

/*--------------------- EVENTOS ------------------------ */

app.get('/Evento/Listar', cors(), (req, res) => {
  const sql = 'Call ev_sp_Listar_Eventos();';

  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});


app.post('/Evento/Dash', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];
  const sql = " Call ev_sp_Listar_EventosDashById(?);";
  connection.query(sql, data, error => {
    if (error) throw error;
    res.send('success');

  });
});




app.post('/Evento/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = " Call ev_sp_Insertar_Eventos(?,?,?,?,?,?,?,?,?,?,?,?,?);";


  connection.query(sql, [
    emp.nCas_Id,
    emp.cEve_Tab_Tipo,
    emp.cEve_Tab_Categoria,
    emp.cEve_Titulo,
    emp.cEve_Ubicacion,
    emp.cEve_Descripcion,
    emp.dEve_FechaIni,
    emp.dEve_FechaFin,
    emp.bEve_TodoDía,
    emp.tEve_HoraIni,
    emp.tEve_HoraFin,
    emp.cEve_tab_Recordar,
    emp.bEve_Estado
  ], error => {
    if (error) throw error;
    res.send('success');

  });
});

app.post('/Evento/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ev_sp_Actualizar_Eventos(?,?,?,?,?,?,?,?,?,?,?,?,?,?);';


  connection.query(sql, [
    emp.nEve_Id,
    emp.nCas_Id,
    emp.cEve_Tab_Tipo,
    emp.cEve_Tab_Categoria,
    emp.cEve_Titulo,
    emp.cEve_Ubicacion,
    emp.cEve_Descripcion,
    emp.dEve_FechaIni,
    emp.dEve_FechaFin,
    emp.bEve_TodoDía,
    emp.tEve_HoraIni,
    emp.tEve_HoraFin,
    emp.cEve_tab_Recordar,
    emp.bEve_Estado
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});






app.post('/Evento/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ev_sp_Eliminar_Eventos(?)`;

  connection.query(sql, [
    emp.nEve_Id
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});








/*--------------------- FIN EVENTOS -------------------- */


/*CLIENTES*/

app.get('/Cliente/Listar', cors(), (req, res) => {
  const sql = 'Call cli_sp_Listar_Cliente();';
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});




app.post('/Cliente/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Insetar_Cliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  connection.query(sql, [

    emp.nUsu_Propietario, emp.bCli_Tipo, emp.cCli_Tab_TipoDoc, emp.cCli_Num_Doc, emp.cCli_Num_DocOtro, emp.cCli_NombreCorto, emp.cCli_RazonSocial, emp.cCli_Materno, emp.nCli_Ubi, emp.cCli_Dirección, emp.cCli_DirecciónPostal, emp.cCli_Email1, emp.cCli_Email2, emp.cCli_Fono1, emp.cCli_Fono2, emp.cCli_Web, emp.cCli_ReferidoPor, emp.cCli_ConNombre, emp.cCli_ConApellidos, emp.cCli_ConCargo, emp.cCli_ConFono1, emp.cCli_ConFono2, emp.cCli_ConEmail1, emp.cCli_ConEmail2, emp.cCli_Tab_Prefijo, emp.cCli_Logo, emp.nCli_Estado
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('success');
      }
    });
});


app.post('/Evento/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cEve_Tab_Tipo,
    e.cEve_Tab_Categoria,
    e.FechaInicial,
    e.FechaFin,
    e.nUsu_ID,
  ];
  const sql = `Call ev_sp_Buscar_Eventos(?,?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});


app.post('/Tarea/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cTar_Tab_Tipo,
    e.cTar_Tab_Categoria,
    e.FechaInicial,
    e.FechaFin,
    e.nUsu_ID,
  ];
  const sql = `Call ta_sp_Buscar_Tareas(?,?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Ingreso/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cIng_Tab_Moneda,
    e.cIng_Tab_Tipo,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call ing_sp_Buscar_Ingreso(?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Laborado/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call lab_sp_Buscar_Laborado(?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Egreso/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cIng_Tab_Moneda,
    e.cEge_Tab_Tipo,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call eg_sp_Buscar_Egresos(?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});



app.post('/Cliente/Buscar/Filtro', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e.TipoCliente,
    e.TipoDoc,
  ];
  const sql = 'Call cli_sp_Buscar_Cliente(?,?);';
  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('No hay datos');
    }
  });
});

app.post('/Cliente/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Actualizar_Cliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  connection.query(sql, [
    emp.nCli_Id, emp.nUsu_Propietario, emp.bCli_Tipo, emp.cCli_Tab_TipoDoc, emp.cCli_Num_Doc, emp.cCli_Num_DocOtro, emp.cCli_NombreCorto, emp.cCli_RazonSocial, emp.cCli_Materno, emp.nCli_Ubi, emp.cCli_Dirección, emp.cCli_DirecciónPostal, emp.cCli_Email1, emp.cCli_Email2, emp.cCli_Fono1, emp.cCli_Fono2, emp.cCli_Web, emp.cCli_ReferidoPor, emp.cCli_ConNombre, emp.cCli_ConApellidos, emp.cCli_ConCargo, emp.cCli_ConFono1, emp.cCli_ConFono2, emp.cCli_ConEmail1, emp.cCli_ConEmail2, emp.cCli_Tab_Prefijo, emp.cCli_Logo, emp.nCli_Estado
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('success');
      }
    });
});


app.post('/Cliente/Buscar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cli_sp_Listar_ClientexId(?);';
  connection.query(sql, [
    emp.nClid_Id
  ], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});



app.post('/Cliente/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Eliminar_Cliente(?)`;
  connection.query(sql, [
    emp.nClid_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




/*FIN CLIENTES*/



/*CASOS*/

app.post('/Casos/DashUsuario', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/ListarCbo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_ListarCombo_Casos()';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Cliente/ListarCbo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call cli_sp_Listar_Combo_Cliente()';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Listar_Casos`;
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/findClient',(req,res)=>{
  const e = req.body
  const data = [e.idClient]
  console.log(data)
  const sql = 'CALL ca_sp_ListarCombo_CasosByClient(?)'
  connection.query(sql,data,(err,rows)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: 'No se encontro resultados'
        });
      } else {
        res.status(500).send({
          message: 'Error al recuperar resultados'
        });
      }
    } else res.send(rows[0]);
  })
})

app.post('/Casos/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Insertar_Casos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ,?,?,?)';
  connection.query(sql, [

    emp.nCli_Id
    , emp.nEqu_ID
    , emp.cCas_Tab_Tipo
    , emp.cCas_Titulo
    , emp.cCas_Cod_Externo
    , emp.cCas_Cod_Interno
    , emp.cCas_Tab_Materia
    , emp.cCas_Tab_SubMateria
    , emp.cCas_Detalle
    , emp.cCas_Tab_Jurisdiccion
    , emp.cCas_Expediente
    , emp.nCas_Exp_Year
    , emp.nJuz_Id
    , emp.cCas_Tab_Sala
    , emp.cCas_Tab_Recurso_Tipo
    , emp.cCas_Recurso_Num
    , emp.cCas_Tab_Distrito_Jud
    , emp.cCas_Tab_Comision
    , emp.cTab_Lugar_Tramite
    , emp.nPer_Id
    , emp.cCas_Referencia
    , emp.cCas_Denominacion
    , emp.dCas_Fec_Ini_Interno
    , emp.dCas_Fec_Ini_Procesal
    , emp.cCas_Tab_Estado_Externo
    , emp.dCas_Fec_Estima_Cierre
    , emp.cTab_Estado_Interno
    , emp.dFas_Fec_Cierre
    , emp.nUsu_ID
    , emp.dUsu_Fecha
    , emp.bCas_Estado
    , emp.cUbi_Org
    , emp.p_cTab_Moneda
    , emp.nCas_Monto
    , emp.cTab_MontoTipo


  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Actualizar_Casos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    emp.nCas_Id
    , emp.nCli_Id
    , emp.nEqu_ID
    , emp.cCas_Tab_Tipo
    , emp.cCas_Titulo
    , emp.cCas_Cod_Externo
    , emp.cCas_Cod_Interno
    , emp.cCas_Tab_Materia
    , emp.cCas_Tab_SubMateria
    , emp.cCas_Detalle
    , emp.cCas_Tab_Jurisdiccion
    , emp.cCas_Expediente
    , emp.nCas_Exp_Year
    , emp.nJuz_Id
    , emp.cCas_Tab_Sala
    , emp.cCas_Tab_Recurso_Tipo
    , emp.cCas_Recurso_Num
    , emp.cCas_Tab_Distrito_Jud
    , emp.cCas_Tab_Comision
    , emp.cTab_Lugar_Tramite
    , emp.nPer_Id
    , emp.cCas_Referencia
    , emp.cCas_Denominacion
    , emp.dCas_Fec_Ini_Interno
    , emp.dCas_Fec_Ini_Procesal
    , emp.cCas_Tab_Estado_Externo
    , emp.dCas_Fec_Estima_Cierre
    , emp.cTab_Estado_Interno
    , emp.dFas_Fec_Cierre
    , emp.nUsu_ID
    , emp.dUsu_Fecha
    , emp.bCas_Estado
    , emp.cUbi_Org
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Eliminar_Casos(?)';
  connection.query(sql, [
    emp.nCas_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Masivo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_cerrarMasivo_Casos(?)';
  connection.query(sql, [
    emp.nCas_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.get('/Casos/Listar2', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call _test_ca_List`;
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});
/*FIN CASOS*/

/*INSERTAR EQUIPO CASOS*/
app.get('/EqCasos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eqc_sp_Listar_EqCasos';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/EqCasos/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eqc_sp_Insertar_EqCasos(?,?,?)';
  connection.query(sql, [
    emp.nCas_Id, emp.nUsu_Id, emp.cCas_Tab_TipoEquipo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


/*FIN INSERTAR EQUIPO CASOS*/



/*USUARIOS*/


app.get('/Usuario/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Listar_Usuarios'
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});





app.post('/Usuario/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Insertar_Usuarios(?,?,?,?,?,?,?)';
  connection.query(sql, [
    emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Usuario/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call us_sp_Actualizar_Usuarios(?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    emp.nUsu_ID, emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Usuario/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Eliminar_Usuarios(?)';
  connection.query(sql, [
    emp.nUsu_ID

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Usuario/Login', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Login_Usuarios(?,?)';
  connection.query(sql, [
    emp.cUsu_email, emp.cUsu_Pass

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


/*FIN USUARIOS */


/* INGRESOS*/


app.get('/Ingresos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ing_sp_Listar_Ingreso()';
  connection.query(sql, [


  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Ingresos/Insertar', cors(), (req, res) => {
  let i = req.body;
  const sql = 'Call ing_sp_Insertar_Ingreso(?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [

    i.nCas_Id,
    i.nCue_Id,
    i.cIng_Tab_Tipo,
    i.cIng_Tab_FormaPago,
    i.cIng_Tab_Moneda,
    i.nIng_Monto,
    i.cIng_Detalle,
    i.dIng_Fecha,
    i.cIng_Num_Ope,
    i.bIng_Estado

  ],
    (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }

    });
});





app.post('/Ingreso/Actualizar', cors(), (req, res) => {

  let i = req.body;
  const sql = 'Call ing_sp_Actualizar_Ingreso(?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    i.nIng_Id,
    i.nCas_Id,
    i.nCue_Id,
    i.cIng_Tab_Tipo,
    i.cIng_Tab_FormaPago,
    i.cIng_Tab_Moneda,
    i.nIng_Monto,
    i.cIng_Detalle,
    i.dIng_Fecha,
    i.cIng_Num_Ope,
    i.bIng_Estado

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Ingreso/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ing_sp_Eliminar_Ingreso(?)';

  connection.query(sql, [
    emp.nIng_Id
  ]
    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});







/*INGRESOS*/
app.get('/Egresos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eg_sp_Listar_Egresos()';
  connection.query(sql, [


  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Egresos/Insertar', cors(), (req, res) => {

  let e = req.body;
  const sql = 'Call eg_sp_Insertar_Egresos(?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    e.nCas_Id,
    e.nCue_Id,
    e.nUsu_ID,
    e.cEge_Tab_Tipo,
    e.cEge_Tab_FormaPago,
    e.cIng_Tab_Moneda,
    e.nEge_Monto,
    e.cEge_Detalle,
    e.cEge_Archivo,
    e.dEge_Fecha,
    e.cEge_Num_Ope,
    e.bEge_Estado
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Evento/Buscar/Cliente', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ev_sp_Listar_EventosClientexId(?)';

  connection.query(sql, [emp.nCli_Id], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });

});


app.post('/Tarea/Buscar/Cliente', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ta_sp_Listar_TareasClientexId(?)';

  connection.query(sql, [emp.nCli_Id], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });

});

app.post('/Casos/Buscar/Cliente', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Listar_CasoClientexId(?)`;
  connection.query(sql, [
    emp.nCli_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Buscar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Buscar_Casos(?)`;
  connection.query(sql, [
    emp.nCas_Id
  ]
    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Egresos/Actualizar', cors(), (req, res) => {

  let e = req.body;
  const sql = 'Call eg_sp_Actualizar_Egresos(?,?,?,?,?,?,?,?,?,?,?,?,?)';

  connection.query(sql, [
    e.nEge_Id,
    e.nCas_Id,
    e.nCue_Id,
    e.nUsu_ID,
    e.cEge_Tab_Tipo,
    e.cEge_Tab_FormaPago,
    e.cIng_Tab_Moneda,
    e.nEge_Monto,
    e.cEge_Detalle,
    e.cEge_Archivo,
    e.dEge_Fecha,
    e.cEge_Num_Ope,
    e.bEge_Estado
  ]
    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Egresos/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eg_sp_Eliminar_Egresos(?)';

  connection.query(sql, [
    emp.nEge_Id
  ]
    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

/*EGRESOS*/

/* Cuenta banco*/
app.get('/CBanco/Listar/Combo', cors(), (req, res) => {
  let emp = req.body;

  const sql = ' call cb_sp_Listar_Combo';

  connection.query(sql, [], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0])
    } else {

      res.json('success')
    }

  })

})



/* 

LABORADO*/

app.post('/Lab/Listar/Estadisticas', cors(), (req, res) => {
  let e = req.body;
  let data = [e.id];
  let q = 'call lab_sp_Listar_Estadisticas(?)';
  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.json(result[0])
    } else if (result == undefined) {
      res.send(err)
    } else {
      res.send('error unknow: ', err);
    }
  })

})


app.post('/Lab/Listar/Metricas', cors(), (req, res) => {
  let e = req.body;
  let data = [e.id];
  let q = 'call lab_sp_Listar_Estadisticas(?)';
  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.json(result[0])
    } else if (result == undefined) {
      res.send(err)
    } else {
      res.send('error unknow: ', err);
    }
  })

})


app.get('/Lab/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call lab_sp_Listar_Laborado()';
  connection.query(sql, []

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




app.post('/Lab/Insertar', cors(), (req, res) => {

  let l = req.body;
  const sql = 'Call lab_sp_Insertar_Laborado(?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [


    l.nCas_Id,
    l.cLab_Tab_Tipo,
    l.nLab_Hora,
    l.dLab_Fecha,
    l.cLab_Detalle,
    l.nUsu_Id,
    l.bLab_PrecioFijo,
    l.tLab_HoraFacturable,
    l.cLab_Tab_Moneda,
    l.nLab_Total,
    l.bLab_Estado,
    l.nLab_TasaXHora

  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Lab/Actualizar', cors(), (req, res) => {

  let l = req.body;
  const sql = 'Call lab_sp_Actualizar_Laborado(?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [


    l.nLab_Id,
    l.nCas_Id,
    l.cLab_Tab_Tipo,
    l.nLab_Hora,
    l.dLab_Fecha,
    l.cLab_Detalle,
    l.nUsu_Id,
    l.bLab_PrecioFijo,
    l.tLab_HoraFacturable,
    l.cLab_Tab_Moneda,
    l.nLab_Total,
    l.bLab_Estado,
    l.nLab_TasaXHora
  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Lab/Eliminar', cors(), (req, res) => {

  let l = req.body;
  const sql = 'Call lab_sp_Eliminar_Laborado(?)';
  connection.query(sql, [

    l.nLab_Id,

  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});





/*FIN LABORADO*/

/*END EGRESOS */

app.get('/Equipo/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eq_sp_Listar_Equipo';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




app.post('/Av/Listar', cors(), (req, res) => {

  let e = req.body;

  let data = [e.expediente, e.anio, e.area, e.texpediente, e.tramite];

  let q = 'call usp_Detalles_Select()';

  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result) {
      res.json(result[0]);
    } else if (result == undefined) {
      res.send(err);
    }

  })

})

/* CUADERNOS*/
app.post('/Cua/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];
  const sql = 'Call cua_sp_ListarById(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Cua/Insertar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.nCas_Id, e.cCua_Codigo, cCua_Nombre];

  const sql = 'Call cua_sp_Insertar_Cuaderno(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Cua/Actualizar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.nCua_Id, e.nCas_Id, e.cCua_Codigo, cCua_Nombre];

  const sql = 'Call cua_sp_Actualizar_Cuaderno(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});





/* FIN CUADERNOS*/


/*DASHBOARD*/


/*DASH USUARIO*/


app.post('/Dashboard/Objetivo', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];

  const sql = 'Call lab_sp_Listar_ObjetivoByProceso(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Tareas', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ta_usp_Listar_MisTareas()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Dashboard/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Eventos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ev_sp_Listar_EventosDash()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});












/* FIN DASH USUARIO*/






/* Dash cliente */



app.post('/Dashboard/Cliente/Laborado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];

  const sql = 'Call lab_sp_Listar_Laborado_DashboardByCliente2(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Casos/Laborado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];

  const sql = 'Call lab_sp_Listar_Laborado_DashboardByCasos2(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Dashboard/Cliente/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];

  const sql = 'Call ca_sp_Listar_Casos_DashboardByCliente(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Test/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [e.tipoDocumento, e.documento, e.fechaLimite];

  const sql = 'Call usp_test_Insert(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Test/Select', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_test_Select()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/*Documentos */


app.post('/Docs/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.nCdo_Doc_Modo,
    e.cCdo_Tab_Tipo_Doc,
    e.dCdo_Fecha_Solicito,
    e.cCdo_Archivo,
    e.dCdo_Fech_Entrega,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
    e.cCdo_Documento


  ];

  console.log("datos obs: ", data);
  const sql = 'Call usp_Casos_Docs_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/List', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_Pendiente()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Docs/ListAprobado', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_Aprobado()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/SubirDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.cCdo_Archivo,
    e.dCdo_Fech_Entrega,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_SubirDoc(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/AprobarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_AprobarDoc(?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});





app.post('/Docs/RechazarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_RechazarDoc(?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/EliminarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_EliminarDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/RevertirDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_RevertirDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/EliminarDef', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_EliminarDefDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/ListRechazados', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_Rechazados()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/ListPorAprobar', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_PorAprobar()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/ListAprobado', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_Aprobado()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/ListEliminado', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_Casos_Docs_Select_Eliminado()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});






app.post('/CasMovs/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id, e.cCav_Detalle, e.cCav_Archivo, e.cCav_Notas

  ];

  console.log(data);

  const sql = 'Call cv_sp_Casos_Movs_Insertar(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/CasMovs/Update', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.p_Cav_Id, e.nCas_Id, e.cCav_Detalle, e.cCav_Archivo, e.cCav_Notas

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Movs_Update(?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/CasMovs/Select', cors(), (req, res) => {

  let e = req.body;
  let data = [

  ];

  console.log(data);

  const sql = 'Call cm_sp_Casos_Movs_Select()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/*Eliminación fisica por mientras - igual no funcionara por el fk*/
app.post('/CasMovs/Delete', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.p_Cav_Id
  ];

  console.log(data);

  const sql = 'Call cm_sp_Casos_Movs_Delete(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Ecuenta', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.ini,
    e.fin,
    e.mon

  ];

  console.log("Estado Cuenta :", data);

  const sql = 'Call _report_sp_EstadoCuenta(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});






app.post('/Reporte/Costos', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log(data);

  const sql = 'Call _report_sp_Costos(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Costos/Liq', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.state,

  ];

  console.log("data filtro liq ",data);

  const sql = 'Call _report_sp_CostosLiq(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Reporte/Costos/Proc/Liq', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log(data);

  const sql = 'Call _report_sp_Costos_Liq_Procesar(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Costos/Procesar', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.id

  ];

  console.log("procesar id:",data);

  const sql = 'Call _report_Procesar_Gastos(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Reporte/CostosHonorarios', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log(data);



  const sql = 'Call _report_sp_CostosHonorarios(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Ecuenta/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log("Total: ", data);



  const sql = 'Call _report_sp_EstadoCuenta_Total(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});






app.post('/Reporte/Costos/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.state

  ];

  console.log("total costos:", data);



  const sql = 'Call _report_sp_CostosCostosTotal(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Reporte/Honorarios/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log("total costos: ", data);



  const sql = 'Call _report_sp_CostosHonorariosTotal(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Tareas/DH', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_sp_Listar_Tareas_DH()`;
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Eventos/DH', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ev_sp_Listar_Eventos_DH()`;
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});



app.post('/Notif/Insert', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.nUsu_Id,
    e.nNot_Eventos,
    e.nNot_Casos,
    e.nNot_Tareas,
    e.nNot_CambioEstado,
    e.nNot_CambioDatos,
    e.nNot_Movimientos,
    e.nNot_Comentarios,
    e.nNot_DiasCongelado

  ]

  console.log(data)

  const sql = 'Call usp_Notificaciones_Insert(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Suscriptores/Listar', cors(), (req, res) => {

  let e = req.body;


  const sql = 'Call su_sp_Listar_Suscriptores()';
  connection.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Suscriptores/Buscar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.FechaInicial,
    e.FechaFin,
  ]

  const sql = 'Call su_sp_Buscar_Suscriptores(?,?)';
  connection.query(sql,data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Registrar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.cSus_Categoria,
    e.cSus_Nombres,
    e.cSus_Correo,
    e.cSus_Pass,
    e.cSus_Telefono,
    e.cSus_Empresa,
    e.cSus_CadenaConexion,
    e.cSus_Cuenta,
    e.bSus_Estado,
  ]

  console.log(data)

  const sql = 'Call su_sp_Insertar_Suscriptores(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Actualizar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.nSus_Id,
    e.cSus_Categoria,
    e.cSus_Nombres,
    e.dSus_FechaVencimiento,
    e.dSus_FechaCreacion,
    e.dSus_UltimoPago,
    e.cSus_Correo,
    e.cSus_Pass,
    e.cSus_Telefono,
    e.cSus_Empresa,
    e.cSus_CadenaConexion,
    e.cSus_Cuenta,
    e.bSus_Estado,
  ]

  console.log(data)

  const sql = 'Call su_sp_Actualizar_Suscriptores(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Eliminar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.nSus_Id,
  ]

  console.log(data)

  const sql = 'Call su_sp_Eliminar_Suscriptores(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/mail', cors(), (req, res) => {


  let e = req.body;

  let data = [
    e.email
  ]

  console.log(data);



  const transporter = nodemailer.createTransport({
    //host: "mail.dotnetsa.com",

    host:"mail.concursalperu.com",
    port: 465,
    secure: true,
    auth: {
      //C1@v€0bv1@
      //user: 'sistemas@dotnetsa.com',
      user: 'notificador@concursalperu.com',
      //C1@v€0bv1@
      pass: 'C1@v€0bv1@',
      //pass: 'C1@v€0bv1@',
    },
    tls: {
      rejectUnauthorized: 0
    }
  })

  var mailOptions = {
    from: '"Prubea" <notificador@concursalperu.com>',
    to: e.email,
    subject: 'GPS LEGAL',
    text: 'Nueva Notifiación ',
    html: '<b>GPS Legal </b><br> Novededades en GPS Legal<br /><img src="cid:uniq-mailtrap.png" alt="mailtrap" />',
    /*attachments: [
      {
        filename: 'mailtrap.png',
        path: __dirname + '/mailtrap.png',
        cid: 'uniq-mailtrap.png'
      }
    ]*/
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    res.send(true)
  });



  //const sql = 'Call usp_Notificaciones_Insert(?,?,?,?,?,?,?,?,?)';
  /*connection.query(sql,data, (error, result) => {
    if (error) throw error;
if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
  });*/
});



app.post('/Correo/List', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.email,
    e.info,


  ]

  console.log(data)

  const sql = 'Call cor_sp_Listar_Correo()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Casos/DataCEJ', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.id


  ]

  console.log(data)

  const sql = 'Call usp_Casos_Movs_Select(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.get('/file-example', function(req, res){

  const file = __dirname+'/baronrojo.jpg';
  res.sendFile(file); // Set disposition and send it.


  imageToBase64(file) // Path to the image
    .then(
        (response) => {
          var ResultadoIMG='data:image/png;base64,'+response;
            res.status(200).json({ img : resultadoIMG});
        }
    )
    .catch(
        (error) => {
            console.log(error); 
        }
    )

});







/* FIN Dash cliente */


connection.connect(error => {
  if (error) throw error;
  console.log('Server is running!');
});






app.listen(PORT, () => console.log(`Server on port : ${PORT}`));