const express = require('express');
const mysql = require('mysql');
var cors = require('cors');
const multer = require('multer');
const mimeType = require('mime-types');
const puppeteer = require('puppeteer');
//const bodyParser  = require('body-parser');
const path = require('path');
const fs = require('fs')
var Tesseract = require('tesseract.js')
var Jimp = require("jimp");
const nodemailer = require("nodemailer");
const moment = require("moment");
const _ = require('lodash');

const jsonwebtoken = require('jsonwebtoken');
const SECRET_KEY_JWT = 'secretkey5353';
const jwt = require('jsonwebtoken');

function createToken(user) {

  const payload = {
    id: user.nUsu_Id,
    email: user.cUsu_email,
    iat: moment().unix(),
    exp: moment().add(1, 'days').unix(),
  }
  return jsonwebtoken.sign({
    payload
  }, SECRET_KEY_JWT);
};

const PUBLISHABLE_KEY = "pk_test_51H7taUANQ3Xx5TzfzQYLqKiRQRopK6opzpDFgHh2T80YSf6VIl6s7P5ZmlVEDb4ep7qE9vvVmsGazW7wrH2Fowt100cB1fWZq7";
const SECRET_KEY = "sk_test_51H7taUANQ3Xx5Tzf6Q7UXUOwJviGZvUmOuUjIWr48KIMXv5RD7G84DJcLkMUqdIhQbfD7Oa2Ee9WEpcw53Xbmi3C00LJ49hbm3";
const stripe = require('stripe')(SECRET_KEY);

const imageToBase64 = require('image-to-base64');

const storage = multer.diskStorage({
  destination: './files',
  filename: function (req, file, cb) {
    cb("", file.originalname + Date.now() + "." + mimeType.extension(file.mimetype));

  }
})

const upload = multer({
  storage: storage
});
const corsOptions = {
  origin: '*',
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200
}

const bodyParser = require('body-parser');
const e = require('express');
const PORT = process.env.PORT || 9000;
const app = express();




const dbConfig = require("../config/db.config");

const connection = mysql.createConnection({

  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,


  multipleStatements: true,
  typeCast: function castField(field, useDefaultTypeCasting) {

    if ((field.type === "BIT") && (field.length === 1)) {

      var bytes = field.buffer();
      return (bytes[0] === 1);

    }

    return (useDefaultTypeCasting());

  }
});

if (!connection) {
  console.log("error en la base de datos")
} else {
  console.log("success")
}

app.use(cors(corsOptions));
app.use(bodyParser.json({
  limit: "50mb"
}));
app.use(bodyParser.urlencoded({
  limit: "50mb",
  extended: true,
  parameterLimit: 50000
}));

app.set("view engine", 'ejs')


app.use('/mail/event', cors(), require('./Routes/mail.route')); //Rt de envio de correos
app.use('/recorypass', cors(), require('./Routes/recoveryPass.route')); //Rt de envio de enlace para recuperar password
app.use('/changepass', cors(), require('./Routes/changePass.route')); //Rt de envio de enlace para recuperar password
app.use('/File/Upload', upload.single('avatar'), cors(), require('./Routes/fileUpload.route')); //Rt de subir archivos
app.use('/Tareas', cors(), require('./Routes/tareas.route')); //Rt de tareas


app.get('/', cors(), (req, res) => {
  res.send('hellow')
});

app.post('/File/Upload', upload.single('avatar'), cors(), (req, res) => {

  //console.log(req.file);

  console.log("nombre archivo: ", req.file.filename);


  let e = req.body;
  let data = [
    e.nCdo_Id,
    req.file.filename,
    '2021-06-06',
    0,
    '',
    1,
    '',
    0,
    '2021-01-01',
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_SubirDoc(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json({
        status: 1,
        name: req.file.filename
      });
    } else {
      res.json({
        status: 1,
        name: req.file.filename
      });
    }
  });




})

/*--------------------- TAREAS--------------------- */


app.post('/Tareas/Buscar', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_sp_Listar_TareaById(?)`;
  connection.query(sql, [e.id], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Tareas/Participantes', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ta_sp_Listar_Participantes()';
  connection.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Tareas/Dash/Usuario', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_usp_Listar_Dash_Usuario()`;
  connection.query(sql, [e.id], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});




app.post('/Login/App', cors(), (req, res) => {

  const user = {
    name: "Augusto",
    email: "juancarlos@gmail.com "

  }
  jwt.sign({
    user
  }, 'secretkey', {
    expiresIn: '3600s'
  }, (err, token) => {
    res.json({
      token
    })
  })

})

app.post("/api/post", verifyToken, (req, res) => {

  jwt.verify(req.token, 'secretkey', (err, data) => {
    if (err) {

      res.sendStatus(403)
    } else {

      ;
      res.json({
        data: data["user"]
      })

    }

  })
})

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization']
  if (typeof bearerHeader !== 'undefined') {
    const bearerToken = bearerHeader.split(" ")[1]
    req.token = bearerToken
    next()
  } else {
    res.sendStatus(403)
  }

};

app.post('/Maestros/Data', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_Combo_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Listar', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_sp_Listar_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/ListarCBO', cors(), (req, res) => {
  //res.header("Access-Control-Allow-Origin", "*");
  let emp = req.body;
  const sql = 'Call Ma_sp_Listar_cbo_Maestro()';
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Correlativo', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Correlativo_Maestro(?)';
  connection.query(sql, [emp.Prefijo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Insertar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Insertar_Maestro(?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabSimbolo,
    emp.cTabNombre
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Actualizar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Actualizar_Maestro(?,?,?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabSimbolo,
    emp.cTabNombre,
    emp.nTabValor,
    emp.nTabValor2,
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Actualizar/Abogado/Soles', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Actualizar_Maestro_Abogado_soles(?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabNombre,
    emp.nTabValor,
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/Actualizar/Abogado/Dolares', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Actualizar_Maestro_Abogado_Dolares(?,?,?)';
  connection.query(sql, [
    emp.cTabCodigo,
    emp.cTabNombre,
    emp.nTabValor,
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});





app.post('/Maestros/Eliminar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Eliminar_Maestro(?)';
  connection.query(sql, [emp.cTabCodigo], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/ls', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call  Ma_sp_Listar_Maestro_Abogados()';
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestros/ls/Abogado', cors(), (req, res) => {
  let emp = req.body;

  let opcion = emp.opc;
  console.log(opcion);

  var sql = '';

  if (opcion == 1) {
    sql = 'Call  Ma_sp_Listar_Maestro_Abogados_Dolares()';
  }
  else if (opcion == 0) {
    sql = 'Call  Ma_sp_Listar_Maestro_Abogados_Soles()';
  }

  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});




app.post('/Tarea/Listar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ta_sp_Listar_Tareas(?)';
  connection.query(sql, [emp.nUsu_ID], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
}); 


app.post('/Tarea/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = " Call ta_sp_Insertar_Tareas(?,?,?,?,?,?,?,?,?,?);";


  connection.query(sql, [
    emp.nCas_Id,
    emp.cTar_Tab_Tipo, 
    emp.cTar_Tab_Categoria, 
    emp.cTar_Titulo, 
    emp.nUsu_Asignado, 
    emp.nUsu_Relacionado, 
    emp.dTar_Fec_Vence, 
    emp.tTar_Hora_Vence, 
    emp.cTar_tab_recordar, 
    emp.bTar_Estado


  ], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
}); 

app.post('/Tarea/Insertar/Participante', cors(), (req, res) => {

  let emp = req.body;
  const sql = " Call ta_sp_Insertar_Participantes_Tareas(?,?);";


  connection.query(sql, [
    emp.nTar_Id, emp.nUsu_ID
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Tarea/Eliminar/Participante', cors(), (req, res) => {

  let emp = req.body;
  const sql = " Call ta_sp_Eliminar_Participantes_Tareas(?);";


  connection.query(sql, [
    emp.nTar_Id
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Tarea/Buscar/Participante', cors(), (req, res) => {
  let emp = req.body;

  const sql = 'Call ta_sp_Buscar_Participantes_Tareas(?)';
  connection.query(sql, [
    emp.nTar_Id
  ], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
}); 


app.post('/Tarea/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  console.log(emp);
  const sql = "Call ta_sp_Actualizar_Tareas (?,?,?,?,?,?,?,?,?,?,?)";
  connection.query(sql, [
    emp.nTar_Id, 
    emp.nCas_Id, 
    emp.cTar_Tab_Tipo, 
    emp.cTar_Tab_Categoria, 
    emp.cTar_Titulo, 
    emp.nUsu_Asignado, 
    emp.nUsu_Relacionado, 
    emp.dTar_Fec_Vence, 
    emp.tTar_Hora_Vence, 
    emp.cTar_tab_recordar, 
    emp.bTar_Estado


  ], error => {
    if (error) throw error;
    res.send('success');
  });
}); 

app.post('/Tarea/Eliminar', (req, res) => {
  let emp = req.body;
  const sql = "Call ta_sp_Eliminar_Tareas(?)";

  connection.query(sql, [emp.nTar_Id], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Tarea/ObtenerTarea', cors(), (req, res) => {
  let e = req.body;
  let data = [e._nTar_Id];
  const sql = 'Call ev_sp_obtenerTarea(?);';
  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
}); 

/*--------------------- FIN TAREAS --------------------- */

/*--------------------- EVENTOS ------------------------ */
app.post('/Evento/ObtenerEvento', cors(), (req, res) => {
  let e = req.body;
  let data = [e._nEve_Id];
  const sql = 'Call ev_sp_obtenerEvento(?);';
  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});


app.post('/Evento/Listar', cors(), (req, res) => {
  let e = req.body;
  let data = [e.nUsu_ID];
  const sql = 'Call ev_sp_Listar_Eventos(?);';
  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});

app.post('/Evento/Buscar/Participantes', cors(), (req, res) => {
  let e = req.body;
  let data = [e.nEve_Id];
  const sql = 'Call ev_sp_Buscar_Participantes_Eventos(?);';

  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});


app.post('/Evento/Dash', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];
  const sql = " Call ev_sp_Listar_EventosDashById(?);";
  connection.query(sql, data, error => {
    if (error) throw error;
    res.send('success');

  });
});




app.post('/Evento/Insertar', cors(), (req, res) => {

  let emp = req.body;
  console.log(emp);
  const sql = " Call ev_sp_Insertar_Eventos(?,?,?,?,?,?,?,?,?,?,?,?,?);";

  connection.query(sql, [
    emp.nCas_Id,
    emp.cEve_Tab_Tipo,
    emp.cEve_Tab_Categoria,
    emp.cEve_Titulo,
    emp.cEve_Ubicacion,
    emp.cEve_Descripcion,
    emp.dEve_FechaIni,
    emp.dEve_FechaFin,
    emp.bEve_TodoDía,
    emp.tEve_HoraIni,
    emp.tEve_HoraFin,
    emp.cEve_tab_Recordar,
    emp.cEve_Estado
  ], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});

app.post('/Evento/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  console.log(emp);
  const sql = 'Call ev_sp_Actualizar_Eventos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';

  connection.query(sql, [
    emp.nEve_Id,
    emp.nCas_Id,
    emp.cEve_Tab_Tipo,
    emp.cEve_Tab_Categoria,
    emp.cEve_Titulo,
    emp.cEve_Ubicacion,
    emp.cEve_Descripcion,
    emp.dEve_FechaIni,
    emp.dEve_FechaFin,
    emp.bEve_TodoDía,
    emp.tEve_HoraIni,
    emp.tEve_HoraFin,
    emp.cEve_tab_Recordar,
    emp.bEve_Estado,
    emp.cEve_Estado
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Evento/Insertar/Participante', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ev_sp_Insertar_Participantes_Eventos(?,?);';

  connection.query(sql, [
    emp.nEve_Id,
    emp.nUsu_ID,
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Evento/Actualizar/Participante', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ev_sp_Actualizar_Participantes_Eventos(?,?);';

  connection.query(sql, [
    emp.nEve_Id,
    emp.nUsu_ID,
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});

app.post('/Evento/Actualizar/ParticipanteMasivo', cors(), (req, res) => {
  let emp = req.body;

  var data = {
    nEve_Id: emp.nEve_Id,
    arrayParticipantes: emp.participantes
  }

  console.log("data");
  console.log(data);

  if (data.arrayParticipantes.length == 0) {
    res.send('success');
  } else {

    var i;
    for (i = 0; i < data.arrayParticipantes.length; i++) {

      const sql = 'Call ev_sp_Actualizar_Participantes_Eventos(?,?);';

      console.log(data.nEve_Id);
      console.log(data.arrayParticipantes[i].value);

      connection.query(sql, [
        data.nEve_Id,
        data.arrayParticipantes[i].value,
      ], (error, results) => {
        if (error) throw error;

        console.log(results);

      });
    }


    if (i == data.arrayParticipantes.length) {
      console.log("dsdd");
      res.send('success');
    }


  }

});

app.post('/Evento/Eliminar/Participante', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ev_sp_Eliminar_Participantes_Eventos(?);';

  connection.query(sql, [
    emp.nEve_Id,
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});


app.post('/Correo/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call co_sp_Insertar_Correo(?,?,?,?);';

  connection.query(sql, [
    emp.nCas_Id,
    emp.nUsu_Id,
    emp.cCor_Tab_Tipo,
    emp.cCor_Asunto,
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});



app.post('/Evento/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ev_sp_Eliminar_Eventos(?)`;

  connection.query(sql, [
    emp.nEve_Id
  ], error => {
    if (error) throw error;
    res.send('success');
  });
});








/*--------------------- FIN EVENTOS -------------------- */


/*CLIENTES*/

app.get('/Cliente/Listar', cors(), (req, res) => {
  const sql = 'Call cli_sp_Listar_Cliente();';
  connection.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});

app.post('/Cliente/Insertar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Insetar_Cliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  connection.query(sql, [

    emp.nUsu_Propietario, emp.bCli_Tipo, emp.cCli_Tab_TipoDoc, emp.cCli_Num_Doc, emp.cCli_Num_DocOtro, emp.cCli_NombreCorto, emp.cCli_RazonSocial, emp.cCli_Materno, emp.nCli_Ubi, emp.cCli_Dirección, emp.cCli_DirecciónPostal, emp.cCli_Email1, emp.cCli_Email2, emp.cCli_Fono1, emp.cCli_Fono2, emp.cCli_Web, emp.cCli_ReferidoPor, emp.cCli_ConNombre, emp.cCli_ConApellidos, emp.cCli_ConCargo, emp.cCli_ConFono1, emp.cCli_ConFono2, emp.cCli_ConEmail1, emp.cCli_ConEmail2, emp.cCli_Tab_Prefijo, emp.cCli_Logo, emp.nCli_Estado
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('success');
      }
    });
});

app.post('/Cliente/UltimoRegistrado', cors(), (req, res) => {
  const sql = `Call cli_sp_UltimoCliente()`;
  connection.query(sql
    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      }
    });
});



app.post('/Evento/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cEve_Tab_Tipo,
    e.cEve_Tab_Categoria,
    e.FechaInicial,
    e.FechaFin,
    e.nUsu_ID,
    e.Estado,
  ];
  const sql = `Call ev_sp_Buscar_Eventos(?,?,?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});


app.post('/Tarea/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cTar_Tab_Tipo,
    e.cTar_Tab_Categoria,
    e.FechaInicial,
    e.FechaFin,
    e.nUsu_ID,
  ];
  const sql = `Call ta_sp_Buscar_Tareas(?,?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Ingreso/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cIng_Tab_Moneda,
    e.cIng_Tab_Tipo,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call ing_sp_Buscar_Ingreso(?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Laborado/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call lab_sp_Buscar_Laborado(?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});

app.post('/Egreso/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id,
    e.cIng_Tab_Moneda,
    e.cEge_Tab_Tipo,
    e.FechaInicial,
    e.FechaFin,
  ];
  const sql = `Call eg_sp_Buscar_Egresos(?,?,?,?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('empty');
    }
  });
});



app.post('/Cliente/Buscar/Filtro', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e.TipoCliente,
    e.TipoDoc,
  ];
  const sql = 'Call cli_sp_Buscar_Cliente(?,?);';
  connection.query(sql, data, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
      console.log(sql,
        data
      )
    } else {
      res.send('No hay datos');
    }
  });
});

app.post('/Cliente/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Actualizar_Cliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  connection.query(sql, [
    emp.nCli_Id, emp.nUsu_Propietario, emp.bCli_Tipo, emp.cCli_Tab_TipoDoc, emp.cCli_Num_Doc, emp.cCli_Num_DocOtro, emp.cCli_NombreCorto, emp.cCli_RazonSocial, emp.cCli_Materno, emp.nCli_Ubi, emp.cCli_Dirección, emp.cCli_DirecciónPostal, emp.cCli_Email1, emp.cCli_Email2, emp.cCli_Fono1, emp.cCli_Fono2, emp.cCli_Web, emp.cCli_ReferidoPor, emp.cCli_ConNombre, emp.cCli_ConApellidos, emp.cCli_ConCargo, emp.cCli_ConFono1, emp.cCli_ConFono2, emp.cCli_ConEmail1, emp.cCli_ConEmail2, emp.cCli_Tab_Prefijo, emp.cCli_Logo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('success');
      }
    });
});


app.post('/Cliente/Buscar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cli_sp_Listar_ClientexId(?);';
  connection.query(sql, [
    emp.nClid_Id
  ], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });
});



app.post('/Cliente/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call cli_sp_Eliminar_Cliente(?)`;
  connection.query(sql, [
    emp.nCli_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




/*FIN CLIENTES*/



/*CASOS*/

app.post('/Casos/DashUsuario', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/ListarCbo', cors(), (req, res) => {
  const e = req.body
  const data = [e.idClient]
  const sql = 'Call ca_sp_ListarCombo_CasosByClient(?)';


  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);


    } else {
      res.send('success');
    }
  });
});

app.post('/Casos/Filtro', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Filtro_Casos(?,?,?,?,?,?,?)`;
  connection.query(sql, [
    emp.nCli_Id,
    emp.cCas_Tab_Materia,
    emp.cCas_Tab_SubMateria,
    emp.bCas_Estado,
    emp.nUsu_ID,
    emp.FechaInicial,
    emp.FechaFin
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      console.log(emp.FechaInicial)
      console.log(emp.FechaFin)
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Cliente/ListarCbo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call cli_sp_Listar_Combo_Cliente(?)';
  connection.query(sql, [emp.bCli_Tipo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
        console.log(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Listar_Casos(?)`;

  connection.query(sql, [
    emp.nUsu_ID,
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Listar/Desactivados', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Listar_Casos_Desactivados(?)`;
  connection.query(sql, [
    emp.nUsu_ID,
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Casos/findClient', (req, res) => {
  const e = req.body
  const data = [e.idClient]
  console.log(data)
  const sql = 'CALL ca_sp_ListarCombo_CasosByClient(?)'
  connection.query(sql, data, (err, rows) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: 'No se encontro resultados'
        });
      } else {
        res.status(500).send({
          message: 'Error al recuperar resultados'
        });
      }
    } else res.send(rows[0]);
  })
})

app.post('/Casos/Insertar', cors(), (req, res) => {

  let c = req.body;
  const sql = 'Call ca_sp_Insertar_Casos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [

    c.nCli_Id,
    c.nEqu_ID,
    c.cCas_Tab_Tipo,
    c.cCas_Titulo,
    c.cCas_Cod_Externo,
    c.cCas_Cod_Interno,
    c.cCas_Tab_Materia,
    c.cCas_Tab_SubMateria,
    c.cCas_Detalle,
    c.cCas_Tab_Jurisdiccion,
    c.cCas_Expediente,
    c.nCas_Exp_Year,
    c.nJuz_Id,
    c.cCas_Tab_Sala,
    c.cCas_Tab_Recurso_Tipo,
    c.cCas_Recurso_Num,
    c.cCas_Tab_Distrito_Jud,
    c.cCas_Tab_Comision,
    c.cTab_Lugar_Tramite,
    c.nPer_Id,
    c.cCas_Referencia,
    c.cCas_Denominacion,
    c.dCas_Fec_Ini_Interno,
    c.dCas_Fec_Ini_Procesal,
    c.cCas_Tab_Estado_Externo,
    c.dCas_Fec_Estima_Cierre,
    c.cTab_Estado_Interno,
    c.dFas_Fec_Cierre,
    c.nUsu_ID,
    c.dUsu_Fecha,
    c.bCas_Estado,
    c.cUbi_Org,
    c.cTab_Tipo_Acto,
    c.cTab_Moneda,
    c.nCas_Monto,
    c.cTab_Tipo_Monto,
    c.cName_Resolucion,
    c.nCas_Origen,
    c.nCas_HonorarioFijo,
    c.nCas_HonorarioExito,
    c.cCas_Titulo1,
    c.cTab_MonedaHF,
    c.cTab_MonedaHE,
    c.nCas_URPs
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/IDCASO', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_FiltroIdCaso(?)`;
  connection.query(sql, [
    emp.cCas_Cod_Externo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Actualizar', cors(), (req, res) => {

  let c = req.body;
  let data = [ c.nCas_Id,
    c.nCli_Id,
    c.nEqu_ID,
    c.cCas_Titulo,
    c.cCas_Cod_Externo,
    c.cCas_Cod_Interno,
    c.cCas_Tab_Materia,
    c.cCas_Tab_SubMateria,
    c.cCas_Detalle,
    c.cCas_Tab_Jurisdiccion,
    c.cCas_Expediente,
    c.nCas_Exp_Year,
    c.nJuz_Id,
    c.cCas_Tab_Sala,
    c.cCas_Tab_Recurso_Tipo,
    c.cCas_Recurso_Num,
    c.cCas_Tab_Distrito_Jud,
    c.cCas_Tab_Comision,
    c.cTab_Lugar_Tramite,
    c.nPer_Id,
    c.cCas_Referencia,
    c.cCas_Denominacion,
    c.dCas_Fec_Ini_Interno,
    c.dCas_Fec_Ini_Procesal,
    c.cCas_Tab_Estado_Externo,
    c.dCas_Fec_Estima_Cierre,
    c.cTab_Estado_Interno,
    c.dFas_Fec_Cierre,
    c.nUsu_ID,
    c.dUsu_Fecha,
    c.cUbi_Org,
    c.cTab_Tipo_Acto,
    c.cTab_Moneda,
    c.nCas_Monto,
    c.cTab_Tipo_Monto,

    c.cName_Resolucion,
    c.nCas_Origen,
    c.nCas_HonorarioFijo,
    c.nCas_HonorarioExito,
    c.cCas_Titulo1,
    c.cTab_MonedaHF,
    c.cTab_MonedaHE,
    c.nCas_URPs]
  const sql = 'Call ca_sp_Actualizar_Casos(?)';
  connection.query(sql, [
    data
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
        console.log(sql)
      } else {
        res.send('success');
        
      }
    });
});

app.post('/Casos/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_Eliminar_Casos(?)';
  connection.query(sql, [
    emp.nCas_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Equipo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eq_sp_Listar_Usuarios(?,?)';
  connection.query(sql, [
    emp.nCas_Id,
    emp.Tipo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        console.log(emp.nCas_Id)
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Masivo', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ca_sp_cerrarMasivo_Casos(?)';
  connection.query(sql, [
    emp.nCas_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.get('/Casos/Listar2', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call _test_ca_List`;
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});
/*FIN CASOS*/

/*INSERTAR EQUIPO CASOS*/
app.get('/EqCasos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eqc_sp_Listar_EqCasos';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/EqCasos/Insertar', cors(), (req, res) => {

  let emp = req.body;
  console.log("Cuerpo de eq casos");
  console.log(emp);

  const sql = 'Call eqc_sp_Insertar_EqCasos(?,?,?,?,?)';
  connection.query(sql, [
    emp.nCas_Id, emp.nUsu_Id, emp.cCas_Tab_TipoEquipo, emp.cCas_TabMoneda, emp.nCas_Monto
  ], (error, result) => {
    if (error) throw error;

    console.log(result);
    res.send(result);

  });
});


app.post('/EqCasos/Masivo/Insertar', cors(), (req, res) => {

  let emp = req.body;
  let arrayUsuarios = emp.nUsu_Id;

  console.log("xddddddddddddddddddddddddddd");
  console.log(arrayUsuarios);

  if (arrayUsuarios.length > 0) {
    for (let i = 0; i < arrayUsuarios.length; i++) {

      let data = [emp.nCas_Id, arrayUsuarios[i].value, emp.cCas_Tab_TipoEquipo, emp.cCas_TabMoneda, emp.nCas_Monto]

      const sql = 'Call eqc_sp_Insertar_EqCasos(?,?,?,?,?)';
      connection.query(sql, data, (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          res.json(result[0]);
        } else {
          res.send('success');
        }
      });
    }
  } else {
    res.status(400);
  }


});

app.post('/EqCasos/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eqc_sp_Eliminar_EqCasos(?)';
  connection.query(sql, [
    emp.nCas_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        console.log("Id eliminar", emp.nCas_Id)
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/EqCasosTpoParticipantes/Eliminar', cors(), (req, res) => {

  let emp = req.body;

  const sql = 'Call eqc_sp_Eliminar_EqCasos_tpoPart(?,?)';
  connection.query(sql, [
    emp.nCas_Id, emp.cCas_Tab_TipoEquipo
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        console.log("Id eliminar", emp.nCas_Id)
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


/*FIN INSERTAR EQUIPO CASOS*/



/*USUARIOS*/


app.get('/Usuario/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Listar_Usuarios()'
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Usuario/Perfil', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Perfil_Usuarios(?)'
  connection.query(sql, [
    emp.nUsu_ID
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Usuario/Acceso', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ua_sp_Listar_UsuarioAcceso(?)'
  connection.query(sql, [
    emp.nUsu_ID
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Usuario/Insertar', cors(), (req, res) => {

  var emp = req.body;

  const sql = `SELECT * FROM Usuarios where cUsu_Login=? and bUsu_Activo=1`;// aregar estado
  connection.query(sql, [emp.cUsu_Login], (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json({
        status: 501  // colocar el codigo para informar de login existente
      });
    } else {


      const sql = 'Call usu_sp_Insertar_Usuarios(?,?,?,?,?,?,?)';
      connection.query(sql, [
        emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

      ]

        , (error, result) => {
          if (error) throw error;

          if (result.length > 0) {
            res.json(result[0]);
          } else {
            res.send('success');
          }
        });

    }
  });

});


app.post('/Usuario/Actualizar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call us_sp_Actualizar_Usuarios(?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    emp.nUsu_ID, emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Usuario/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call usu_sp_Eliminar_Usuarios(?)';
  connection.query(sql, [
    emp.nUsu_ID

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Usuario/Login', cors(), (req, res) => {

  var emp = req.body;
  const sql = 'Call usu_sp_Login_Usuarios(?,?)';
  connection.query(sql, [
    emp.cUsu_email, emp.cUsu_Pass
  ], (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      var obj = JSON.parse(JSON.stringify(result[0]));

      if (obj[0].ID == null) {
        const sql = `select * 
                      from Usuarios 
                      where cUsu_email=? and cUsu_Pass=?`;
        connection.query(sql, [emp.cUsu_email, emp.cUsu_Pass], (error, result) => {
          if (error) throw error;
          if (result.length > 0) {
            console.log(result[0]);
            var resobj = JSON.parse(JSON.stringify(result[0]));
            console.log(resobj);
            let objRes = [
              RowDataPacket = {
                CONDICION: 1,
                ID: resobj.nUsu_Id,
                NombreC: resobj.cUsu_Nombres,
                Correo: resobj.cUsu_email,
              }
            ]
            console.log("1", objRes);
            res.json(objRes);

          } else {
            console.log("2", result[0]);
            res.send('success');
          }
        });

      } else {

        let objRes = [
          RowDataPacket = {
            CONDICION: 1,
            ID: result[0][0].ID,
            NombreC: result[0][0].NombreC,
            Correo: result[0][0].Correo,
          }
        ]
        res.json(objRes);
      }
    } else {
      res.send('success');
    }
  });
});

app.post('/Admin/Login', cors(), (req, res) => {

  var emp = req.body;
  const sql = 'Call Adm_sp_Login_Admarios(?,?)';
  connection.query(sql, [
    emp.cUsu_email, emp.cUsu_Pass
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      console.log(result[0]);

      var obj = JSON.parse(JSON.stringify(result[0]));
      console.log(obj[0].ID);

      if (obj[0].ID == null) {
        console.log("vacio");
        const sql = `select * 
                      from Usuarios 
                      where cUsu_email=? and cUsu_Pass=?`;
        connection.query(sql, [emp.cUsu_email, emp.cUsu_Pass], (error, result) => {
          if (error) throw error;
          if (result.length > 0) {
            console.log(result[0]);
            var resobj = JSON.parse(JSON.stringify(result[0]));
            console.log(resobj);
            let objRes = [
              RowDataPacket = {
                CONDICION: 1,
                ID: resobj.nUsu_Id,
                NombreC: resobj.cUsu_Nombres,
                Correo: resobj.cUsu_email,
              }
            ]

            console.log("encontrado por email");
            console.log(objRes);
            res.json(objRes);

          } else {
            console.log("no encontro usuario segundo nivel");
            console.log(result[0]);
            res.send('success');
          }
        });

      } else {
        console.log("encontro el usuario[");
        res.json(result[0]);
      }
    } else {

    }
  });
});


app.post('/Validar/LoginEstado', cors(), (req, res) => {

  var emp = req.body;

  const sql = 'Call usu_sp_validarLoginEstado(?)';
  connection.query(sql, [
    emp._nUsu_Id
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(result[0]);
    } else {
      res.send('success');
    }
  });

});


/*FIN USUARIOS */


/* INGRESOS*/


app.post('/Ingresos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ing_sp_Listar_Ingreso(?)';
  connection.query(sql, [
    emp.nUsu_ID
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Ingresos/Insertar', cors(), (req, res) => {
  let i = req.body;
  const sql = 'Call ing_sp_Insertar_Ingreso(?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [

    i.nCas_Id,
    i.nCue_Id,
    i.cIng_Tab_Tipo,
    i.cIng_Tab_FormaPago,
    i.cIng_Tab_Moneda,
    i.nIng_Monto,
    i.cIng_Detalle,
    i.dIng_Fecha,
    i.cIng_Num_Ope,
    i.bIng_Estado

  ],
    (error, result) => {
      if (error) throw error;

      console.log(result);
      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.json(result[0]);
      }
    });
});


app.post('/Ingreso/Actualizar', cors(), (req, res) => {

  let i = req.body;
  const sql = 'Call ing_sp_Actualizar_Ingreso(?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    i.nIng_Id,
    i.nCas_Id,
    i.nCue_Id,
    i.cIng_Tab_Tipo,
    i.cIng_Tab_FormaPago,
    i.cIng_Tab_Moneda,
    i.nIng_Monto,
    i.cIng_Detalle,
    i.dIng_Fecha,
    i.cIng_Num_Ope,
    i.bIng_Estado

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Ingreso/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call ing_sp_Eliminar_Ingreso(?)';

  connection.query(sql, [
    emp.nIng_Id
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});







/*INGRESOS*/
app.post('/Egresos/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eg_sp_Listar_Egresos(?)';
  connection.query(sql, [
    emp.nUsu_ID
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Egresos/Insertar', cors(), (req, res) => {

  let e = req.body;
  const sql = 'Call eg_sp_Insertar_Egresos(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [
    e.nCas_Id,
    e.nCue_Id,
    e.nUsu_ID,
    e.cEge_Tab_Tipo,
    e.cEge_Tab_FormaPago,
    e.cIng_Tab_Moneda,
    e.dEge_TCambio,
    e.nEge_Monto,
    e.cEge_Detalle,
    e.cEge_Archivo,
    e.dEge_Fecha,
    e.cEge_Num_Ope,
    e.bEge_Estado
  ], (error, result) => {
    if (error) throw error;

    console.log(result);
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.json(result[0]);
    }

  });
});



app.post('/Evento/Buscar/Cliente', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ev_sp_Listar_EventosClientexId(?)';

  connection.query(sql, [emp.nCli_Id], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });

});


app.post('/Tarea/Buscar/Cliente', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call ta_sp_Listar_TareasClientexId(?)';

  connection.query(sql, [emp.nCli_Id], (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.json(results[0]);
    } else {
      res.send('No hay datos');
    }
  });

});

app.post('/Casos/Buscar/Cliente', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Listar_Casos_DashboardByCliente(?,?)`;
  connection.query(sql, [
    emp.nCli_Id, emp.nUsu_Id
  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});

app.post('/Casos/Buscar', cors(), (req, res) => {

  let emp = req.body;
  const sql = `Call ca_sp_Buscar_Casos(?)`;
  connection.query(sql, [
    emp.nCas_Id
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Egresos/Actualizar', cors(), (req, res) => {

  let e = req.body;
  const sql = 'Call eg_sp_Actualizar_Egresos(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

  connection.query(sql, [
    e.nEge_Id,
    e.nCas_Id,
    e.nCue_Id,
    e.nUsu_ID,
    e.cEge_Tab_Tipo,
    e.cEge_Tab_FormaPago,
    e.cIng_Tab_Moneda,
    e.dEge_TCambio,
    e.nEge_Monto,
    e.cEge_Detalle,
    e.cEge_Archivo,
    e.dEge_Fecha,
    e.cEge_Num_Ope,
    e.bEge_Estado
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Egresos/Eliminar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eg_sp_Eliminar_Egresos(?)';

  connection.query(sql, [
    emp.nEge_Id
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/*EGRESOS*/

/* Cuenta banco*/
app.get('/CBanco/Listar/Combo', cors(), (req, res) => {
  let emp = req.body;

  const sql = ' call cb_sp_Listar_Combo';

  connection.query(sql, [], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0])
    } else {

      res.json('success')
    }

  })

})



/* 

LABORADO*/

app.post('/Lab/Listar/Estadisticas', cors(), (req, res) => {
  let e = req.body;
  let data = [e.id];
  let q = 'call lab_sp_Listar_Estadisticas(?)';
  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.json(result[0])
    } else if (result == undefined) {
      res.send(err)
    } else {
      res.send('error unknow: ', err);
    }
  })

})


app.post('/Lab/Listar/Metricas', cors(), (req, res) => {
  let e = req.body;
  let data = [e.id];
  let q = 'call lab_sp_Listar_Estadisticas(?)';
  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.json(result[0])
    } else if (result == undefined) {
      res.send(err)
    } else {
      res.send('error unknow: ', err);
    }
  })

})


app.get('/Lab/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call lab_sp_Listar_Laborado()';
  connection.query(sql, []

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




app.post('/Lab/Insertar', cors(), (req, res) => {

  let l = req.body;
  const sql = 'Call lab_sp_Insertar_Laborado(?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [


    l.nCas_Id,
    l.cLab_Tab_Tipo,
    l.nLab_Hora,
    l.dLab_Fecha,
    l.cLab_Detalle,
    l.nUsu_Id,
    l.bLab_PrecioFijo,
    l.tLab_HoraFacturable,
    l.cLab_Tab_Moneda,
    l.nLab_Total,
    l.bLab_Estado,
    l.nLab_TasaXHora

  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});



app.post('/Lab/Actualizar', cors(), (req, res) => {

  console.log(req.body);

  let l = req.body;
  const sql = 'Call lab_sp_Actualizar_Laborado(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, [


    l.nLab_Id,
    l.nCas_Id,
    l.cLab_Tab_Tipo,
    l.nLab_Hora,
    l.dLab_Fecha,
    l.cLab_Detalle,
    l.nUsu_Id,
    l.bLab_PrecioFijo,
    l.tLab_HoraFacturable,
    l.cLab_Tab_Moneda,
    l.nLab_Total,
    l.bLab_Estado,
    l.nLab_TasaXHora

  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});


app.post('/Lab/Eliminar', cors(), (req, res) => {

  let l = req.body;
  const sql = 'Call lab_sp_Eliminar_Laborado(?)';
  connection.query(sql, [

    l.nLab_Id,

  ]


    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});





/*FIN LABORADO*/

/*END EGRESOS */

app.get('/Equipo/Listar', cors(), (req, res) => {

  let emp = req.body;
  const sql = 'Call eq_sp_Listar_Equipo';
  connection.query(sql, [

  ]

    , (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
    });
});




app.post('/Av/Listar', cors(), (req, res) => {

  let e = req.body;

  let data = [e.expediente, e.anio, e.area, e.texpediente, e.tramite];

  let q = 'call usp_Detalles_Select()';

  connection.query(q, data, (err, result) => {
    if (err) throw err;
    if (result) {
      res.json(result[0]);
    } else if (result == undefined) {
      res.send(err);
    }

  })

})

/* CUADERNOS*/
app.post('/Cua/Buscar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];
  const sql = 'Call cua_sp_ListarById(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Cua/Insertar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.nCas_Id, e.cCua_Codigo, cCua_Nombre];

  const sql = 'Call cua_sp_Insertar_Cuaderno(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Cua/Actualizar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.nCua_Id, e.nCas_Id, e.cCua_Codigo, cCua_Nombre];

  const sql = 'Call cua_sp_Actualizar_Cuaderno(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});





/* FIN CUADERNOS*/


/*DASHBOARD*/


/*DASH USUARIO*/


app.post('/Dashboard/Objetivo', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id];

  const sql = 'Call lab_sp_Listar_ObjetivoByProceso(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Tareas', cors(), (req, res) => {

  let e = req.body;
  let data = [e.nUsu_ID];

  const sql = 'Call ta_usp_Listar_MisTareas(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Dashboard/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ca_sp_Listar_CasosByDashUsuario()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Eventos', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call ev_sp_Listar_EventosDash()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});












/* FIN DASH USUARIO*/






/* Dash cliente */



app.post('/Dashboard/Cliente/Laborado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id, e.M, e.Y];

  const sql = 'Call lab_sp_Listar_Laborado_DashboardByCliente2(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Dashboard/Casos/Laborado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id, e.M, e.Y];

  const sql = 'Call lab_sp_Listar_Laborado_DashboardByCasos2(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Dashboard/Cliente/Casos', cors(), (req, res) => {

  let e = req.body;
  let data = [e.id, e.nUsu_Id];

  const sql = 'Call ca_sp_Listar_Casos_DashboardByCliente(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Test/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [e.tipoDocumento, e.documento, e.fechaLimite];

  const sql = 'Call usp_test_Insert(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});




app.post('/Test/Select', cors(), (req, res) => {

  let e = req.body;
  let data = [];

  const sql = 'Call usp_test_Select()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/*Documentos */


app.post('/Docs/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.nCdo_Doc_Modo,
    e.cCdo_Tab_Tipo_Doc,
    e.dCdo_Fecha_Solicito,
    e.cCdo_Archivo,
    e.dCdo_Fech_Entrega,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
    e.cCdo_Documento


  ];

  console.log("datos obs: ", data);
  const sql = 'Call usp_Casos_Docs_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/List', cors(), (req, res) => {

  let e = req.body;
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_Pendiente(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/ListAprobado', cors(), (req, res) => {

  let e = req.body;
  console.log(e)
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_Aprobado(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/SubirDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.cCdo_Archivo,
    e.dCdo_Fech_Entrega,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_SubirDoc(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/AprobarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_AprobarDoc(?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/RechazarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,
    e.nCdo_Pendiente,
    e.cCdo_NotaPendiente,
    e.nCdo_PorAprobar,
    e.cCdo_NotaPorAprobar,
    e.nCdo_Aprobado,
    e.dCdo_Fecha_Aprobo,
  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_RechazarDoc(?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/EliminarDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_EliminarDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/RevertirDoc', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_RevertirDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/EliminarDef', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCdo_Id,

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Docs_EliminarDefDoc(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/ListRechazados', cors(), (req, res) => {

  let e = req.body;
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_Rechazados(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/ListPorAprobar', cors(), (req, res) => {

  let e = req.body;
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_PorAprobar(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Docs/ListAprobado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_Aprobado(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Docs/ListEliminado', cors(), (req, res) => {

  let e = req.body;
  let data = [e.client, e.case];

  const sql = 'Call usp_Casos_Docs_Select_Eliminado(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Juzgado/NProcesos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
  ]

  const sql = 'Call report_Analitico_Juzgado(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Juzgado/NProcesos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
  ]

  const sql = 'Call report_Analitico_Juzgado(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Materia/NProcesos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
  ]

  const sql = 'Call report_Analitico_Materia(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Materia/Ingresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Materia_Ingresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Juzgado/Ingresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Juzgado_Ingresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Analitico/Cliente/Ingresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Cliente_Ingresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Analitico/SubMateria/Ingresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_SubMateria_Ingresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Materia/Egresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Materia_Egresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Analitico/Juzgado/Egresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Juzgado_Egresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/Cliente/Egresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_Cliente_Egresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/SubMateria/Egresos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
    e.Moneda,
  ]

  const sql = 'Call report_Analitico_SubMateria_Egresos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Analitico/Cliente/NProcesos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
  ]

  const sql = 'Call report_Analitico_Cliente(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Analitico/SubMateria/NProcesos', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.ParmGroup,
  ]

  const sql = 'Call report_Analitico_SubMateria(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/CasMovs/Insert', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id, e.cCav_Detalle, e.cCav_Archivo, e.cCav_Notas

  ];

  console.log(data);

  const sql = 'Call cv_sp_Casos_Movs_Insertar(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/CasMovs/Update', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.p_Cav_Id, e.nCas_Id, e.cCav_Detalle, e.cCav_Archivo, e.cCav_Notas

  ];

  console.log(data);

  const sql = 'Call usp_Casos_Movs_Update(?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/CasMovs/Select', cors(), (req, res) => {

  let e = req.body;
  let data = [

  ];

  console.log(data);

  const sql = 'Call cm_sp_Casos_Movs_Select()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/*Eliminación fisica por mientras - igual no funcionara por el fk*/
app.post('/CasMovs/Delete', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.p_Cav_Id
  ];

  console.log(data);

  const sql = 'Call cm_sp_Casos_Movs_Delete(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Ecuenta', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.ini,
    e.fin,
    e.mon

  ];

  console.log("Estado Cuenta :", data);

  const sql = 'Call _report_sp_EstadoCuenta(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Costos', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log(data);

  const sql = 'Call _report_sp_Costos(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Costos/Liq', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.CodigoLiq,

  ];

  console.log("data filtro liq ", data);

  const sql = 'Call _report_sp_CostosLiq(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

var expresionRegular = /\s*;\s*/;

app.post('/Vincular/CEJ/Cuadernos', cors(), (req, res) => {
  console.time('loop');
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  var cod_expediente = "";
  var cod_anio = "";
  var cod_incidente = "";
  var cod_distprov = "";
  var cod_organo = "";
  var cod_especialidad = "";
  var cod_instancia = "";

  var cod_distritof = "";
  var cod_instanciaf = "";
  var cod_especialidadf = "";
  var cod_aniof = "";
  var cod_expedientef = "";


  if (xcej.length == 7) {
    cod_expediente = xcej[0];
    cod_anio = xcej[1];
    cod_incidente = xcej[2];
    cod_distprov = xcej[3];
    cod_organo = xcej[4];
    cod_especialidad = xcej[5];
    cod_instancia = xcej[6];
  } else {
    cod_distritof = xcej[0];
    cod_instanciaf = xcej[1];
    cod_especialidadf = xcej[2];
    cod_aniof = xcej[3];
    cod_expedientef = xcej[4];
  }



  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox"
      ],
      defaultViewport: null,
      'ignoreHTTPSErrors': true
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(95000);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');

    if (xcej.length == 7) {
      await page.waitForSelector('#captcha_image');
      await page.click('#myTab > li:nth-child(2)')
      await page.waitForSelector('#divConsultar')
      await page.waitForTimeout(500)

      await page.type('[id=cod_expediente]', cod_expediente, {
        delay: 10
      });
      await page.type('[id=cod_anio]', cod_anio, {
        delay: 10
      });
      await page.type('[id=cod_incidente]', cod_incidente, {
        delay: 10
      });
      await page.type('[id=cod_distprov]', cod_distprov, {
        delay: 10
      });
      await page.type('[id=cod_organo]', cod_organo, {
        delay: 10
      });
      await page.type('[id=cod_especialidad]', cod_especialidad, {
        delay: 10
      });
      await page.type('[id=cod_instancia]', cod_instancia, {
        delay: 10
      });
    } else {

      const option1 = (await page.$x(
        '//*[@id = "distritoJudicial"]/option[text() = "' + cod_distritof + '"]'
      ))[0];
      const value1 = await (await option1.getProperty('value')).jsonValue();
      await page.select('#distritoJudicial', value1);
      await page.waitForTimeout(500)

      const option2 = (await page.$x(
        '//*[@id = "organoJurisdiccional"]/option[text() = "' + cod_instanciaf + '"]'
      ))[0];
      const value2 = await (await option2.getProperty('value')).jsonValue();
      await page.select('#organoJurisdiccional', value2);
      await page.waitForTimeout(500)

      const option3 = (await page.$x(
        '//*[@id = "especialidad"]/option[text() = "' + cod_especialidadf + '"]'
      ))[0];
      const value3 = await (await option3.getProperty('value')).jsonValue();
      await page.select('#especialidad', value3);
      await page.waitForTimeout(500)

      await page.select('[id=anio]', cod_aniof);
      await page.waitForTimeout(100)
      await page.type('[id=numeroExpediente]', cod_expedientef, {
        delay: 10
      });
    }



    function Rompedor() {
      (async () => {
        /*const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 558,
            'y': 618,
            'width': 110,
            'height': 44
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.contrast(.999).posterize(500).opacity(.9999).greyscale().contrast(1).posterize(2).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);

              
            })();
          });
        });*/
        await page.click('#btnReload');
        await page.waitForTimeout(500)
        const text = await page.$eval("#deleteSound > input", (input) => {
          return input.getAttribute("value")
        });
        await page.type('[id=codigoCaptcha]', text, {
          delay: 5
        });
        await page.click('#consultarExpedientes');
        try {
          await page.waitForTimeout(2000)
          const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
          const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
          const Pasante = CaptchaPass[0]
          const Pasante2 = CaptchaError[0]

          console.log("-------------------------------------------")
          /*  if (Pasante2 == 'color: rgb(255, 0, 0);') {
              await browser.close()
              res.status(200).json({
                DataCuadernos: [],
                DataGneral: [],
                DataMovimienntos: [],
                result: true,
                mensaje: "No se encontraron registros con los datos ingresados."
              });
             // fs.unlinkSync(file)
            } else if (Pasante == '') {
              var Resultado = text.split(expresionRegular).toString().trim();
              console.log("Captcha no resuelto  ", Resultado)
            // fs.unlinkSync(file)
              Rompedor();
            } else {*/
          await page.waitForTimeout(500)
          await page.waitForSelector("#divDetalles")
          var Resultado = text.split(expresionRegular).toString().trim();
          console.log("Captcha resuelto ", Resultado)

          //Cantidad de cuadernos o procesos
          let nCuadernos = (await page.$$('#divDetalles > div')).length;
          let JsonDataCuadernos = []

          for (let i = 1; i <= nCuadernos; i++) {
            await page.waitForSelector('#divDetalles > div:nth-child(' + i + ')');
            let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
            let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
            let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
            let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());
            var data = {
              CodigoC: xCodigoC,
              CodigoExterno: xCodigoExterno,
              Detalle: xDetalle,
              Juzgado: xJuzgado,
            }
            JsonDataCuadernos.push(data)
          }
          console.timeEnd('loop');
          res.status(200).json({
            DataCuadernos: JsonDataCuadernos,
            result: true
          });
          await browser.close();
          //  fs.unlinkSync(file)
          console.log('Archivo generado  Eliminado')
          /* }*/
        } catch (error) {
          Rompedor();
        }
      })();
    }
    Rompedor();
  })();
});

app.post('/Vincular/CEJ', cors(), (req, res) => {

  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");
  console.time(CodigoExterno);
  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {
    // declare function
    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox"
      ],
      defaultViewport: null,
      'ignoreHTTPSErrors': true
    });
    const page = await browser.newPage();

    await page.setDefaultTimeout(50000);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(500)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 10
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 10
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 10
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 10
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 10
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 10
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 10
    });

    function Rompedor() {
      (async () => {
        /* const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
         const nameImage = await element.screenshot({
           type: 'png',
           'clip': {
             'x': 558,
             'y': 618,
             'width': 110,
             'height': 44
           },
           encoding: "base64"
         });
         const buffer = Buffer.from(nameImage, "base64");
         const hoy = new Date()
         const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
         var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
         Jimp.read(buffer, function (err, image) {
           if (err) throw err;
           image.contrast(.999).posterize(500).opacity(.9999).greyscale().contrast(1).posterize(2).invert().write(file, function (err, image) {
             if (err) throw err;
             (async () => {
               const {
                 data: {
                   text
                 }
               } = await Tesseract.recognize(file);
 
               
             })();
           });
         });*/

        await page.click('#btnReload');
        await page.waitForTimeout(500)
        const text = await page.$eval("#deleteSound > input", (input) => {
          return input.getAttribute("value")
        });
        await page.type('[id=codigoCaptcha]', text, {
          delay: 5
        });
        await page.click('#consultarExpedientes');

        try {
          await page.waitForTimeout(2000)
          const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
          const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
          const Pasante = CaptchaPass[0]
          const Pasante2 = CaptchaError[0]
          console.log("-------------------------------------------")
          /*  if (Pasante2 == 'color: rgb(255, 0, 0);') {
              await browser.close()
              res.status(200).json({
                DataCuadernos: [],
                DataGneral: [],
                DataMovimienntos: [],
                result: true,
                mensaje: "No se encontraron registros con los datos ingresados."
              });
              fs.unlinkSync(file)
            } else if (Pasante == '') {
              var Resultado = text.split(expresionRegular).toString().trim();
              console.log("Captcha no resuelto  ", Resultado)
              fs.unlinkSync(file)
              Rompedor();
            } else {*/
          await page.waitForTimeout(1000)
          await page.waitForSelector("#divDetalles")
          var Resultado = text.split(expresionRegular).toString().trim();
          console.log("Captcha resuelto ", Resultado)

          //Cantidad de cuadernos o procesos
          let nCuadernos = (await page.$$('#divDetalles > div')).length;
          let JsonDataCuadernos = []
          let JsonDataCuadernosInformacion = []
          let JsonDataMovimientos = []
          let JsonDataNotificacion = []



          for (let i = 1; i <= nCuadernos; i++) {
            await page.waitForSelector('#divDetalles > div:nth-child(' + i + ')');
            let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
            let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
            let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
            let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());

            if (xCodigoExterno == CodigoExterno) {

              var data = {
                CodigoC: xCodigoC,
                CodigoExterno: xCodigoExterno,
                Detalle: xDetalle,
                Juzgado: xJuzgado,
              }
              JsonDataCuadernos.push(data)

              await page.waitForTimeout(1000)
              await page.click('#divDetalles > div:nth-child(' + i + ') >  .celdCentro > form > button');

              await page.waitForSelector('#collapseThree')
              let CodigoExterno = await page.$eval('#gridRE > div:nth-child(1) > div.celdaGrid.celdaGridXe > b', e => e.innerText.trim());
              let orgJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
              let disJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(4)', e => e.innerText.trim());
              let juez = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
              let espLegal = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(4)', e => e.innerText.trim());
              let fInicio = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(2)', e => e.innerText.trim());
              let proceso = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(4)', e => e.innerText.trim());
              let obs = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(2)', e => e.innerText.trim());
              let esp = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(4)', e => e.innerText.trim());
              let materia = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(2)', e => e.innerText.trim());
              let estado = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(4)', e => e.innerText.trim());
              let eProcesal = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(2)', e => e.innerText.trim());
              let fConclusion = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(4)', e => e.innerText.trim());
              let ubicacion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(2)', e => e.innerText.trim());
              let mConclusion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(4)', e => e.innerText.trim());
              let sumilla = await page.$eval('#gridRE > div:nth-child(9) > div:nth-child(2)', e => e.innerText.trim());
              let xTipo = await page.$eval('#collapseTwo > div > div:nth-child(1) > div:nth-child(1)', e => e.innerText.trim());
              let nParticipantes = (await page.$$('#collapseTwo > div > div')).length;
              let strPart = "";
              for (let i = 2; i <= nParticipantes; i++) {
                let xPart = await page.$eval('#collapseTwo > div > div:nth-child(' + i + ')', e => e.innerText.trim());
                if (nParticipantes == i) {
                  strPart += xPart
                } else {
                  strPart += "(" + xTipo + ")" + xPart + "|"
                }
              }

              const sql1 = 'Call Ma_sp_Insertar_Maestro_SCRP(?,?)';
              connection.query(sql1, [
                'EG',
                estado
              ], (error, result) => {
                console.log(result,estado)
              });


              const sql2 = 'Call Ma_sp_Insertar_Maestro_SCRP(?,?)';
              connection.query(sql2, [
                'MA',
                esp
              ], (error, result) => {
                console.log(result,esp)
              });

              const sql3 = 'Call Ma_sp_Insertar_Maestro_SCRP(?,?)';
              connection.query(sql3, [
                'MC',
                materia
              ], (error, result) => {
                console.log(result,materia)
              });

              var sPart = strPart.split("\n").join(" ");

              var dataCabecera = {
                CodigoC: xCodigoC,
                CodigoExterno: CodigoExterno,
                General: [{
                  OrganoJurisdiccional: orgJuris,
                  DistritoJudicial: disJuris,
                  Juez: juez,
                  EspecialistaLegal: espLegal,
                  FechadeInicio: fInicio,
                  Proceso: proceso,
                  Observacion: obs,
                  Especialidad: esp,
                  Materia: materia,
                  Estado: estado,
                  EtapaProcesal: eProcesal,
                  FechaConclusión: fConclusion,
                  Ubicación: ubicacion,
                  MotivoConclusión: mConclusion,
                  Sumilla: sumilla,
                  Participantes: sPart,
                }]
              }
              JsonDataCuadernosInformacion.push(dataCabecera)
              await page.waitForTimeout(1500)
              let xMovimientos = (await page.$$('#collapseThree > div')).length - 1;

              for (let i = 0; i < xMovimientos; i++) {
                let CodigoMoviento = xMovimientos - i;
                let xFechaResolucion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                let xResolucion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                let xTNotificacion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                let xActo = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                let xFojas = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                let xProveido = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                let xSumilla = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                let xDescripcionUsuario = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                let dataMovimiento = {
                  Moviento: CodigoMoviento,
                  CodigoC: xCodigoC,
                  CodigoExterno: CodigoExterno,
                  FechaResolucion: xFechaResolucion,
                  Resolucion: xResolucion,
                  TNotificacion: xTNotificacion,
                  Acto: xActo,
                  Fojas: xFojas,
                  Proveido: xProveido,
                  Sumilla: xSumilla,
                  ComentarioUsuario: xDescripcionUsuario,
                  cFechaDescarga: "Descargado el " + cFecha,
                }
                JsonDataMovimientos.push(dataMovimiento)
                var xnNotificacion = (await page.$$('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div > div')).length;

                for (let j = 0; j < xnNotificacion; j++) {
                  let CodigoNotificacion = xnNotificacion - j;
                  let CodigoMoviento = xMovimientos - i;
                  let titulo = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div >h5', e => e.innerText.trim());
                  let xDestinatario = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                  let xFecha = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                  let xAnexos = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                  let xFormaEntrega = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

                  let dataNotificacion = {
                    CodigoNotificacion: CodigoNotificacion,
                    CodigoMoviento: CodigoMoviento,
                    CodigoExterno: xCodigoExterno,
                    titulo: titulo,
                    destinatario: xDestinatario,
                    fechaEnvio: xFecha,
                    anexos: xAnexos,
                    formaEntrega: xFormaEntrega,
                  }
                  JsonDataNotificacion.push(dataNotificacion)
                }
              }
              await page.waitForTimeout(800)
              await page.click('#divCuerpo > div:nth-child(1) > a:nth-child(1) > img');
            }
          }
          await browser.close()
          console.timeEnd(CodigoExterno);
          res.status(200).json({
            DataCuadernos: JsonDataCuadernos,
            DataGneral: JsonDataCuadernosInformacion,
            DataMovimienntos: JsonDataMovimientos,
            DataNotificacion: JsonDataNotificacion,
            result: true,
            mensaje: "Scraping realizado"
          });
          // fs.unlinkSync(file)
          /*}*/

        } catch (error) {
          await browser.close()
          console.timeEnd(CodigoExterno);
          res.status(200).json({
            result: false,
            mensaje: error
          });
          //fs.unlinkSync(file)
        }

      })();
    }
    Rompedor();
  })();
});

app.post('/Vincular/CEJ/MasivoUpdate', cors(), (req, res) => {
  const {
    CodigoExterno
  } = req.body;
  console.log(CodigoExterno)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {
    // declare function
    const browser = await puppeteer.launch(({
      args: [
        "--disable-gpu",
        "--disable-dev-shm-usage",
        "--disable-setuid-sandbox",
        "--no-sandbox",
      ]
    })); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(800)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 30
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 30
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 30
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 30
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 30
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 30
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 30
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.contrast(.999).posterize(500).opacity(.9999).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);


              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(1500)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                console.log("-------------------------------------------")
                if (Pasante == '') {
                  var Resultado = text.split(expresionRegular).toString().trim();
                  fs.unlinkSync(file)
                  Rompedor();

                } else {
                  await page.waitForSelector("#divDetalles")
                  await page.waitForTimeout(1500)
                  await page.click('#divDetalles > div:nth-child(1) >  .celdCentro > form > button');
                  await page.waitForSelector("#gridRE")
                  let Asunto = await page.$eval('#gridRE > div:nth-child(9) > div.celdaGridxT', e => e.innerText.trim());

                  res.status(200).json({
                    Asunto: Asunto,
                    result: true
                  });
                  console.log(Asunto)
                  fs.unlinkSync(file)
                  await browser.close()
                }

              } catch (error) {
                console.log(error)
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});

app.post('/Vincular/CorteSuprema', cors(), (req, res) => {

  const {
    CodSalaSuprema,
    CodRecurso,
    CodNRecurso,
    CodAnoRecurso,
    CodNExpediente,
    CodAnoxpediente,
    CodDistritoOrigen,
    CodApePaterno,
    CodApeMaterno,
    CodNombre,
  } = req.body;

  console.time("Test");
  (async () => {
    // declare function
    const browser = await puppeteer.launch(({
      headless: false,
      args: [
        "--disable-gpu",
        "--disable-dev-shm-usage",
        "--disable-setuid-sandbox",
        "--no-sandbox",
      ],
    })); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(120000);
    await page.setViewport({
      width: 1366,
      height: 768
    });

    await page.goto('https://apps.pj.gob.pe/cejSupremo/ConsultaExpediente.aspx');
    await page.select('[id=ddlSala]', CodSalaSuprema);
    await page.waitForTimeout(100)
    await page.select('[id=ddlMotIng]', CodRecurso);
    await page.waitForTimeout(100)
    await page.select('[id=ddlAnio]', CodAnoRecurso);
    await page.waitForTimeout(100)
    await page.select('[id=ddlAnio0]', CodAnoxpediente);
    await page.waitForTimeout(100)
    await page.select('[id=ddlDisOri]', CodDistritoOrigen);
    await page.waitForTimeout(100)

    await page.type('[id=txtNum]', CodNRecurso, {
      delay: 0
    });
    await page.type('[id=txtNum0]', CodNExpediente, {
      delay: 0
    });
    await page.type('[id=txtPat]', CodApePaterno, {
      delay: 0
    });
    await page.type('[id=txtMat]', CodApeMaterno, {
      delay: 0
    });
    await page.type('[id=txtNom]', CodNombre, {
      delay: 0
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#c_consultaexpediente_captcha1_CaptchaImage'); // declare a variable with an ElementHandle
        const nameImage = await element.screenshot({
          type: 'png',
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";

        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.color([{
            apply: 'desaturate',
            params: [150]
          }]).contrast(.999).posterize(.99).opacity(.9999).write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);
              var ResultadoSplit1 = text.replace(/ /gi, "");
              var ResultadoSplit2 = ResultadoSplit1.replace(/I/gi, "");
              console.log("-------------------------------------------")
              try {
                await page.type('[id=TextBox1]', ResultadoSplit2.split(" "), {
                  delay: 0
                });
                await page.click('#btnBuscar');
                await page.waitForTimeout(500)
                let lblEstado = await page.$eval('#lblEstado', e => e.innerText.trim());
                await page.waitForTimeout(1500)
                try {
                  if (lblEstado == 'Código CAPTCHA incorrecto') {

                    console.log("Captcha no resuelto  ", ResultadoSplit2)
                    fs.unlinkSync(file)
                    console.log('Archivo generado  Eliminado')
                    Rompedor();
                  } else {

                    console.log("Captcha resuelto ", ResultadoSplit2)
                    await page.waitForTimeout(1000)
                    await page.waitForSelector("#btnSeguimiento")
                    await page.click('#btnSeguimiento');

                    await page.waitForTimeout(1000)
                    await page.waitForSelector("#form1 > table ")

                    let JsonDataInformacion = []
                    let JsonDataPartesProcesales = []
                    let JsonDataVistaCausas = []
                    let JsonDataSeguimientoExpediente = []

                    let NroExpediente = await page.$eval('#CodExpediente', e => e.innerText.trim());
                    let Instancia = await page.$eval('#txtinstancia', e => e.innerText.trim());

                    let RecursoSala = await page.$eval('#txtrecurso', e => e.innerText.trim());
                    let FechaIngreso = await page.$eval('#fecinicio', e => e.innerText.trim());
                    let Organo = await page.$eval('#txtOrgPro', e => e.innerText.trim());
                    let Procedencia = await page.$eval('#txtOrgPro', e => e.innerText.trim());
                    let Relator = await page.$eval('#txtespecialista', e => e.innerText.trim());

                    let DistritoJudicial = await page.$eval('#txtDisPro', e => e.innerText.trim());
                    let ExpProcendenciaNro = await page.$eval('#txtNroPro', e => e.innerText.trim());
                    let Secretario = await page.$eval('#txtsecretario', e => e.innerText.trim());

                    let Materia = await page.$eval('#txtdelito', e => e.innerText.trim());
                    let Ubicacion = await page.$eval('#txtubicacion', e => e.innerText.trim());

                    let Estado = await page.$eval('#txtestado', e => e.innerText.trim());

                    var DataInformacion = {
                      NroExpediente: NroExpediente,
                      Instancia: Instancia,
                      RecursoSala: RecursoSala,
                      FechaIngreso: FechaIngreso,
                      Organo: Organo,
                      Procedencia: Procedencia,
                      Relator: Relator,
                      DistritoJudicial: DistritoJudicial,
                      ExpProcendenciaNro: ExpProcendenciaNro,
                      Secretario: Secretario,
                      Materia: Materia,
                      Ubicacion: Ubicacion,
                      Estado: Estado,
                    }
                    JsonDataInformacion.push(DataInformacion)

                    let PProcesales = (await page.$$('#gvPartes > tbody > tr')).length;
                    for (let i = 1; i <= PProcesales; i++) {
                      let TIPO = await page.$eval('#gvPartes > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                      let Nombre = await page.$eval('#gvPartes > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                      var DataPartesProcesales = {
                        IdPProcesales: i,
                        Tipo: TIPO,
                        Nombre: Nombre,
                      }
                      JsonDataPartesProcesales.push(DataPartesProcesales)
                    }

                    let VCausas = (await page.$$('#gvVistas > tbody > tr')).length;
                    for (let i = 1; i <= VCausas; i++) {
                      let FechaVista = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(2) > td:nth-child(1)', e => e.innerText.trim());
                      let FechaProgramacion = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(2) > td:nth-child(2)', e => e.innerText.trim());
                      let Parte = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(2) > td:nth-child(3)', e => e.innerText.trim());
                      let SentidoResultado = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(4) > td:nth-child(3)> span', e => e.innerText.trim());
                      let Observacion = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(5) > td:nth-child(2)> span', e => e.innerText.trim());
                      let TipoVista = await page.$eval('#gvVistas > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(6) > td:nth-child(3)> span', e => e.innerText.trim());

                      var DataVistaCausas = {
                        IdVCausas: i,
                        FechaVista: FechaVista,
                        FechaProgramacion: FechaProgramacion,
                        Parte: Parte,
                        SentidoResultado: SentidoResultado,
                        Observacion: Observacion,
                        TipoVista: TipoVista,
                      }
                      JsonDataVistaCausas.push(DataVistaCausas)
                    }

                    let SeguimientoSeguimiento = (await page.$$('#gvSeguimiento > tbody > tr')).length - 1;
                    for (let i = 1; i <= SeguimientoSeguimiento; i++) {
                      let Fecha = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(1) > td:nth-child(1)> span', e => e.innerText.trim());
                      let Acto = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(1) > td:nth-child(2)> span', e => e.innerText.trim());
                      let Resolucion = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(1) > td:nth-child(3)> span', e => e.innerText.trim());
                      let Fojas = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(1) > td:nth-child(4)> span', e => e.innerText.trim());

                      let Sumilla = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(2) > td:nth-child(2)> span', e => e.innerText.trim());
                      let DescargadorUsuario = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(4) > td:nth-child(2)> span', e => e.innerText.trim());
                      let Presentante = await page.$eval('#gvSeguimiento > tbody > tr:nth-child(' + i + ') > td > table > tbody > tr:nth-child(5) > td:nth-child(2)> span', e => e.innerText.trim());


                      var DataSeguimientoExpediente = {
                        IdSeguimientoSeguimiento: i,
                        Fecha: Fecha,
                        Acto: Acto,
                        Resolucion: Resolucion,
                        Fojas: Fojas,
                        Sumilla: Sumilla,
                        DescargadorUsuario: DescargadorUsuario,
                        Presentante: Presentante,

                      }
                      JsonDataSeguimientoExpediente.push(DataSeguimientoExpediente)
                    }

                    res.status(200).json({
                      DataInformacion: JsonDataInformacion,
                      DataPartesProcesales: JsonDataPartesProcesales,
                      DataVistaCausas: JsonDataVistaCausas,
                      DataSeguimientoExpediente: JsonDataSeguimientoExpediente,
                      result: true
                    });
                    fs.unlinkSync(file)
                    console.timeEnd("Test");
                    console.log('Archivo generado  Eliminado')
                    await browser.close();
                  }

                } catch (error) {
                  res.status(500).json({
                    result: false,
                    mensaje: "Ups Error"
                  });
                  console.log(error)
                  await browser.close();
                }
              } catch (error) {

              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});


app.post('/Scraping/Insertar', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.nCas_Id,
    JSON.stringify(e.cCav_Cuadernos).replace(/\//g, '').toString(),
    JSON.stringify(e.cCav_General).replace(/\//g, '').toString(),
    JSON.stringify(e.cCav_Movimientos).replace(/\//g, '').toString(),
    JSON.stringify(e.cCav_Notificacion).replace(/\//g, '').toString(),
    e.cCav_Archivo,
    e.cCav_Notas,

  ];

  const sql = 'Call usp_Casos_Movs_Insert(?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      console.log("Insertado")
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Caso/Update/Masivo', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.cCas_Titulo,
    e.cCas_Cod_Externo,
  ];
  console.log(data)

  const sql = 'Call sp_UpdateMasivo(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Costos/Proc/Liq', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.nCas_Id

  ];

  console.log(data);

  const sql = 'Call _report_sp_Costos_Liq_Procesar(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Costos/Procesar', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.id,
    e.Codigo
  ];

  console.log("procesar id:", data);

  const sql = 'Call _report_Procesar_Gastos(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Combo/Codigo/Liq', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e.nCli_Id,
    e.nCas_Id
  ];
  const sql = 'Call cd_sp_LiscarCodigoLiq(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Costos/ProcesarH', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.id,
    e.Codigo
  ];
  const sql = 'Call _report_Procesar_Honorarios(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/CostosHonorarios', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.nCas_Id

  ];

  console.log(data);



  const sql = 'Call _report_sp_CostosHonorarios(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Reporte/Costos/H', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.CodigoLiq,
  ];

  console.log(data);



  const sql = 'Call _report_sp_CostosLiqH(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/GenerarCodigo/Liquidacion', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.MontoH,
    e.MontoG,
  ];

  const sql = 'select sp_Generar_Codigo_Liq(?,?,?) as Codigo';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});



app.post('/Reporte/Ecuenta/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon

  ];

  console.log("Total: ", data);



  const sql = 'Call _report_sp_EstadoCuenta_Total(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Report/Cases/Excel', cors(), (req, res) => {

  let e = req.body;
  let data = [];
  const sql = 'Call _report_sp_Export_Data_Excel()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Costos/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.ini,
    e.fin,
    e.mon,
    e.state,
    e.cEgre_Liq_Numero,
    e.nCas_Id
  ];
  const sql = 'Call _report_sp_CostosCostosTotal(?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Reporte/Honorarios/Total', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.ini,
    e.fin,
    e.mon,
    e.state,
    e.cLab_Liq_Numero,
    e.nCas_Id
  ];

  console.log("total costos: ", data);



  const sql = 'Call _report_sp_CostosHonorariosTotal(?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Tareas/DH', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.nCli_Id,
    e.nUsu_ID,
  ];
  const sql = `Call ta_sp_Listar_Tareas_DH(?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Eventos/DH', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.nCli_Id,
    e.nUsu_ID,
  ];
  const sql = `Call ev_sp_Listar_Eventos_DH(?,?,?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Eventos/Participante', cors(), (req, res) => {
  let e = req.body;
  const sql = `Call ev_sp_Listar_Participantes()`;
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Tareas/Verificar/Estado', cors(), (req, res) => {

  let e = req.body;
  const sql = `Call ta_Verificar_Estado()`;
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});

app.post('/Tareas/Actualizar/Estado', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nTar_Id
  ];
  const sql = `Call ta_Actualizar_Estado(?)`;
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('empty');
    }
  });
});



app.post('/Suscriptores/Listar', cors(), (req, res) => {

  let e = req.body;


  const sql = 'Call su_sp_Listar_Suscriptores()';
  connection.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Roles/Listar', cors(), (req, res) => {
  let e = req.body;
  const sql = 'Call rl_sp_Listar_Roles()';
  connection.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Acceso/xDefecto', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.nUsu_Id,
    e.cUsu_Modulo,
    e.cUsu_Opcion,
    e.bUsu_Editar,
    e.bUsu_Eliminar,
    e.bUsu_Consultar
  ]

  console.log(data)

  const sql = 'Call ua_sp_Insertar_UsuarioAcceso(?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Buscar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.FechaInicial,
    e.FechaFin,
  ]

  const sql = 'Call su_sp_Buscar_Suscriptores(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Registrar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.cSus_Categoria,
    e.cSus_Nombres,
    e.cSus_Correo,
    e.cSus_Pass,
    e.cSus_Telefono,
    e.cSus_Empresa,
    e.cSus_CadenaConexion,
    e.cSus_Cuenta,
    e.bSus_Estado,
  ]

  console.log(data)

  const sql = 'Call su_sp_Insertar_Suscriptores(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Actualizar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.nSus_Id,
    e.cSus_Categoria,
    e.cSus_Nombres,
    e.dSus_FechaVencimiento,
    e.dSus_FechaCreacion,
    e.dSus_UltimoPago,
    e.cSus_Correo,
    e.cSus_Pass,
    e.cSus_Telefono,
    e.cSus_Empresa,
    e.cSus_CadenaConexion,
    e.cSus_Cuenta,
    e.bSus_Estado,
  ]

  console.log(data)

  const sql = 'Call su_sp_Actualizar_Suscriptores(?,?,?,?,?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Suscriptores/Eliminar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.nSus_Id,
  ]

  console.log(data)

  const sql = 'Call su_sp_Eliminar_Suscriptores(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/mail/weekly', cors(), (req, res) => {
  let e = req.body;

  let data = [
    e.email
  ]

  console.log(data)

  const transporter = nodemailer.createTransport({
    //host: "mail.dotnetsa.com",

    host: "mail.concursalperu.com",
    port: 465,
    secure: true,
    auth: {
      //C1@v€0bv1@
      //user: 'sistemas@dotnetsa.com',
      user: 'notificador@concursalperu.com',
      //C1@v€0bv1@
      pass: 'C1@v€0bv1@',
      //pass: 'C1@v€0bv1@',
    },
    tls: {
      rejectUnauthorized: 0
    }
  })

  var mailOptions = {
    from: '"GPS Legal" <notificador@concursalperu.com>',
    to: e.email,
    subject: 'Resumen Semanal',
    text: 'GPS Legal',
    html: `<center>
    <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;'>
        <tbody><tr>
            <td align='center' valign='top' id='bodyCell' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;border-top: 0;'>
                <!-- BEGIN TEMPLATE // -->
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <tbody><tr>
                        <td align='center' valign='top' id='templatePreheader' style='background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                            <!--[if (gte mso 9)|(IE)]>
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            <![endif]-->
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='preheaderContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <!--[if mso]>
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>
<![endif]-->
  
<!--[if mso]>
<td valign='top' width='600' style='width:600px;'>
<![endif]-->
        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%' class='mcnTextContentContainer'>
            <tbody><tr>
                
  
            </tr>
        </tbody></table>
<!--[if mso]>
</td>
<![endif]-->
        
<!--[if mso]>
</tr>
</table>
<![endif]-->
    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateHeader' style='background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;'>
                            <!--[if (gte mso 9)|(IE)]>
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            <![endif]-->
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='headerContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnImageBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnImageBlockOuter'>
    <tr>
        <td valign='top' style='padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnImageBlockInner'>
            <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' class='mcnImageContentContainer' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <tbody><tr>
                    <td class='mcnImageContent' valign='top' style='padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                        
                            
                                <img align='center' alt='' src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' width='564' style='max-width: 4096px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' class='mcnImage'>
                            
                        
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateBody' style='background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                            <!--[if (gte mso 9)|(IE)]>
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            <![endif]-->
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='bodyContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <!--[if mso]>
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>
<![endif]-->
  
<!--[if mso]>
<td valign='top' width='600' style='width:600px;'>
<![endif]-->
        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%' class='mcnTextContentContainer'>
            <tbody><tr>
                
                <td valign='top' class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;'>
                
                    <h1 style='display: block;margin: 0;padding: 0;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;'>Tenemos para usted las novedades del dia.</h1>

<p style='margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;'>En nuestra revisión de sus procesos hemos encontrado las siguientes novedades:<br>
<br>
<strong><span style='color:#aa1b0e'>PROCESOS:</span></strong><br>
Cliente: Juanito Perez<br>
Caso: Alimentos<br>
Movimientos:<br>
Resolución TRECE<br>
Fecha: 22/07/2021<br>
<br>
Notificación<br>
Notificación<br>
Notificación<br>
<br>
Cliente: Javier Miranda<br>
Caso: Divorcio<br>
Movimientos:<br>
Resolución UNO<br>
Fecha: 12/06/2021<br>
<br>
Notificación<br>
Notificación<br>
<br>
<br>
<span style='color:#aa1b0e'><strong>EVENTOS DE HOY:</strong></span><br>
10:30 Reunion Dotnet<br>
14:15 Charla de nuevos clientes<br>
<br>
<strong><span style='color:#aa1b0e'>EVENTOS DE MAÑANA:</span></strong><br>
12:45 Zoom con Representante de Amazon Web Services<br>
17:00 Entrevistar webmaster<br>
<br>
<span style='color:#aa1b0e'><strong>TAREAS POR VENCERSE:</strong></span><br>
Elaborar el escrito de Demanda<br>
Pagar Tasas&nbsp;</p>

                </td>
            </tr>
        </tbody></table>
<!--[if mso]>
</td>
<![endif]-->
        
<!--[if mso]>
</tr>
</table>
<![endif]-->
    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateFooter' style='background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                            <!--[if (gte mso 9)|(IE)]>
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            <![endif]-->
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='footerContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnFollowBlockOuter'>
<tr>
    <td align='center' valign='top' style='padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowBlockInner'>
        <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentContainer' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody><tr>
<td align='center' style='padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
    <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContent'>
        <tbody><tr>
            <td align='center' valign='top' style='padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <table align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <tbody><tr>
                        <td align='center' valign='top' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                            <!--[if mso]>
                            <table align='center' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                            <![endif]-->
                            
                                <!--[if mso]>
                                <td align='center' valign='top'>
                                <![endif]-->
                                
                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://www.twitter.com/' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png' alt='Twitter' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                
                                <!--[if mso]>
                                </td>
                                <![endif]-->
                            
                                <!--[if mso]>
                                <td align='center' valign='top'>
                                <![endif]-->
                                
                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://www.facebook.com' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png' alt='Facebook' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                
                                <!--[if mso]>
                                </td>
                                <![endif]-->
                            
                                <!--[if mso]>
                                <td align='center' valign='top'>
                                <![endif]-->
                                
                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://mailchimp.com' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png' alt='Website' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                
                                <!--[if mso]>
                                </td>
                                <![endif]-->
                            
                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
</td>
</tr>
</tbody></table>

    </td>
</tr>
</tbody>
</table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;'>
<tbody class='mcnDividerBlockOuter'>
<tr>
    <td class='mcnDividerBlockInner' style='min-width: 100%;padding: 10px 18px 25px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
            <tbody><tr>
                <td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <span></span>
                </td>
            </tr>
        </tbody></table>
<!--            
        <td class='mcnDividerBlockInner' style='padding: 18px;'>
        <hr class='mcnDividerContent' style='border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;' />
-->
    </td>
</tr>
</tbody>
</table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <!--[if mso]>
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>
<![endif]-->
  
<!--[if mso]>
<td valign='top' width='600' style='width:600px;'>
<![endif]-->
        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%' class='mcnTextContentContainer'>
            <tbody><tr>
                
                <td valign='top' class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;'>
                
                    <em>Copyright © </em>GPS Legal<em>, Todos los derechos reservados.</em><br>
<br>
<strong>Nuestra dirección de correo es:</strong><br>
Atencionalcliente@gpslegal.pe<br>
<br>
Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.
                </td>
            </tr>
        </tbody></table>
<!--[if mso]>
</td>
<![endif]-->
        
<!--[if mso]>
</tr>
</table>
<![endif]-->
    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </tbody></table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </tbody></table>
</center>`

  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    res.json({
      message: 'sended successfully'
    })
  });



  //const sql = 'Call usp_Notificaciones_Insert(?,?,?,?,?,?,?,?,?)';
  /*connection.query(sql,data, (error, result) => {
    if (error) throw error;
if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
  });*/
});

app.post('/mail/novelty', cors(), (req, res) => {


  let e = req.body;

  let data = [
    e.email
  ]

  console.log(data);



  const transporter = nodemailer.createTransport({
    //host: "mail.dotnetsa.com",

    host: "mail.concursalperu.com",
    port: 465,
    secure: true,
    auth: {
      //C1@v€0bv1@
      //user: 'sistemas@dotnetsa.com',
      user: 'notificador@concursalperu.com',
      //C1@v€0bv1@
      pass: 'C1@v€0bv1@',
      //pass: 'C1@v€0bv1@',
    },
    tls: {
      rejectUnauthorized: 0
    }
  })

  var mailOptions = {
    from: '"GPS Legal" <notificador@concursalperu.com>',
    to: e.email,
    subject: 'Novedades',
    text: 'GPS Legal',
    html: `<center><table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;'>
        <tbody><tr>
            <td align='center' valign='top' id='bodyCell' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;border-top: 0;'>
                
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <tbody><tr>
                        <td align='center' valign='top' id='templatePreheader' style='background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                            
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='preheaderContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
     
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>

<td valign='top' width='600' style='width:600px;'>

</td>

</tr>
</table>

    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            
                            </td>
                            </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateHeader' style='background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;'>
                            
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                            
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='headerContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnImageBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnImageBlockOuter'>
    <tr>
        <td valign='top' style='padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnImageBlockInner'>
            <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' class='mcnImageContentContainer' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <tbody><tr>
                    <td class='mcnImageContent' valign='top' style='padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                        
                            
                                <img align='center' alt='' src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' width='564' style='max-width: 4096px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' class='mcnImage'>
                            
                        
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                        
                            </td>
                            </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateBody' style='background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                          
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                           
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='bodyContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
     
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>


<td valign='top' width='600' style='width:600px;'>

        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%' class='mcnTextContentContainer'>
            <tbody><tr>
                
                <td valign='top' class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;'>
                
                    <h1 style='display: block;margin: 0;padding: 0;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;'>Tenemos para usted las novedades del dia.</h1>

<p style='margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;line-height: 150%;text-align: left;'>En nuestra revisión de sus procesos hemos encontrado las siguientes novedades:<br>
<br>
<strong><span style='color:#aa1b0e'>PROCESOS:</span></strong><br>
Cliente: Gianluca Lapadula<br>
Caso: Alimentos<br>
Movimientos:<br>
Resolución TRECE<br>
Fecha: 23/08/2021<br>
<br>
Notificación<br>
Notificación<br>
Notificación<br>
<br>
Cliente: Javier Moroa<br>
Caso: Divorcio<br>
Movimientos:<br>
Resolución UNO<br>
Fecha: 12/10/2021<br>
<br>
Notificación<br>
Notificación<br>
<br>
<br>
<span style='color:#aa1b0e'><strong>EVENTOS DE HOY:</strong></span><br>
10:30 Reunion Dotnet SA<br>
14:15 Charla de nuevos clientes<br>
<br>
<strong><span style='color:#aa1b0e'>EVENTOS DE MAÑANA:</span></strong><br>
12:45 Zoom con Representante de Amazon Web Services<br>
17:00 Entrevistar webmaster<br>
<br>
<span style='color:#aa1b0e'><strong>TAREAS POR VENCERSE:</strong></span><br>
Elaborar el escrito de Demanda<br>
Pagar Tasas&nbsp;</p>

                </td>
            </tr>
        </tbody></table>

</td>

</tr>
</table>

    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                           
                            </td>
                            </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align='center' valign='top' id='templateFooter' style='background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;'>
                          
                            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
                            <tr>
                            <td align='center' valign='top' width='600' style='width:600px;'>
                    
                            <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;'>
                                <tbody><tr>
                                    <td valign='top' class='footerContainer' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnFollowBlockOuter'>
<tr>
    <td align='center' valign='top' style='padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowBlockInner'>
        <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentContainer' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody><tr>
<td align='center' style='padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
    <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContent'>
        <tbody><tr>
            <td align='center' valign='top' style='padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <table align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <tbody><tr>
                        <td align='center' valign='top' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                            <table align='center' border='0' cellspacing='0' cellpadding='0'>
                            <tr>

                                <td align='center' valign='top'>

                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://www.twitter.com/' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png' alt='Twitter' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>

                                <td align='center' valign='top'>
                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://www.facebook.com' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png' alt='Facebook' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                
                                </td>
                                
                                <td align='center' valign='top'>
                                
                                
                                    <table align='left' border='0' cellpadding='0' cellspacing='0' style='display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                        <tbody><tr>
                                            <td valign='top' style='padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' class='mcnFollowContentItemContainer'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnFollowContentItem' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                    <tbody><tr>
                                                        <td align='left' valign='middle' style='padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                            <table align='left' border='0' cellpadding='0' cellspacing='0' width='' style='border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                <tbody><tr>
                                                                    
                                                                        <td align='center' valign='middle' width='24' class='mcnFollowIconContent' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                                                                            <a href='http://mailchimp.com' target='_blank' style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png' alt='Website' style='display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;' height='24' width='24' class=''></a>
                                                                        </td>
                                                                    
                                                                    
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                
                                
                                </td>
                                
                            
                            
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
</td>
</tr>
</tbody></table>

    </td>
</tr>
</tbody>
</table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;'>
<tbody class='mcnDividerBlockOuter'>
<tr>
    <td class='mcnDividerBlockInner' style='min-width: 100%;padding: 10px 18px 25px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
            <tbody><tr>
                <td style='mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <span></span>
                </td>
            </tr>
        </tbody></table>

    </td>
</tr>
</tbody>
</table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
<tbody class='mcnTextBlockOuter'>
<tr>
    <td valign='top' class='mcnTextBlockInner' style='padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
        <!--[if mso]>
<table align='left' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;'>
<tr>
<![endif]-->
  
<!--[if mso]>
<td valign='top' width='600' style='width:600px;'>
<![endif]-->
        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;' width='100%' class='mcnTextContentContainer'>
            <tbody><tr>
                
                <td valign='top' class='mcnTextContent' style='padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;'>
                
                    <em>Copyright © </em>GPS Legal<em>, Todos los derechos reservados.</em><br>
<br>
<strong>Nuestra dirección de correo es:</strong><br>
Atencionalcliente@gpslegal.pe<br>
<br>
Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.
                </td>
            </tr>
        </tbody></table>
<!--[if mso]>
</td>
<![endif]-->
        
<!--[if mso]>
</tr>
</table>
<![endif]-->
    </td>
</tr>
</tbody>
</table></td>
                                </tr>
                            </tbody></table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </tbody></table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </tbody></table>
</center>`,
    /*attachments: [
      {
        filename: 'mailtrap.png',
        path: __dirname + '/mailtrap.png',
        cid: 'uniq-mailtrap.png'
      }
    ]*/
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    res.json({
      message: 'sended successfully'
    })
  });



  //const sql = 'Call usp_Notificaciones_Insert(?,?,?,?,?,?,?,?,?)';
  /*connection.query(sql,data, (error, result) => {
    if (error) throw error;
if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }
  });*/
});

app.post('/Correo/List', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.email,
    e.info,


  ]

  console.log(data)

  const sql = 'Call cor_sp_Listar_Correo()';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Dashboard/Casos/DataCEJ', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.id


  ]

  console.log(data)

  const sql = 'Call usp_Casos_Movs_Select(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.get('/file-example', function (req, res) {

  const file = __dirname + '/baronrojo.jpg';
  res.sendFile(file); // Set disposition and send it.


  imageToBase64(file) // Path to the image
    .then(
      (response) => {
        var ResultadoIMG = 'data:image/png;base64,' + response;
        res.status(200).json({
          img: resultadoIMG
        });
      }
    )
    .catch(
      (error) => {
        console.log(error);
      }
    )

});

app.post('/Vincular/Indecopi', cors(), (req, res) => {
  let e = req.body;
  console.log(e);
  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox"
      ],
      defaultViewport: null,
      'ignoreHTTPSErrors': true
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(30000);
    await page.setViewport({
      width: 1366,
      height: 768
    });


    switch (e.comision) {
      case 1:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCPC.jsp?pIdAreaMenu=8');

            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });

            const option1 = (await page.$x(
              '//*[@id = "opcArea"]/option[text() = "' + xCod[2] + '"]'
            ))[0];
            const value1 = await (await option1.getProperty('value')).jsonValue();
            await page.select('#opcArea', value1);

            await page.waitForTimeout(50)
            await page.select('[name=lstTipoExpediente]', xCod[3]);
            await page.waitForTimeout(50)
            await page.select('[name=lstLugarTramite]', xCod[4]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(5000)
              await page.waitForSelector('.resultados-item-res');
              let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
              let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

              let rpt = [];
              let datos = [];

              for (let i = 2; i <= ntrInfoCon; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                let str = value.split("\t")
                datos.push({
                  datos: str[1]
                });
              }

              for (let i = 2; i <= ntr; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                rpt.push(value);
              }

              let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.resultados-der', e => e.innerText.trim());

              console.log(rpt);
              //buscar el indice donde aparece el texto Denunciantes
              initDn = rpt.indexOf("Denunciantes");
              //buscar el indice donde aparece el texto Denunciados
              initDs = rpt.indexOf("Denunciados");

              var initAll = rpt.length;

              let denunciados = [];
              let denunciantes = [];

              let NExpediente = [{
                NExpediente: NExpedienteS,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                SAC: SAC,
                LugarT: LugarT,
                LugarP: LugarP,
              }];


              for (let j = initDn + 1; j < initDs; j++) {
                //denunciados["denunciado-" + j] = (rpt[j]);
                denunciados.push({
                  denunciado: rpt[j]
                })
              }
              console.log("denunciados : ", denunciados);
              for (let k = initDs + 1; k < initAll; k++) {
                //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
                denunciantes.push({
                  denunciante: rpt[k]
                })
              }
              console.log("denunciantes: ", denunciantes);

              let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
              //arr de resultados de notificacion
              let Notications = [];

              for (let i = 3; i <= nNotificacion; i++) {
                try {
                  let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                  let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                  Notications.push({
                    fecha: fechaData,
                    detalle: actividad
                  });
                } catch (error) {
                  console.log(error);
                }
              }


              res.status(200).json({
                NExpediente: NExpediente,
                datos: datos,
                denunciantes: denunciantes,
                denunciados: denunciados,
                Notications: Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }

            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=8&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;
              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=8&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });

          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }


        }
        break;
      case 2:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaSumario.jsp?pIdAreaMenu=119');
            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });

            const option1 = (await page.$x(
              '//*[@id = "opcArea"]/option[text() = "' + xCod[2] + '"]'
            ))[0];
            const value1 = await (await option1.getProperty('value')).jsonValue();
            await page.select('#opcArea', value1);
            await page.waitForTimeout(300)
            await page.select('[name=lstTipoExpediente]', xCod[3]);
            await page.waitForTimeout(300)
            await page.select('[name=lstLugarTramite]', xCod[4]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(1000)
              await page.waitForSelector('.resultados-item-res');

              let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
              let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

              let rpt = [];
              let datos = [];

              for (let i = 2; i <= ntrInfoCon; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                let str = value.split("\t")
                datos.push({
                  datos: str[1]
                });
              }

              for (let i = 2; i <= ntr; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                rpt.push(value);
              }

              let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr.resultados-item > td:nth-child(2) > b', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

              let NExpediente = [{
                NExpediente: NExpedienteS,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                SAC: SAC,
                LugarT: LugarT,
                LugarP: LugarP,
              }];

              console.log(rpt);
              //buscar el indice donde aparece el texto Denunciantes
              initDn = rpt.indexOf("Denunciantes");
              //buscar el indice donde aparece el texto Denunciados
              initDs = rpt.indexOf("Denunciados");

              var initAll = rpt.length;

              let denunciados = [];
              let denunciantes = [];




              for (let j = initDn + 1; j < initDs; j++) {
                //denunciados["denunciado-" + j] = (rpt[j]);
                denunciados.push({
                  denunciado: rpt[j]
                })
              }
              console.log("denunciados : ", denunciados);
              for (let k = initDs + 1; k < initAll; k++) {
                //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
                denunciantes.push({
                  denunciante: rpt[k]
                })
              }
              console.log("denunciantes: ", denunciantes);

              let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
              //arr de resultados de notificacion
              let Notications = [];

              for (let i = 3; i <= nNotificacion; i++) {
                let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                try {
                  Notications.push({
                    fecha: fechaData,
                    detalle: actividad
                  });
                } catch (error) {
                  console.log(error);
                }
              }
              res.status(200).json({
                NExpediente,
                datos: datos,
                denunciantes: denunciantes,
                denunciados: denunciados,
                Notications: Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoPS.jsp?pIdAreaMenu=119&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');

                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoPS.jsp?pIdAreaMenu=119&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');

                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });
          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }
        }
        break;
      case 3:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCCD.jsp?pIdAreaMenu=5');
            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });
            await page.select('[name=lstLugarTramite]', xCod[4]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(800)
              let nExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let Acumulado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let Asistente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

              let FormaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let FechaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let nResolucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());

              let NExpediente = [{
                NExpediente: nExpediente,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                LugarT: LugarT,
                LugarP: LugarP,
                Acumulado: Acumulado,
                Asistente: Asistente,
                FormaConclucion: FormaConclucion,
                FechaConclucion: FechaConclucion,
                nResolucion: nResolucion,
              }];

              let nPersonasNJ = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr')).length;

              var Datos = []
              for (let i = 2; i <= nPersonasNJ; i++) {
                let Nombre = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let DocumentoI = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                let DomicilioP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3)', e => e.innerText.trim());
                let PDProvincia = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(4)', e => e.innerText.trim());
                Datos.push({
                  Nombre: Nombre,
                  DocumentoI: DocumentoI,
                  DomicilioP: DomicilioP,
                  PDProvincia: PDProvincia,
                });
              }

              var ArrayDenunciantes = Datos.findIndex(obj => obj.Nombre == "Denunciantes");
              var ArrayDenunciados = Datos.findIndex(obj => obj.Nombre == "Denunciados");
              var denunciados = []
              var denunciantes = []
              for (let j = ArrayDenunciantes + 1; j < ArrayDenunciados; j++) {
                denunciados.push(Datos[j])
              }
              for (let j = ArrayDenunciados + 1; j < Datos.length; j++) {
                denunciantes.push(Datos[j])
              }
              let Notications = [];
              let nNotificaciones = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr')).length;
              for (let i = 2; i < nNotificaciones; i++) {
                let fecha = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let detalle = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                Notications.push({
                  fecha: fecha,
                  detalle: detalle
                });
              }


              res.status(200).json({
                NExpediente: NExpediente,
                denunciantes,
                denunciados,
                Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=5&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');

                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=5&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                await page.click('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td > input[type=radio]:nth-child(1)');

                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });
          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }



        }
        break;
      case 4:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCEB.jsp?pIdAreaMenu=4');
            await page.waitForTimeout(800)


            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=10');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 5:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCPC.jsp?pIdAreaMenu=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=14');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=10');
            break;
          case 4:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 6:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=2');
            break;
        }
        break;
      case 7:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=7');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=2');
            break;
        }
        break;
      case 8:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=3');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=2');
            break;
        }
        break;
      case 9:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaSPC.jsp?pId=8');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=2');
            break;
        }
        break;
    }


   // await browser.close();

  })();
});

app.post('/Vincular/Indecopi/Scraping', cors(), (req, res) => {
  let e = req.body;
  console.log(e);
  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox"
      ],
      defaultViewport: null,
      'ignoreHTTPSErrors': true
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(30000);
    await page.setViewport({
      width: 1366,
      height: 768
    });


    switch (e.comision) {
      case 1:
        await page.goto(e.url);
        try {
          await page.waitForTimeout(5000)
          await page.waitForSelector('.resultados-item-res');
          let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
          let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

          let rpt = [];
          let datos = [];

          for (let i = 2; i <= ntrInfoCon; i++) {
            let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
            let str = value.split("\t")
            datos.push({
              datos: str[1]
            });
          }

          for (let i = 2; i <= ntr; i++) {
            let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
            rpt.push(value);
          }

          let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
          let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
          let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
          let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
          let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
          let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());
          let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.resultados-der', e => e.innerText.trim());

          console.log(rpt);
          //buscar el indice donde aparece el texto Denunciantes
          initDn = rpt.indexOf("Denunciantes");
          //buscar el indice donde aparece el texto Denunciados
          initDs = rpt.indexOf("Denunciados");

          var initAll = rpt.length;

          let denunciados = [];
          let denunciantes = [];

          let NExpediente = [{
            NExpediente: NExpedienteS,
            TipoExpediente: tExpediente,
            FechaPresentacion: Fpresentacion,
            Tpresentación: Tpresentación,
            SAC: SAC,
            LugarT: LugarT,
            LugarP: LugarP,
          }];


          for (let j = initDn + 1; j < initDs; j++) {
            //denunciados["denunciado-" + j] = (rpt[j]);
            denunciados.push({
              denunciado: rpt[j]
            })
          }
          console.log("denunciados : ", denunciados);
          for (let k = initDs + 1; k < initAll; k++) {
            //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
            denunciantes.push({
              denunciante: rpt[k]
            })
          }
          console.log("denunciantes: ", denunciantes);

          let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
          //arr de resultados de notificacion
          let Notications = [];

          for (let i = 3; i <= nNotificacion; i++) {
            try {
              let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
              let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
              Notications.push({
                fecha: fechaData,
                detalle: actividad
              });
            } catch (error) {
              console.log(error);
            }
          }


          res.status(200).json({
            NExpediente: NExpediente,
            datos: datos,
            denunciantes: denunciantes,
            denunciados: denunciados,
            Notications: Notications,
            result: true
          });

        } catch (error) {

          res.status(200).json({
            mensaje: "No hay resultados coincidentes con los criterios de busqueda",
            result: false
          });
        }
        break;
      case 2:
        await page.goto(e.url);
        try {
          await page.waitForTimeout(1000)
          await page.waitForSelector('.resultados-item-res');

          let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
          let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

          let rpt = [];
          let datos = [];

          for (let i = 2; i <= ntrInfoCon; i++) {
            let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
            let str = value.split("\t")
            datos.push({
              datos: str[1]
            });
          }

          for (let i = 2; i <= ntr; i++) {
            let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
            rpt.push(value);
          }

          let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr.resultados-item > td:nth-child(2) > b', e => e.innerText.trim());
          let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
          let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
          let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
          let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
          let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
          let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

          let NExpediente = [{
            NExpediente: NExpedienteS,
            TipoExpediente: tExpediente,
            FechaPresentacion: Fpresentacion,
            Tpresentación: Tpresentación,
            SAC: SAC,
            LugarT: LugarT,
            LugarP: LugarP,
          }];

          console.log(rpt);
          //buscar el indice donde aparece el texto Denunciantes
          initDn = rpt.indexOf("Denunciantes");
          //buscar el indice donde aparece el texto Denunciados
          initDs = rpt.indexOf("Denunciados");

          var initAll = rpt.length;

          let denunciados = [];
          let denunciantes = [];




          for (let j = initDn + 1; j < initDs; j++) {
            //denunciados["denunciado-" + j] = (rpt[j]);
            denunciados.push({
              denunciado: rpt[j]
            })
          }
          console.log("denunciados : ", denunciados);
          for (let k = initDs + 1; k < initAll; k++) {
            //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
            denunciantes.push({
              denunciante: rpt[k]
            })
          }
          console.log("denunciantes: ", denunciantes);

          let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
          //arr de resultados de notificacion
          let Notications = [];

          for (let i = 3; i <= nNotificacion; i++) {
            let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
            let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
            try {
              Notications.push({
                fecha: fechaData,
                detalle: actividad
              });
            } catch (error) {
              console.log(error);
            }
          }
          res.status(200).json({
            NExpediente,
            datos: datos,
            denunciantes: denunciantes,
            denunciados: denunciados,
            Notications: Notications,
            result: true
          });

        } catch (error) {
          res.status(200).json({
            mensaje: "No hay resultados coincidentes con los criterios de busqueda",
            result: false
          });
        }
        break;
      case 3:
        await page.goto(e.url);
        let xCod = e.cod.split("-")
        console.log(xCod)

        await page.type('[name=txtNroExpediente]', xCod[0], {
          delay: 0
        });
        await page.type('[name=txtAnioExpediente]', xCod[1], {
          delay: 0
        });
        await page.select('[name=lstLugarTramite]', xCod[4]);
        await page.click('[name=btnBuscar]');

        try {
          await page.waitForTimeout(800)
          let nExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
          let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.resultados-der', e => e.innerText.trim());
          let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
          let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
          let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
          let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
          let Acumulado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
          let Asistente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

          let FormaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
          let FechaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
          let nResolucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());

          let NExpediente = [{
            NExpediente: nExpediente,
            TipoExpediente: tExpediente,
            FechaPresentacion: Fpresentacion,
            Tpresentación: Tpresentación,
            LugarT: LugarT,
            LugarP: LugarP,
            Acumulado: Acumulado,
            Asistente: Asistente,
            FormaConclucion: FormaConclucion,
            FechaConclucion: FechaConclucion,
            nResolucion: nResolucion,
          }];

          let nPersonasNJ = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr')).length;

          var Datos = []
          for (let i = 2; i <= nPersonasNJ; i++) {
            let Nombre = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
            let DocumentoI = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
            let DomicilioP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3)', e => e.innerText.trim());
            let PDProvincia = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(4)', e => e.innerText.trim());
            Datos.push({
              Nombre: Nombre,
              DocumentoI: DocumentoI,
              DomicilioP: DomicilioP,
              PDProvincia: PDProvincia,
            });
          }

          var ArrayDenunciantes = Datos.findIndex(obj => obj.Nombre == "Denunciantes");
          var ArrayDenunciados = Datos.findIndex(obj => obj.Nombre == "Denunciados");
          var denunciados = []
          var denunciantes = []
          for (let j = ArrayDenunciantes + 1; j < ArrayDenunciados; j++) {
            denunciados.push(Datos[j])
          }
          for (let j = ArrayDenunciados + 1; j < Datos.length; j++) {
            denunciantes.push(Datos[j])
          }
          let Notications = [];
          let nNotificaciones = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr')).length;
          for (let i = 2; i < nNotificaciones; i++) {
            let fecha = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
            let detalle = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
            Notications.push({
              fecha: fecha,
              detalle: detalle
            });
          }


          res.status(200).json({
            NExpediente: NExpediente,
            denunciantes,
            denunciados,
            Notications,
            result: true
          });

        } catch (error) {
          res.status(200).json({
            mensaje: "No hay resultados coincidentes con los criterios de busqueda",
            result: false
          });
        }
        break;
      case 4:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCEB.jsp?pIdAreaMenu=4');
            await page.waitForTimeout(800)


            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=10');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 5:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCPC.jsp?pIdAreaMenu=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=14');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=10');
            break;
          case 4:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 6:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=2');
            break;
        }
        break;
      case 7:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=7');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=2');
            break;
        }
        break;
      case 8:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=3');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=2');
            break;
        }
        break;
      case 9:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaSPC.jsp?pId=8');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=2');
            break;
        }
        break;
    }


    await browser.close();

  })();
});


app.post('/CEJ2', cors(), (req, res) => {
  let data = req.body;

  console.log(data);

  (async () => {
    let codigo = data.cod;
    let idCas = data.id;
    const browser = await puppeteer.launch({
      headless: false
    });
    // const browser = await puppeteer.launch({ headless: 0, executablePath: '/Applications/Microsoft/Edge.app' });
    const page = await browser.newPage();
    let dom = "https://ie7.thecasetracking.com";
    let url = 'https://ie7.thecasetracking.com/ct/cases/proxy/Cej?code=' + codigo;
    await page.goto(url);
    let cookieset = await page.cookies(url)
    await page.waitForSelector('#root > div > div.auth-body.relative > div > div:nth-child(4) > form > div:nth-child(4) > button');
    await page.type('[id=email]', "asosa@dotnetsa.com");
    await page.type('[id=password]', "Lima_2019");

    await page.waitForTimeout(2000)
    //await page.waitForNavigation();
    await page.click('#rememberMe');
    await page.click('#root > div > div.auth-body.relative > div > div:nth-child(4) > form > div:nth-child(4) > button')
    await page.waitForNavigation();

    await page.waitForSelector('#dashboard > div.box')

    await page.close();
    const page2 = await browser.newPage(); // open new tab2
    await page2.goto(url);

    const link = await page2.$$eval('body > iframe[src]', imgs => imgs.map(img => img.getAttribute('src')));

    const page3 = await browser.newPage();
    await page2.close(); // open new tab2
    await page3.goto(dom + link);
    await page3.waitForTimeout(10000)

    await page3.waitForSelector('#collapseThree')
    let tabla = await page3.$eval('#accordionX', e => e.innerText.trim());
    let orgJuris = await page3.$eval('#gridRE > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
    let disJuris = await page3.$eval('#gridRE > div:nth-child(2) > div:nth-child(4)', e => e.innerText.trim());
    let juez = await page3.$eval('#gridRE > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
    let espLegal = await page3.$eval('#gridRE > div:nth-child(3) > div:nth-child(4)', e => e.innerText.trim());
    let fInicio = await page3.$eval('#gridRE > div:nth-child(4) > div:nth-child(2)', e => e.innerText.trim());
    let proceso = await page3.$eval('#gridRE > div:nth-child(4) > div:nth-child(4)', e => e.innerText.trim());
    let obs = await page3.$eval('#gridRE > div:nth-child(5) > div:nth-child(2)', e => e.innerText.trim());
    let esp = await page3.$eval('#gridRE > div:nth-child(5) > div:nth-child(4)', e => e.innerText.trim());
    let materia = await page3.$eval('#gridRE > div:nth-child(6) > div:nth-child(2)', e => e.innerText.trim());
    let estado = await page3.$eval('#gridRE > div:nth-child(6) > div:nth-child(4)', e => e.innerText.trim());
    let eProcesal = await page3.$eval('#gridRE > div:nth-child(7) > div:nth-child(2)', e => e.innerText.trim());
    let fConclusion = await page3.$eval('#gridRE > div:nth-child(7) > div:nth-child(4)', e => e.innerText.trim());
    let ubicacion = await page3.$eval('#gridRE > div:nth-child(8) > div:nth-child(2)', e => e.innerText.trim());
    let mConclusion = await page3.$eval('#gridRE > div:nth-child(8) > div:nth-child(4)', e => e.innerText.trim());
    let sumilla = await page3.$eval('#gridRE > div:nth-child(9) > div:nth-child(2)', e => e.innerText.trim());
    //let part = await page3.$eval('#collapseTwo', e => e.innerText.trim());

    let movs = await page3.$eval('#collapseThree', e => e.innerText.trim());
    //console.log(movs);
    let xTipo = await page3.$eval('#collapseTwo > div > div:nth-child(1) > div:nth-child(1)', e => e.innerText.trim());
    let nPart = (await page3.$$('#collapseTwo > div > div')).length;
    let strPart = "";


    for (let i = 2; i <= nPart; i++) {
      //let nA = intA+i;
      //let fechaData= await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child('+ i+') > td:nth-child(1)', e => e.innerText.trim());
      //let actividad= await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child('+i+') > td:nth-child(2)', e => e.innerText.trim());

      //let xTipPart = await page3.$eval('#collapseTwo > div > div:nth-child('+(i)+') > div:nth-child(1)', e => e.innerText.trim()); 
      let xPart = await page3.$eval('#collapseTwo > div > div:nth-child(' + i + ')', e => e.innerText.trim());


      //let xTipo = await page3.$eval('#collapseTwo > div > div:nth-child(2) > div:nth-child(1)', e => e.innerText.trim());
      //console.log(i)
      if (nPart == i) {
        strPart += xPart
      } else {
        strPart += "(" + xTipo + ")" + xPart + "|"
      }


    }

    var sPart = strPart.split("\n").join(" ");

    jsonCEJ = '[{"OrganoJurisdiccional":' + '"' + orgJuris + '",' +
      '"DistritoJudicial":' + '"' + disJuris + '",' +
      '"Juez":' + '"' + juez + '",' +
      '"EspecialistaLegal":' + '"' + espLegal + '",' +
      '"FechadeInicio":' + '"' + fInicio + '",' +
      '"Proceso":' + '"' + proceso + '",' +
      '"Observacion":' + '"' + obs + '",' +
      '"Especialidad":' + '"' + esp + '",' +
      '"Materia":' + '"' + materia + '",' +
      '"Estado":' + '"' + estado + '",' +
      '"EtapaProcesal":' + '"' + eProcesal + '",' +
      '"FechaConclusión":' + '"' + fConclusion + '",' +
      '"Ubicación":' + '"' + ubicacion + '",' +
      '"MotivoConclusión":' + '"' + mConclusion + '",' +
      '"Sumilla":' + '"' + sumilla + '",' +
      '"Participantes":' + '"' + sPart + '"' + '}]';

    // let sql = "CALL usp_Casos_Movs_Insert("+idCas+",'" + jsonCEJ + "','','')"

    await page3.waitForTimeout(1000)

    let xPartNotif = (await page3.$$('#collapseThree > div')).length;





    console.log("numerode notificaciones: " + xPartNotif);


    //#pnlSeguimiento1

    let notFechaResolucion = [];
    let notResolucion = [];
    let notTNotificacion = [];
    let notActo = [];
    let notFojas = [];
    let notProveido = [];
    let notSumilla = [];
    let notDesUser = [];
    //let notF
    for (let i = 1; i < xPartNotif; i++) {
      // let dataMovs = await page3.$eval('#pnlSeguimiento'+i, e => e.innerText.trim());

      //for(let j=1;j<)
      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
      //let xtest1 = await page3.$eval('#pnlSeguimiento'+i+' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
      //#divResol > div > div.row > div:nth-child(1) > div:nth-child(1)
      notFechaResolucion.push(xtest)




    }

    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

      notResolucion.push(xtest)

    }

    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());

      notTNotificacion.push(xtest)

    }

    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());

      notActo.push(xtest)


    }


    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

      notFojas.push(xtest)

    }

    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());

      notProveido.push(xtest)

    }


    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());

      notSumilla.push(xtest)

    }


    for (let i = 1; i < xPartNotif; i++) {

      let xtest = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

      notDesUser.push(xtest)

    }

    await page3.waitForTimeout(5000)

    let dataDet = [];
    let dataNotif = [];

    for (let i = 1; i < xPartNotif; i++) {

      //console.log(i);



      try {

        let nPartNotifDet = (await page3.$$('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div > div')).length;

        //console.log("Num Divs: ",i, ", N:",nPartNotif);


        for (let j = 1; j <= nPartNotifDet; j++) {
          //console.log(i," subid:",j) ;

          try {
            let titulo = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > h5', e => e.innerText.trim());
            let xDestinatario = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
            let xFecha = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
            let xAnexos = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
            let xFormaEntrega = await page3.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
            //let xxx =           await page3.$eval('#pnlSeguimiento'+i+' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child('+j+') > div > h5',e=>e.innerText.trim());

            let arr = {
              idMov: i - 1,
              titulo: titulo,
              destinatario: xDestinatario,
              fechaEnvio: xFecha,
              anexos: xAnexos,
              formaEntrega: xFormaEntrega,
            }
            dataDet.push(arr)
            //console.log(xxx)

          } catch (error) {

            dataDet.push(null)
            //console.log(i, error)
          }

        }




      } catch (error) {
        dataDet.push(null)

        //console.log("error, con el id: ",i, "\n",error)
      }




    }

    //console.log(dataDet);
    //notProveido.push(xtest)








    //console.log(notSumilla,notDesUser)






    /*for(let i=1;i<xPartNotif;i++){
       
     let xtest = await page3.$eval('#pnlSeguimiento'+i+' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(4) > div:nth-child(2)', e => e.innerText.trim());
     
     notActo.push(xtest)
     
   }*/

    let dataFinal = [];

    for (let k = 0; k < xPartNotif - 1; k++) {

      //dataFinal = '[{"fechaResolución":'+'"'+notFechaResolucion[k]+'","resolucion":"'+notResolucion[k]+'","tipoNot":"'+notTNotificacion[k]+'","acto":"'+notActo[k]+'","fojas":"'+notFojas[k]+'"}]'+

      let data = {
        idMov: k,
        notFecha: notFechaResolucion[k],
        resolucion: notResolucion[k],
        tipoNot: notTNotificacion[k],
        sumilla: notSumilla[k],
        comUsu: notDesUser[k],
        acto: notActo[k],
        fojas: notFojas[k],
        proveido: notProveido[k],
        notificaciones: []



      }

      //console.log(dataDet[0].idMov);


      dataFinal.push(data);


    }


    const rex = dataFinal.map(invoice => {
      invoice.notificaciones = dataDet.filter((line) => line.idMov === invoice.idMov)
      return invoice;
    })

    //console.log(rex);


    //console.log(dataFinal);
    /*console.log("DATA FINAL")
    
    
    
    
    
      console.log("FECHAS---------------")
      console.log(notFechaResolucion)
      console.log("RESOLUCIÓN---------------")
      console.log(notResolucion)
      console.log("TIPO DE NOTIFICACIÓN---------------")
      console.log(notTNotificacion)
    
      console.log("FOJAS--------------")
      
      console.log(notFojas)
    
      console.log("PROVEIDO---------------")
      console.log(notProveido)*/
    connection.query("CALL usp_Casos_Movs_Insert(?,?,?,?,?,?)", [
      idCas,
      jsonCEJ,
      JSON.stringify(rex),
      JSON.stringify(dataDet),
      '',
      ''

    ], (error, result) => {


      if (error) throw console.log('error al insetar', error);


      //console.log(result);
      if (result) {
        res.status(200).send({
          success: true
        });

      } else if (result == undefined) {
        res.status(500).send({
          error: error
        });
      } else {

        res.status(500).send({
          error: error
        });
      }
    })
  })();
});






/*
const transporter = nodemailer.createTransport({
  host: "mail.concursalperu.com",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    //C1@v€0bv1@
    //user: 'sistemas@dotnetsa.com',
    user: 'notificador@concursalperu.com',
    //C1@v€0bv1@
    pass: 'C1@v€0bv1@',
    //pass: 'C1@v€0bv1@',
  },
  tls: {
    rejectUnauthorized: 0
  }
})


});
*/

app.post('/mail/eventPrueba', cors(), async (req, res) => {

  /***********  Codigo de Hernan Rios ******** */
  let parametro = req.body.parametro;

  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
      pass: 'aybcwyjqemoauboy', // generated ethereal password
    },
  });

  var mailOptions = {
    from: '📨 "GPS Legal" <notificador@concursalperu.com>',
    to: arrayParticipantes[i].email,
    subject: 'NOTIFICACIÓN DE TAREA',
    text: 'GPS Legal',
    html: `<div> holi </div>`
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Mensaje enviajo');
    res.json({
      message: 'sended successfully'
    })
  });


  /*********** ********************* ******** */

  /*
    const transporter = nodemailer.createTransport({
      host: "mail.dotnetsa.com",
  
      //host: "mail.concursalperu.com",
      port: 465,
      secure: true,
      auth: {
        //C1@v€0bv1@
        //user: 'sistemas@dotnetsa.com',
        user: 'user@dotnetsa.com',
        //C1@v€0bv1@
        pass: 'Proyecto2',
        //pass: 'C1@v€0bv1@',
      },
      tls: {
        rejectUnauthorized: 0
      }
    })
   */

  if (parametro.tpoTbl == 'evento') { // <<<<<<  parametro referente a evento >>>>>
    console.log(req.body);
    let arrayParticipantes = req.body.arrayParticipantes;
    let evento = req.body.dataEvent;
    const nCli_Id = req.body.cliente;
    var nomCli = "";
    var ttlNotEve = "";
    var subTtlNotEve = "";


    console.log(req.body);

    const payload = {
      _ncasId: req.body.idCaso,
      _nCliId: nCli_Id,
      iat: moment().unix(),
      exp: moment().add(1, 'days').unix(),
    }
    const token = jsonwebtoken.sign({
      payload
    }, SECRET_KEY_JWT);
    console.log(token);


    evento.cEve_Descripcion = evento.cEve_Descripcion.substr(0, 150)

    if (parametro.tpoAct == 'insertar') {
      ttlNotEve = "Tenemos para usted un nuevo evento.";
      subTtlNotEve = "Se le ha incluido en el siguiente evento.";

      console.log("--------- consulta --------------------");
      var idEvent = req.body.idEvent;
      var fechaI2 = "";
      var fechaF2 = "";

      const sql = `SELECT DATE_FORMAT(dEve_FechaIni, '%d/%m/%Y') as 'dEve_FechaIni2',
        DATE_FORMAT(dEve_FechaIni, '%Y-%m-%d') as 'dEve_FechaIni',
        DATE_FORMAT(dEve_FechaFin, '%d/%m/%Y') as 'dEve_FechaFin2',
        DATE_FORMAT(dEve_FechaFin, '%Y-%m-%d') as 'dEve_FechaFin',
        DATE_FORMAT(tEve_HoraIni, '%h:%i %p') as 'tEve_HoraIni2',
        DATE_FORMAT(tEve_HoraFin, '%h:%i %p') as 'tEve_HoraFin2'
        FROM Eventos 
        where nEve_Id=${idEvent}`;
      connection.query(sql, (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          console.log(result[0]);
          fechaI2 = result[0].tEve_HoraIni2;
          fechaF2 = result[0].tEve_HoraFin2;
          console.log("+-+++++++++++++++++++++++++++++++");
          console.log(fechaI2);
          console.log(fechaI2);

          evento.tEve_HoraIni = fechaI2;
          evento.tEve_HoraFin = fechaF2;

        } else {
          res.send('success');
        }
      });

      console.log("--------- consulta --------------------");

    } else if (parametro.tpoAct == 'actualizar') {
      ttlNotEve = "Se ha actualizado un evento.";
      subTtlNotEve = "Se ha actualizado un evento donde usted participa.";

      evento.tEve_HoraIni = moment(evento.tEve_HoraIni, ["HH.mm"]).format("hh:mm a");
      evento.tEve_HoraFin = moment(evento.tEve_HoraFin, ["HH.mm"]).format("hh:mm a");

    }

    if (arrayParticipantes.length == 0) {
      res.json({
        status: 1,
        msj: "no existen participantes, no se envio ningun correo"
      });
    } else {
      const sql = `SELECT nCli_Id,CASE bCli_Tipo WHEN 1 THEN cCli_RazonSocial ELSE CONCAT(cCli_NombreCorto, ' ', cCli_RazonSocial, ' ', cCli_Materno) END AS 'cCli_NombreCompleto' 
      FROM Clientes where nCli_Id=${nCli_Id}`;
      connection.query(sql, (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          console.log(result[0].cCli_NombreCompleto);
          nomCli = result[0].cCli_NombreCompleto;
          //arrayParticipantes[2].email="hernanriosvalencia92@gmail.com";
          for (let i = 0; i < arrayParticipantes.length; i++) {

            var mailOptions = {
              from: '📨 "GPS Legal" <notificador@concursalperu.com>',
              to: arrayParticipantes[i].email,
              subject: 'NOTIFICACIÓN DE EVENTO',
              text: 'GPS Legal',
              html: `<div style="width: 100%; height: 700px;margin:auto">
  
              <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
              <div style="width: 60%;background-color: white;float: left;">
                <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                  <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                </div>
                <div style="width: 100%; background-color: white;margin-top: 10px;">
                  <h3 style="font-size: 24px;color: black;">
                    <strong style="margin-left: 40px;padding-left: 0px;">${ttlNotEve}</strong> 
                  </h3>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 5px;">
                  <h5 style="font-size: 14px;margin-left: 40px;">
                    <strong>${subTtlNotEve}</strong> 
                  </h5>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
            
                  <div style="width: 100%;background-color: white;">
                    <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                      <strong>Titulo</strong> 
                    </span>
                    <span style="font-size: 14px;margin-left: 20px;margin-right: 20px;">:</span>
                    <span style="font-size: 14px;">
                      ${evento.cEve_Titulo}
                    </span>
                  </div>
            
                  <div style="width: 100%;background-color: white; margin-top: 10px;">
                    <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                      <strong>Cliente</strong> 
                    </span>
              
                    <span style="font-size: 14px;margin-left: 20px;margin-right: 20px;">:</span>
              
                    <span style="font-size: 14px;">
                      ${nomCli}
                    </span>
                  </div>
            
                  <div style="width: 100%;background-color: white; margin-top: 10px;">
                    <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                      <strong>Fecha inicio</strong> 
                    </span>
              
                    <span style="font-size: 14px;margin-left: 16px;margin-right: 20px;">:</span>
              
                    <span style="font-size: 14px;">
                      ${evento.dEve_FechaIni} &nbsp; ${evento.tEve_HoraIni}
                    </span>
                  </div>
  
                  <div style="width: 100%;background-color: white; margin-top: 10px;">
                    <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                      <strong>Fecha Fin</strong> 
                    </span>
              
                    <span style="font-size: 14px;margin-left: 31px;margin-right: 20px;">:</span>
              
                    <span style="font-size: 14px;">
                      ${evento.dEve_FechaFin} &nbsp; ${evento.tEve_HoraFin}
                    </span>
                  </div>
  
                  <div style="width: 100%;background-color: white; margin-top:10px;display: block;">
            
                    <div style=" width: 26%;background-color: white;float: left;">
                      <span style="font-size: 14px;margin-left: 39px;margin-right: 0px;">
                      <strong>Ubicacion</strong>  
                      </span>
                      <span style="font-size: 14px;margin-left:32px">:</span>
                    </div>
              
                    <div style="width: 60%;min-width: 60px;background-color: white !important; margin-top: 0px;float: left;margin-left:0px">
                      <span style="font-size: 14px; margin-left:0px;min-width: 200px !important;min-height:150px !important">
                        ${evento.cEve_Ubicacion}
                      </span>
                    </div>
                    
                  </div>
            
                  <div style="width: 100%;background-color: white; margin-top: 10px;display: block;float: left;">
            
                    <div style="width: 26%;background-color: white; float: left;">
                    
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                      <strong>Descripción</strong>  
                      </span>
                      <span style="font-size: 14px;margin-left:19px">
                      :
                      </span>
                    
                    </div>
                    
              
                    <div style="width: 70%;background-color: white; margin-top: 0px;margin-left:0px;float: left;">
                      <label style="font-size: 14px; margin-left:0px" maxlength="50">
                        ${evento.cEve_Descripcion}
                      </label>
                    </div>
                    
                  </div>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 150px;text-align: center;">
                  <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                  <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                    <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                  </div>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                    <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                  <strong>Nuestra dirección de correo es:</strong><br>
                    <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                </div>
            
                <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
            
                  <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                  <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                    <span>Este correo electrónico fue enviado a su cuenta de correo electrónico<span>&nbsp;</span><a href="mailto:XXXX@gmail.com" style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" target="_blank">XXXX@gmail.com</a><span>&nbsp;</span>por<wbr>que
                      es suscriptor de GPS Legal y ha configurado su cuenta para recibir notificaciones. Si desea cambiar sus preferencias de notificación, ingrese al sistema al modulo de notificaciones..</span>
                  </div>
                  
                </div>
              </div>
              <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                </div>`
            }

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log(error);
              }
              console.log('Mensaje enviajo');
              res.json({
                message: 'sended successfully'
              })
            });
          }
        } else {
          res.send('success');
        }
      });
    }
  } else if (parametro.tpoTbl == 'tarea') { // <<<<<<  parametro referente a tareas >>>>>

    console.log(req.body);
    let arrayParticipantes = req.body.arrayParticipantes;
    let tarea = req.body.dataTarea;
    const nCli_Id = req.body.cliente;
    var nomCli = "";

    var ttlNotTar = "";
    var subTtlNotTar = "";


    if (arrayParticipantes.length == 0) {
      res.json({
        status: 1,
        msj: "no existen participantes, no se envio ningun correo"
      });
    } else {
      if (parametro.tpoAct == 'insertar') {
        ttlNotTar = "Tenemos para usted una nueva tarea.";
        subTtlNotTar = "Se le ha incluido en la siguiente tarea.";

        var idTar = req.body.idTar;
        console.log("--------- consulta --------------------");
        var fechaF2 = "";

        const sql = `SELECT 
                      DATE_FORMAT(tTar_Hora_Vence, '%h:%i %p') AS tTar_Hora_Vence2
                      FROM Tareas 
                      where nTar_Id=${idTar}`;
        connection.query(sql, (error, result) => {
          if (error) throw error;
          if (result.length > 0) {
            console.log(result[0]);
            fechaF2 = result[0].tTar_Hora_Vence2;
            console.log("+-+++++++++++++++++++++++++++++++");
            console.log(fechaF2);
            tarea.tTar_Hora_Vence = fechaF2;

          } else {
            res.send('success');
          }
        });

        console.log("--------- consulta --------------------");

      } else if (parametro.tpoAct == 'actualizar') {
        ttlNotTar = "Se ha actualizado una tarea.";
        subTtlNotTar = "Se ha actualizado una tarea donde usted participa.";

        tarea.tTar_Hora_Vence = moment(tarea.tTar_Hora_Vence, ["HH.mm"]).format("hh:mm a");
        console.log(tarea.tTar_Hora_Vence);
      }

      const sql = `SELECT nCli_Id,CASE bCli_Tipo WHEN 1 THEN cCli_RazonSocial ELSE CONCAT(cCli_NombreCorto, ' ', cCli_RazonSocial, ' ', cCli_Materno) END AS 'cCli_NombreCompleto' 
        FROM Clientes where nCli_Id=${nCli_Id}`;

      connection.query(sql, (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          console.log(result[0].cCli_NombreCompleto);
          nomCli = result[0].cCli_NombreCompleto;

          //arrayParticipantes[2].email="hernanriosvalencia92@gmail.com";

          for (let i = 0; i < arrayParticipantes.length; i++) {

            var mailOptions = {
              from: '📨 "GPS Legal" <notificador@concursalperu.com>',
              to: arrayParticipantes[i].email,
              subject: 'NOTIFICACIÓN DE TAREA',
              text: 'GPS Legal',
              html: `<div style="width: 100%; height: 700px;margin:auto">
    
                <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                <div style="width: 60%;background-color: white;float: left;">
                  <div style="width: 100%; background-color: white;text-align: center;margin-top: 50px;">
                    <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
                  </div>
                  <div style="width: 100%; background-color: white;margin-top: 10px;">
                    <h3 style="font-size: 24px;color: black;">
                      <strong style="margin-left: 40px;padding-left: 0px;">${ttlNotTar}</strong> 
                    </h3>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 5px;">
                    <h5 style="font-size: 14px;margin-left: 40px;">
                      <strong>${subTtlNotTar}</strong> 
                    </h5>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 45px; margin-bottom: 30px;"> <!-- cuerpo de datos -->
              
                    <div style="width: 100%;background-color: white;">
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 39px;">
                        <strong>Titulo</strong> 
                      </span>
                      <span style="font-size: 14px;margin-left: 50px;margin-right: 20px;">:</span>
                      <span style="font-size: 14px;">
                        ${tarea.cTar_Titulo}
                      </span>
                    </div>
              
                    <div style="width: 100%;background-color: white; margin-top: 10px;">
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 30px;">
                        <strong>Cliente</strong> 
                      </span>
                
                      <span style="font-size: 14px;margin-left: 50px;margin-right: 20px;">:</span>
                
                      <span style="font-size: 14px;">
                        ${nomCli}
                      </span>
                    </div>
              
                    <div style="width: 100%;background-color: white; margin-top: 10px;">
                      <span style="font-size: 14px;margin-left: 40px;margin-right: 0px;">
                        <strong>Fecha vencimiento</strong> 
                      </span>
                
                      <span style="font-size: 14px;margin-left: 0px;margin-right: 20px;">:</span>
                
                      <span style="font-size: 14px;">
                        ${tarea.dTar_Fec_Vence} &nbsp; ${tarea.tTar_Hora_Vence}
                      </span>
                    </div>
              
                   
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 50px;text-align: center;">
                    <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
                    <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                      <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                    </div>
                    <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                      <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                    </div>
                    <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                      <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
                    </div>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                      <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                    <strong>Nuestra dirección de correo es:</strong><br>
                      <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
                  </div>
              
                  <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
              
                    <div style="width: 10%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                    <div style="width: 80%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                      <span>Este correo electrónico fue enviado a su cuenta de correo electrónico<span>&nbsp;</span><a href="mailto:XXXX@gmail.com" style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" target="_blank">XXXX@gmail.com</a><span>&nbsp;</span>por<wbr>que
                        es suscriptor de GPS Legal y ha configurado su cuenta para recibir notificaciones. Si desea cambiar sus preferencias de notificación, ingrese al sistema al modulo de notificaciones..</span>
                    </div>
                    
                  </div>
                </div>
                <div style="width: 20%; height: 700px; background-color: white;float: left;"></div>
                 </div>`
            }

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log(error);
              }
              console.log('Mensaje enviajo');
              res.json({
                message: 'sended successfully'
              })
            });
          }

        } else {
          res.send('success');
        }
      });
    }

  } else if (parametro.tpoTbl == 'ingreso') {
    console.log(req.body);
    console.log("entro a ingreso");
  } else if (parametro.tpoTbl == 'egreso') {
    console.log(req.body);
    console.log("entro a egreso");
  }





  //********************************************************************************** */

});

app.post('/Correo/List/Filter', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.user,
    e.type,
    e.fechaini,
    e.fechafin
  ]

  console.log(data)

  const sql = 'Call cor_sp_Listar_Correo_filter(?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.json({
        status: 0,
        message: 'error'
      });
    }
  });
});

/* FIN Dash cliente */
connection.connect(error => {
  if (error) throw error;
  console.log('Server is running!');
});

var json1 = [{
  "Acto": "DECRETO",
  "Fojas": "1",
  "CodigoC": "1",
  "Sumilla": "ESTANDO A LO QUE SE EXPONE, A CONOCIMIENTO DEL DEMANDADO POR EL TÉRMINO DE TRES DIAS LA INTERRUPCIÓN DEL PLAZO DE LA RESOLUCIÓN NÚMERO TREINTA SOLICITADO POR EL RECURRENTE",
  "Moviento": 1,
  "Proveido": "15112021",
  "Resolucion": "TREINTA Y DOS",
  "Notificacion": [],
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "TNotificacion": "",
  "cFechaDescarga": "Descargado el 24 de Noviembre de 2021",
  "FechaResolucion": "15112021",
  "ComentarioUsuario": "DESCARGADO POR: GOMEZ GARDEZ, KELLY ANGIE"
},
{
  "Acto": "DECRETO",
  "Fojas": "1",
  "CodigoC": "1",
  "Sumilla": "NO HABIENDO CUMPLIDO EL APODERADO DE LA DEMANDANTE CON SEÑALAR DE FORMA CLARA Y PRECISE EL ACTO PROCESAL DEL QUE SOLICITA SU SUSPENSIÓN, SIENDO ESTO ASÍ, CARECE DE OBJETO LO SOLICITADO",
  "Moviento": 2,
  "Proveido": "15112021",
  "Resolucion": "TREINTA Y UNO",
  "Notificacion": [],
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "TNotificacion": "",
  "cFechaDescarga": "Descargado el 24 de Noviembre de 2021",
  "FechaResolucion": "15112021",
  "ComentarioUsuario": "DESCARGADO POR: GOMEZ GARDEZ, KELLY ANGIE"
},
{
  "Acto": "ESCRITO",
  "Fojas": "8",
  "CodigoC": "1",
  "Sumilla": "LO QUE SE INDICA",
  "Moviento": 3,
  "Proveido": "15112021",
  "Resolucion": "TREINTA Y DOS",
  "Notificacion": [],
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "TNotificacion": "",
  "cFechaDescarga": "Descargado el 24 de Noviembre de 2021",
  "FechaResolucion": "08112021 15:55",
  "ComentarioUsuario": "INGRESADO POR:USUARIO DE MESA DE PARTES WEB"
},
{
  "Acto": "DECRETO",
  "Fojas": "1",
  "CodigoC": "1",
  "Sumilla": "ESTANDO A LO SOLICITADO, SE LE CONCEDE UN PLAZO EXCEPCIONAL DE TRES DÍAS AL RECURRENTE A FIN DE QUE CUMPLA CON CANCELAR EL CINCUENTA POR CIENTO RESTANTE DE LOS HONORARIOS PROFESIONALES DEL PERITO SEÑALADOS POR LA RESOLUCIÓN DIEZ, BAJO APERCIBIMIENTO DE IMPONÉRSELE NUEVA MULTA",
  "Moviento": 4,
  "Proveido": "18102021",
  "Resolucion": "TREINTA",
  "Notificacion": [],
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "TNotificacion": "Pta. Cedula Not.",
  "cFechaDescarga": "Descargado el 24 de Noviembre de 2021",
  "FechaResolucion": "18102021",
  "ComentarioUsuario": "DESCARGADO POR: BRINGAS MUÑOZ, RAMIRO"
}
]

var json2 = [{
  "Moviento": 1,
  "CodigoC": "1",
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "FechaResolucion": "15/11/2021",
  "Resolucion": "TREINTA Y DOS",
  "TNotificacion": "Pta. Cedula Not.",
  "Acto": "DECRETO",
  "Fojas": "1",
  "Proveido": "15/11/2021",
  "Sumilla": "ESTANDO A LO QUE SE EXPONE, A CONOCIMIENTO DEL DEMANDADO POR EL TÉRMINO DE TRES DIAS LA INTERRUPCIÓN DEL PLAZO DE LA RESOLUCIÓN NÚMERO TREINTA SOLICITADO POR EL RECURRENTE",
  "ComentarioUsuario": "DESCARGADO POR: GOMEZ GARDEZ, KELLY ANGIE",
  "cFechaDescarga": "Descargado el 25 de Noviembre de 2021",
  "Notificacion": []
},
{
  "Moviento": 2,
  "CodigoC": "1",
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "FechaResolucion": "15/11/2021",
  "Resolucion": "TREINTA Y UNO",
  "TNotificacion": "Pta. Cedula Not.",
  "Acto": "DECRETO",
  "Fojas": "1",
  "Proveido": "15/11/2021",
  "Sumilla": "NO HABIENDO CUMPLIDO EL APODERADO DE LA DEMANDANTE CON SEÑALAR DE FORMA CLARA Y PRECISE EL ACTO PROCESAL DEL QUE SOLICITA SU SUSPENSIÓN, SIENDO ESTO ASÍ, CARECE DE OBJETO LO SOLICITADO",
  "ComentarioUsuario": "DESCARGADO POR: GOMEZ GARDEZ, KELLY ANGIE",
  "cFechaDescarga": "Descargado el 25 de Noviembre de 2021",
  "Notificacion": []
},
{
  "Moviento": 3,
  "CodigoC": "1",
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "FechaResolucion": "08/11/2021 15:55",
  "Resolucion": "TREINTA Y DOS",
  "TNotificacion": "",
  "Acto": "ESCRITO",
  "Fojas": "8",
  "Proveido": "15/11/2021",
  "Sumilla": "LO QUE SE INDICA",
  "ComentarioUsuario": "INGRESADO POR:USUARIO DE MESA DE PARTES WEB",
  "cFechaDescarga": "Descargado el 25 de Noviembre de 2021",
  "Notificacion": []
},
{
  "Moviento": 4,
  "CodigoC": "1",
  "CodigoExterno": "07037-2018-0-1801-JR-CI-22",
  "FechaResolucion": "18/10/2021",
  "Resolucion": "TREINTA",
  "TNotificacion": "Pta. Cedula Not.",
  "Acto": "DECRETO",
  "Fojas": "1",
  "Proveido": "18/10/2021",
  "Sumilla": "ESTANDO A LO SOLICITADO, SE LE CONCEDE UN PLAZO EXCEPCIONAL DE TRES DÍAS AL RECURRENTE A FIN DE QUE CUMPLA CON CANCELAR EL CINCUENTA POR CIENTO RESTANTE DE LOS HONORARIOS PROFESIONALES DEL PERITO SEÑALADOS POR LA RESOLUCIÓN DIEZ, BAJO APERCIBIMIENTO DE IMPONÉRSELE NUEVA MULTA",
  "ComentarioUsuario": "DESCARGADO POR: BRINGAS MUÑOZ, RAMIRO",
  "cFechaDescarga": "Descargado el 25 de Noviembre de 2021",
  "Notificacion": []
}
]

let jsonmio1 = [{
  "id": "6"
},
{
  "id": "5"
},
{
  "id": "4"
},
{
  "id": "3"
},
{
  "id": "2"
},
{
  "id": "1"
},
]

let jsonmio2 = [{
  "id": "3"
},
{
  "id": "2"
},
{
  "id": "5"
},
{
  "id": "1"
},
{
  "id": "4"
},
{
  "id": "6"
},
]


app.post('/Dashboard/Casos/NovedadesMail', cors(), (req, res) => {

  console.log(req.body);

  var novedades = req.body.JsonNovedad;
  var Notificaciones = req.body.JsonNotif.slice(0, 6);
  var CodExterno = req.body.CodExt;
  var _idCaso = req.body.nCas_Id;

  if (Notificaciones.length == 0 && novedades.length == 0) {
    console.log("No encontro novedades");
    res.status(500).json({
      result: false,
      mensaje: "Ups Error"
    });
  } else {
    const titulo = "Usted tiene las siguientes novedades";
    const cantNovedades = novedades.length + Notificaciones.length;

    var htmlElementsCabecera = "";
    var htmlElements = "";


    const sql = 'call ca_sp_Buscar_Casos(?)';
    connection.query(sql, _idCaso, (error, result) => {
      if (error) throw error;

      if (result.length > 0) {
        console.log(result);
        let nombreCompleto = result[0][0].cCli_NombreCompleto;
        console.log();

        if (novedades.length > 0) {
          for (var i = 0; i < novedades.length; i++) {
            htmlElements +=
              `<div style="width:100%;display:block;margin-top:10px">
                  <p><b>${i + 1}. ${novedades[i]["Acto"]} </b> ${novedades[i]["Sumilla"]}</p>
               </div>`;
            for (var i2 = 0; i2 < Notificaciones.length; i2++) {
              htmlElements +=
                `<div style="width:100%;display:block;margin-top:10px">
                  <p><b>${Notificaciones[i2]["titulo"]}</b> ${Notificaciones[i2]["destinatario"]} ${Notificaciones[i2]["fechaEnvio"]}</p>
               </div>`;
            }
          }

        } else {
          for (var i = 0; i < Notificaciones.length; i++) {
            htmlElements +=
              `<p>Notificaciones encontradas: ${Notificaciones.length}</p>
              <div style="width:100%;display:block;margin-top:10px">
                  <p><b>${i + 1}. ${Notificaciones[i]["titulo"]} </b></p>
               </div>`;
          }
        }

        htmlElementsCabecera += `

            <div style="width: 100%; height: 200px;margin:auto;text-align:center;padding-top:30px">
              <img src='https://mcusercontent.com/ed85e4228211118429465984c/images/65f96943-ae91-35ae-af14-fa0c45aeb244.png' style="width: 600px;text-align: center;">
            </div>

            <div style="width:100%; margin-left:10%">
              <div style="width:100%;margin-top:48px";color:blue;> Estimado/a: <b>${nombreCompleto}</b>   </div>
              <div style="width:100%;display:block;margin-top:8px">${titulo}</div>
              <div style="width:100%;display:block;margin-top:8px;">Cantidad de novedades: ${cantNovedades}</div>
              <div style="width:100%;display:block;margin-top:30px;">
                <div style="width:100%;display:block;margin-top:10px">Codigo externo: <b style="color:#DD0000">${CodExterno}</b> </div>
                <div style="width:75%;margin-left:20px">
                  ${htmlElements}
                </div>
                
              </div>
              
            </div>

            <div style="width: 100%;background-color: white;margin-top: 20px;text-align: center;">
                  
            <div style="width: 100%;background-color: white;padding-top: 70px;text-align: center;">
              <div style="width: 44%;background-color: white;margin-top: 5px; text-align: center;float: left;"></div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://www.twitter.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.twitter.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHK4gxx85IYZmQndDsZBszUbttS0A"><img src="https://ci5.googleusercontent.com/proxy/9JCidMa5VknLAPg4RYMmH7FGGVE7GTqtQfZp9sA6f68f38QxOQjaZjOzu4PYlR28CsRZztKJCldsl4XY02xdjYH6F1CkIDvevrQ6WPEMdEVx5YYDxBx0t302TgzwTA=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://www.facebook.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.facebook.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHbp6FADuN-J0HDRdWWpcWxuaAMnw"><img src="https://ci5.googleusercontent.com/proxy/KLWDyxU_2JT5nOGTE6_NSp-hT37kpCU8B8HLih6GyBnhKJEvCDQsIeq4uLfJ7CQWsSCfpfcbCXVh74IrAuFYiXcU4R2sPN1CInMYwE7DpPIiYM9dGmBbl7FrtmeFZ6I=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
              <div style="width: 5%;background-color: white;margin-top: 5px; text-align: center;float: left;">
                <a href="http://mailchimp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://mailchimp.com/&amp;source=gmail&amp;ust=1634312086533000&amp;usg=AFQjCNHKU8Sg22rEbYb8gAjxru-IBIJHIw"><img src="https://ci5.googleusercontent.com/proxy/FR4I0VM10pxcUwbQ63iIF6cAOqyzEbM1yC4ru84XQ1cT1RbvvmtJzUt4RdH1WUB452ecisGFRwh877ppJp5BhUmQhUWIABs5JUY80JFlBF08huivKdmS6R-dPg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Website" height="24" width="24" style="border:0px;height:auto;outline:none;text-decoration:none;display:block" class="CToWUd"></a>
              </div>
            </div>
                        
              <div style="width: 100%;background-color: white;margin-top: 55px;display: inline-block; text-align: center;">
                  <em>Copyright ©<span>&nbsp;</span></em>GPS Legal<em>, Todos los derechos reservados.</em><br>
              </div>
          
              <div style="width: 100%;background-color: white;margin-top: 25px;text-align: center;">
                <strong>Nuestra dirección de correo es:</strong><br>
                  <a style="color:rgb(101,101,101);font-weight:normal;text-decoration:underline" href="mailto:Atencionalcliente@gpslegal.pe" target="_blank">Atencionalcliente@gpslegal.pe</a><br>
              </div>
          
              <div style="width: 100%;background-color: white;margin-top: 15px;text-align: center;">
          
                <div style="width: 15%;background-color: white;margin-top: 5px;text-align: center;float: left;"></div>
                <div style="width: 70%;background-color: white;margin-top: 5px;text-align: center;float: left;">
                  <span>Este correo electrónico ha sido enviado porque es suscriptor de GPS Legal. Si desea cambiar sus preferencias de notificación, ingrese al sistema en el módulo de notificaciones.</span>
                </div>
                
              </div>
        
            
            `;

        let transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          port: 465,
          secure: true, // true for 465, false for other ports
          auth: {
            user: 'hernanriosvalencia92@gmail.com', // generated ethereal user
            pass: 'aybcwyjqemoauboy', // generated ethereal password
          },
        });

        const sqlCons = "Call eq_sp_Listar_Usuarios(?,?)";
        connection.query(sqlCons, [_idCaso, null], (error, result) => {

          console.log(result);

          for (let i = 0; i < result[0].length; i++) {

            var mailOptions = {
              from: '📨 "GPS Legal" <notificador@concursalperu.com>',
              to: result[0][i].email,
              subject: 'GPS LEGAL - Nuevos movimientos',
              text: 'GPS Legal',
              html: htmlElementsCabecera
            }

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log(error);
              }
              console.log('Mensaje enviajo');
              res.json({
                message: 'sended successfully'
              })
            });

          }

        });

      }
    });

  }






});



app.post('/Dashboard/Casos/Novedades/Insertar', cors(), (req, res) => {
  let e = req.body;
  let data = [e.nCas_Id,
  JSON.stringify(e.cNov_Movimiento).replace(/\//g, '').toString(),
  JSON.stringify(e.cNov_Notificacion).replace(/\//g, '').toString()
  ]

  console.log(data)
  const sql = 'Call nov_sp_Insertar_Novedades(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Dashboard/Casos/Novedades/Listar', cors(), (req, res) => {
  let e = req.body;
  let data = [e.nNov_Estado]

  const sql = 'Call nov_sp_Listar_Novedades(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Dashboard/Casos/Novedades/CambiarEstado', cors(), (req, res) => {
  let e = req.body;
  let data = [e.nNov_Id]
  const sql = 'Call nov_sp_CambiarEstado_Novedades(?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

/***** Horas laboradas por cliente */
app.post('/Dashboard/HorasLaboradas', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nUsu_ID,
    e.M,
    e.Y
  ];
  console.log(data);
  const sql = 'Call lab_sp_Listar_Laborado_DashboardByUsuario(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log("Resultado", result[0]);
    if (result[0].length == 0) {
      res.json(result[0]);
    } else {

      if (result.length > 0) {
        res.json(result[0]);
      } else {
        res.send('success');
      }

    }


  });
});

/***** Meses laborados por cliente */
app.post('/Dashboard/MesesLaborados', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nUsu_ID,
    e.nCas_Id,
    e.nCli_Id,
    e.M,
    e.Y
  ];
  const sql = 'Call lab_sp_Listar_LaboradoMeses_DashboardByUsuario(?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }


  });
});

/***** Eliminar participantes caso */
app.post('/Participantes/EliminarParticipantes', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id,
    e.p_cCas_Tab_TipoEquipo,
    e.nUsu_Id
  ]

  console.log(data);
  console.log("consultando");
  const sql = 'Call eqc_sp_Eliminar_Participante(?,?,?)';

  connection.query(sql, data, (error, result) => {


    if (error) throw error;

    console.log(result);
    res.send(result);


  });

});

/***** Eliminar casos fisicamente */
app.post('/Casos/Eliminar_Fisicamente', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id
  ]

  console.log(data);
  console.log("consultando");
  const sql = 'Call ca_sp_Eliminar_Fisicamente(?)';

  connection.query(sql, data, (error, result) => {
    res.send('success');
  });

});

/*** Buscar equipo casos por usuario */
app.post('/EQU/BuscarUsuarios', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e.nEqu_ID
  ]

  console.log(data)
  const sql = 'Call equ_sp_BuscarUsuarios(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      console.log(result)
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


/*** Buscar equipo casos por usuario */
app.post('/EQU/InsParticipantesFromEC', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nEqu_ID,
    e.nCas_Id,
  ]

  console.log(data);
  console.log("consultando 000000");
  const sql = 'Call equ_sp_InsParticipantesFromEC(?,?)';

  connection.query(sql, data, (error, result) => {

    if (error) throw error;
    console.log(result);
    res.send(result);

  });

});

/*** Eliminar participantes menos al abogado responsable y al usuario */
app.post('/EQU/DeleteParticipantesExceptoAR', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nCas_Id
  ]

  console.log(data);
  console.log("consultando 000000");
  const sql = 'Call eqC_sp_eliminarEQC(?)';

  connection.query(sql, data, (error, result) => {

    if (error) throw error;
    console.log(result);
    res.send(result);

  });

});


/*** listar departamentos */
app.get('/ubi/ListarDepartamentos', cors(), (req, res) => {
  console.log("55555555555");
  const sql = 'Call Ubi_sp_ListarDepa()';
  connection.query(sql, (error, result) => {

    if (error) throw error;
    console.log(result);
    res.send(result[0]);

  });
});

/*** listar provincias */
app.post('/ubi/ListarProvincias', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e._depa
  ]

  console.log(data);
  const sql = 'Call Ubi_sp_ListarProv(?)';
  connection.query(sql, data, (error, result) => {

    if (error) throw error;
    console.log(result);
    res.send(result[0]);

  });
});

/*** listar distritos */
app.post('/ubi/ListarDistritos', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e._prov
  ]

  console.log(data);

  const sql = 'Call Ubi_sp_ListarDist(?)';
  connection.query(sql, data, (error, result) => {

    if (error) throw error;
    console.log(result);
    res.send(result[0]);

  });
});


/***** Eliminar participantes evento */
app.post('/Evento/Participantes/EliminarParticipantes', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nEve_Id,
    e.nUsu_Id
  ]

  console.log(data);
  console.log("consultando");
  const sql = 'Call ev_sp_EliminarParticipante(?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result);
    res.send(result);
  });

});

/***** Eliminar participantes tarea */
app.post('/Tarea/Participantes/EliminarParticipantes', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.nTar_Id,
    e.nUsu_Id
  ]

  console.log(data);
  console.log("consultando");
  const sql = 'Call ta_sp_EliminarParticipante(?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result);
    res.send(result);
  });

});

/***** Validar si el caso tiene horas laboradas */
app.post('/Caso/validarHoras', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e._nCas_Id
  ]

  console.log(data);
  const sql = 'Call la_sp_validarHorasCaso(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result);
    res.send(result);
  });
});

/** Listar los participantes del caso */
app.post('/Caso/Participantes_Caso_All', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e._nCas_Id
  ]

  console.log("data");
  console.log(data);
  const sql = 'Call eqc_sp_Participantes_Caso_All(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});

app.post('/Liquidacion/ActualizarFactura', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.Factura,
    e.Codigo,
    e.Estado,
    e.NumeroLiq,
    e.Estado2
  ]

  console.log("data");
  console.log(data);
  const sql = 'Call cl_sp_Actualizar_Factura(?,?,?,?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});


/** Obtener Moneda y monto del participante seleccionado en horas laboradas */
app.post('/Caso/GetMonedaMonto_UsuarioCaso', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e._nCas_Id,
    e._nUsu_Id
  ]

  console.log("data");
  console.log(data);
  const sql = 'Call la_sp_GetMonedaMonto_UsuarioCaso(?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});



/**  Creacion de PDF  johan */
const pdf = require("html-pdf");


app.post('/Generar/ReportePDF', (req, respuesta) => {

  var file = __dirname + "/FacturaPlantilla/index2.html";



  const ubicacionPlantilla = require.resolve(file);
  let contenidoHtml = fs.readFileSync(ubicacionPlantilla, 'utf8');
  var config = {
    // Export options
    "directory": "/tmp",
    "format": "A4",
    "orientation": "portrait", //portrait OR  landscape
    "border": {
      "top": "30px",
      "bottom": "30px",
      "right": "30px",
      "left": "30px"
    },
    paginationOffset: 1, // Override the initial pagination number
    /*
    "footer": {
      "height": "28mm",
      "contents": {
        first: 'Cover page',
        2: 'Second page', // Any page number is working. 1-based index
        default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
        last: 'Last Page'
      }
    },*/
  }


  let e = req.body;
  let data = [
    e.CodigoLiq,
    e.ArrayDatos,
    e.TGastos,
    e.THonorarios,
    e.Tipo,
  ]

  var Tipo = ""
  if (e.Tipo == "R" || e.Tipo == "RP") {
    Tipo = "R"
  } else {
    Tipo = e.Tipo
  }

  var fileSave = __dirname + "/FilesLiquidacion/" + Tipo + "-" + e.CodigoLiq + ".pdf";

  const sql = 'Call _report_PDF_ListaReporteLiquidacion(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {

      // Podemos acceder a la petición HTTP
      var CodLiquidacion = e.CodigoLiq;
      contenidoHtml = contenidoHtml.replace("{{Codigo}}", CodLiquidacion);

      var Cliente = e.ArrayDatos.cCli_NombreCompleto;
      contenidoHtml = contenidoHtml.replace("{{Cliente}}", Cliente);

      var Direccion = "Lima 351, Surco";
      contenidoHtml = contenidoHtml.replace("{{Direccion}}", Direccion);

      if (e.ArrayDatos.cCli_Fono1 == null) {
        e.ArrayDatos.cCli_Fono1 = "Sin teléfono"
      }

      var Telefono = e.ArrayDatos.cCli_Fono1;
      contenidoHtml = contenidoHtml.replace("{{Telefono}}", Telefono);

      var Caso = e.ArrayDatos.cCas_Titulo;
      contenidoHtml = contenidoHtml.replace("{{Caso}}", Caso);

      var CodExtCaso = e.ArrayDatos.cCas_Cod_Externo;
      contenidoHtml = contenidoHtml.replace("{{CodExtCaso}}", CodExtCaso);

      var Materia = e.ArrayDatos.cCas_Tab_Materia2;
      contenidoHtml = contenidoHtml.replace("{{Materia}}", Materia);

      var fechaProcesado = e.ArrayDatos.Fecha;
      contenidoHtml = contenidoHtml.replace("{{fechaProcesado}}", fechaProcesado);

      if (e.Tipo == "R" || e.Tipo == "RP") {
        var Estado = "Rechazado";
        contenidoHtml = contenidoHtml.replace("{{Estado}}", Estado);
      } else {
        var Estado = "";
        contenidoHtml = contenidoHtml.replace("{{Estado}}", Estado);
      }

      var MonedaH = result[0][0].Moneda;
      contenidoHtml = contenidoHtml.replace("{{MonedaH}}", MonedaH);

      var MonedaG = result[0][0].Moneda;
      contenidoHtml = contenidoHtml.replace("{{MonedaG}}", MonedaG);

      var Gastos = "";
      var Honorarios = "";

      for (var i = 0; i < result[0].length; i++) {
        if (result[0][i].Tipo == " 1") {
          Gastos += `<tr class="list-item">
          <td class="tableitem" width="10%" style="text-align: left;">${result[0][i].Fecha}</td>
          <td class="tableitem" width="20%" style="text-align: left;">${result[0][i].Motivo}</td>
          <td class="tableitem" width="50%" style="text-align: left;">${result[0][i].Detalle}</td>
          <td class="tableitem" width="20%" style="text-align: right;">${result[0][i].Monto}</td>
          </tr>`;
        } else {
          Honorarios += `<tr>
          <td class="tableitem" width="10%" style="text-align: left;">${result[0][i].Fecha}</td>
          <td class="tableitem" width="20%" style="text-align: left;">${result[0][i].Motivo}</td>
          <td class="tableitem" width="50%" style="text-align: left;">${result[0][i].Detalle}</td>
          <td class="tableitem" width="20%" style="text-align: right;">${result[0][i].Monto}</td>
          </tr>`;
        }

      }


      contenidoHtml = contenidoHtml.replace("{{Gastos}}", Gastos);
      const TGastos = e.TGastos;
      contenidoHtml = contenidoHtml.replace("{{TGastos}}", TGastos);

      contenidoHtml = contenidoHtml.replace("{{Honorarios}}", Honorarios);
      const THonorarios = e.THonorarios;
      contenidoHtml = contenidoHtml.replace("{{THonorarios}}", THonorarios);


      pdf.create(contenidoHtml, config).toFile(fileSave, function (err, res) {
        if (err) return console.log(err);

        respuesta.download(fileSave, function (errs) {
          if (errs) {
            console.log(errs)
          }
        })
      });
    } else {
      respuesta.send('success');
    }
  });




});

/** Listar Auditoria */
app.post('/Auditoria/listarCasos', cors(), (req, res) => {

  let e = req.body;
  let data = []

  const sql = 'Call au_sp_ListarAuditoriaCasos()';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});

app.post('/Auditoria/listar', cors(), (req, res) => {

  let e = req.body;
  let data = []

  const sql = 'Call au_sp_ListarAuditoria()';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});

/** Insertar Auditoria por usuario*/
app.post('/Auditoria/Filtro', cors(), (req, res) => {
  let e = req.body;
  let data = [
    e.nUsu_ID,
    e.cAud_nombre,
    e.FechaInicial,
    e.FechaFin,
  ]

  console.log(e);
  console.log(data);

  const sql = 'Call au_sp_ListarAuditoria_Filtro(?,?,?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  })
});

/** Insertar Auditoria por tabla*/
app.post('/Auditoria/listar/Tabla', cors(), (req, res) => {
  let e = req.body;
  let data = [
    _cTabCodigo
  ]

  const sql = 'Call au_sp_ListarAuditoria_Tabla(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  })
});


/** Insertar Auditoria */
app.post('/Auditoria/Insertar', cors(), (req, res) => {

  let e = req.body;
  let data = [
    e.cAud_nombre,
    e.nAud_IdReg,
    e.nUsu_ID,
    e.dAud_Fecha,
    e.nAud_Accion,
    e.cTabCodigo
  ]

  console.log(req.body);

  const sql = 'Call au_sp_InsertarAccion(?,?,?,?,?,?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.send(result[0]);
  });

});


app.post('/Descargar/ReportePDF', (req, respuesta) => {
  let e = req.body;
  let data = [
    e.CodigoLiq,
    e.Tipo,
  ]

  var fileSave = __dirname + "/FilesLiquidacion/" + e.Tipo + "-" + e.CodigoLiq + ".pdf";
  respuesta.download(fileSave, function (errs) {
    if (errs) {
      console.log(errs)
    }
  })
});

/** Notificaciones */
app.post('/config/notifications/user', (req, res) => {
  let e = req.body;
  let data = [
    e.iduser
  ]

  console.log(req.body);

  const sql = 'Call usp_Notificaciones_Select(?)';

  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    console.log(result[0]);
    res.json(result[0]);
  });
});

app.post('/comentarios/Insert', cors(), (req, res) => {

  let e = req.body;

  let data = [

    e.iduser,
    JSON.stringify(e.case).replace(/\//g, '').toString(),
    JSON.stringify(e.client).replace(/\//g, '').toString(),
    JSON.stringify(e.eventotaks).replace(/\//g, '').toString(),
    JSON.stringify(e.ingresoegreso).replace(/\//g, '').toString(),
    JSON.stringify(e.parametros).replace(/\//g, '').toString(),
    e.nNot_Movimientos,
    e.nNot_Comentarios,
    e.nNot_DiasCongelado

  ]

  console.log(data)

  const sql = 'Call usp_Notificaciones_Insert(?,?,?,?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Roles/Insertar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.cRol_Nombre,
    e.bRol_Estado
  ]

  const sql = 'Call rl_sp_InsertarRoles(?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});

app.post('/Roles/Actualizar', cors(), (req, res) => {

  let e = req.body;

  let data = [
    e.nRol_ID,
    e.cRol_Nombre,
    e.bRol_Estado
  ]


  const sql = 'Call rl_sp_UpdateRoles(?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


/*****************  Tarifas **********************/
app.post('/Tarifa/Insertar', cors(), (req, res) => {
  let emp = req.body;

  const sqlValidar = 'Call Ta_sp_Validar_Tarifa(?,?)'
  const sql = 'Call Tarifa_sp_Insertar_Tarifa(?,?,?)';


  connection.query(sqlValidar, [
    emp.cTar_Tab_Rol,
    emp.cTar_Tab_Moneda
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {

      if (result[0][0] == undefined) {
        console.log(result[0][0])
        connection.query(sql, [
          emp.cTar_Tab_Rol,
          emp.cTar_Tab_Moneda,
          emp.nTar_Monto
        ], (error, result) => {
          if (error) throw error;

          if (result.length > 0) {
            res.json(result[0]);
          } else {
            res.send('Error');
          }
        });
      }
      else {

        console.log("Ya existe un registro asi");
        console.log(result[0][0].cTar_Id);

        res.json({
          status: 501
        });

      }
    } else {
      connection.query(sql, [
        emp.cTar_Tab_Rol,
        emp.cTar_Tab_Moneda,
        emp.nTar_Monto
      ], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {
          res.json(result[0]);
        } else {
          res.send('Error');
        }
      });
    }
  });
});

app.post('/Tarifa/Actualizar', cors(), (req, res) => {
  let emp = req.body;

  const sqlValidar = 'Call Ta_sp_Validar_Tarifa(?,?)'
  const sql = 'Call Tarifa_sp_Actualizar_Tarifa(?,?,?,?)';

  connection.query(sqlValidar, [
    emp.cTar_Tab_Rol,
    emp.cTar_Tab_Moneda
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {



      if (result[0][0] == undefined) {
        console.log(result[0][0])
        connection.query(sql, [
          emp.cTar_Tab_Rol,
          emp.cTar_Tab_Moneda,
          emp.nTar_Monto,
          emp.cTar_Id
        ], (error, result) => {
          if (error) throw error;

          if (result.length > 0) {
            res.json(result[0]);
          } else {
            res.send('Error');
          }
        });
      }
      else {
        if (result[0][0].cTar_Id == emp.cTar_Id) {
          connection.query(sql, [
            emp.cTar_Tab_Rol,
            emp.cTar_Tab_Moneda,
            emp.nTar_Monto,
            emp.cTar_Id
          ], (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
              res.json(result[0]);
            } else {
              res.send('Error');
            }
          });
        }
        else {
          console.log("Ya existe un registro asi");
          console.log(result[0][0].cTar_Id);

          res.json({
            status: 501
          });
        }
      }
    } else {
      connection.query(sql, [
        emp.cTar_Tab_Rol,
        emp.cTar_Tab_Moneda,
        emp.nTar_Monto,
        emp.cTar_Id
      ], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {
          res.json(result[0]);
        } else {
          res.send('Error');
        }
      });
    }
  });

});

app.post('/Tarifa/listar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Tarifa_sp_Listar_Tarifa()';
  connection.query(sql, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);

    } else {
      res.send('Error');
    }
  });
});

app.post('/Tarifa/listar/Filtro', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Tarifa_sp_Listar_Tarifa_Filtro(?)';
  connection.query(sql, [emp.cTar_Tab_Moneda], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
      console.log(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Maestro/Listar/Roles', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call Ma_sp_Listar_Roles()';
  connection.query(sql, [
    emp.cTar_Tab_Rol,
    emp.cTar_Tab_Moneda,
    emp.nTar_Monto
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

/** Cuenta */
app.post('/Cuentas/Listar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cb_sp_listarCuentaBanco()';
  connection.query(sql, [], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Cuentas/Insertar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cb_sp_Insertar_Cuenta(?,?,?,?,?)';
  connection.query(sql, [
    emp.cCue_Nombre,
    emp.cCue_Numero,
    emp.cCue_Tab_Banco,
    emp.cCue_Tab_Moneda,
    emp.bCue_Estado,
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Cuentas/Actualizar', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cb_sp_Actualizar_Cuenta(?,?,?,?,?,?)';
  connection.query(sql, [
    emp.cCue_Nombre,
    emp.cCue_Numero,
    emp.cCue_Tab_Banco,
    emp.cCue_Tab_Moneda,
    emp.bCue_Estado,
    emp.nCue_Id,
  ], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});

app.post('/Cuentas/Listar/Bancos', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call cb_sp_listarBancos()';
  connection.query(sql, [], (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('Error');
    }
  });
});



app.post('/Correlativo/DB', cors(), (req, res) => {
  let emp = req.body;
  const sql = 'Call db_sp_Verificar_BaseDatos()';
  connection.query(sql,  (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.json(result[0]);
      console.log(result[0]);
    } else {
      res.send('Error');
    }
  });
});




app.listen(PORT, () => console.log(`Server on port : ${PORT}`));