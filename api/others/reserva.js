/* 1. Casos */
app.post('/Casos/Listar', cors(), (req, res) => {

    let emp = req.body;
    const sql = `Call ca_sp_Listar_Casos(?)`;

    connection.query(sql, [
        emp.nUsu_ID,
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/Listar/Desactivados', cors(), (req, res) => {

    let emp = req.body;
    const sql = `Call ca_sp_Listar_Casos_Desactivados(?)`;
    connection.query(sql, [
        emp.nUsu_ID,
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/Filtro', cors(), (req, res) => {

    let emp = req.body;
    const sql = `Call ca_sp_Filtro_Casos(?,?,?,?,?,?,?)`;
    connection.query(sql, [
        emp.nCli_Id,
        emp.cCas_Tab_Materia,
        emp.cCas_Tab_SubMateria,
        emp.bCas_Estado,
        emp.nUsu_ID,
        emp.FechaInicial,
        emp.FechaFin
    ], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {
            console.log(emp.FechaInicial)
            console.log(emp.FechaFin)
            res.json(result[0]);
        } else {
            res.send('success');
        }
    });
});

app.post('/Casos/Insertar', cors(), (req, res) => {

    let c = req.body;
    const sql = 'Call ca_sp_Insertar_Casos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    connection.query(sql, [

        c.nCli_Id,
        c.nEqu_ID,
        c.cCas_Tab_Tipo,
        c.cCas_Titulo,
        c.cCas_Cod_Externo,
        c.cCas_Cod_Interno,
        c.cCas_Tab_Materia,
        c.cCas_Tab_SubMateria,
        c.cCas_Detalle,
        c.cCas_Tab_Jurisdiccion,
        c.cCas_Expediente,
        c.nCas_Exp_Year,
        c.nJuz_Id,
        c.cCas_Tab_Sala,
        c.cCas_Tab_Recurso_Tipo,
        c.cCas_Recurso_Num,
        c.cCas_Tab_Distrito_Jud,
        c.cCas_Tab_Comision,
        c.cTab_Lugar_Tramite,
        c.nPer_Id,
        c.cCas_Referencia,
        c.cCas_Denominacion,
        c.dCas_Fec_Ini_Interno,
        c.dCas_Fec_Ini_Procesal,
        c.cCas_Tab_Estado_Externo,
        c.dCas_Fec_Estima_Cierre,
        c.cTab_Estado_Interno,
        c.dFas_Fec_Cierre,
        c.nUsu_ID,
        c.dUsu_Fecha,
        c.bCas_Estado,
        c.cUbi_Org,
        c.cTab_Tipo_Acto,
        c.cTab_Moneda,
        c.nCas_Monto,
        c.cTab_Tipo_Monto,
        c.cName_Resolucion,
        c.nCas_Origen,
        c.nCas_HonorarioFijo,
        c.nCas_HonorarioExito
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/Actualizar', cors(), (req, res) => {

    let c = req.body;
    const sql = 'Call ca_sp_Actualizar_Casos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    connection.query(sql, [
        c.nCas_Id,
        c.nCli_Id,
        c.nEqu_ID,
        c.cCas_Titulo,
        c.cCas_Cod_Externo,
        c.cCas_Cod_Interno,
        c.cCas_Tab_Materia,
        c.cCas_Tab_SubMateria,
        c.cCas_Detalle,
        c.cCas_Tab_Jurisdiccion,
        c.cCas_Expediente,
        c.nCas_Exp_Year,
        c.nJuz_Id,
        c.cCas_Tab_Sala,
        c.cCas_Tab_Recurso_Tipo,
        c.cCas_Recurso_Num,
        c.cCas_Tab_Distrito_Jud,
        c.cCas_Tab_Comision,
        c.cTab_Lugar_Tramite,
        c.nPer_Id,
        c.cCas_Referencia,
        c.cCas_Denominacion,
        c.dCas_Fec_Ini_Interno,
        c.dCas_Fec_Ini_Procesal,
        c.cCas_Tab_Estado_Externo,
        c.dCas_Fec_Estima_Cierre,
        c.cTab_Estado_Interno,
        c.dFas_Fec_Cierre,
        c.nUsu_ID,
        c.dUsu_Fecha,
        c.cUbi_Org,
        c.cTab_Tipo_Acto,
        c.cTab_Moneda,
        c.nCas_Monto,
        c.cTab_Tipo_Monto
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
                console.log(c.cUbi_Org)
            }
        });
});

app.post('/Casos/Eliminar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call ca_sp_Eliminar_Casos(?)';
    connection.query(sql, [
        emp.nCas_Id
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/IDCASO', cors(), (req, res) => {

    let emp = req.body;
    const sql = `Call ca_sp_FiltroIdCaso(?)`;
    connection.query(sql, [
        emp.cCas_Cod_Externo
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/Equipo', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call eq_sp_Listar_Usuarios(?,?)';
    connection.query(sql, [
        emp.nCas_Id,
        emp.Tipo
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                console.log(emp.nCas_Id)
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Casos/Masivo', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call ca_sp_cerrarMasivo_Casos(?)';
    connection.query(sql, [
        emp.nCas_Id
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

/* Fin Casos */

/* 2. Equipo Caso */
app.get('/EqCasos/Listar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call eqc_sp_Listar_EqCasos';
    connection.query(sql, [

    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/EqCasos/Insertar', cors(), (req, res) => {

    let emp = req.body;
    console.log("Cuerpo de eq casos");
    console.log(emp);

    const sql = 'Call eqc_sp_Insertar_EqCasos(?,?,?,?,?)';
    connection.query(sql, [
        emp.nCas_Id, emp.nUsu_Id, emp.cCas_Tab_TipoEquipo, emp.cCas_TabMoneda, emp.nCas_Monto
    ], (error, result) => {
        if (error) throw error;

        console.log(result);
        res.send(result);

    });
});

app.post('/EqCasos/Masivo/Insertar', cors(), (req, res) => {

    let emp = req.body;
    let arrayUsuarios = emp.nUsu_Id;

    console.log("xddddddddddddddddddddddddddd");
    console.log(arrayUsuarios);

    if (arrayUsuarios.length > 0) {
        for (let i = 0; i < arrayUsuarios.length; i++) {

            let data = [emp.nCas_Id, arrayUsuarios[i].value, emp.cCas_Tab_TipoEquipo, emp.cCas_TabMoneda, emp.nCas_Monto]

            const sql = 'Call eqc_sp_Insertar_EqCasos(?,?,?,?,?)';
            connection.query(sql, data, (error, result) => {
                if (error) throw error;
                if (result.length > 0) {
                    res.json(result[0]);
                } else {
                    res.send('success');
                }
            });
        }
    } else {
        res.status(400);
    }


});

app.post('/EqCasos/Eliminar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call eqc_sp_Eliminar_EqCasos(?)';
    connection.query(sql, [
        emp.nCas_Id
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                console.log("Id eliminar", emp.nCas_Id)
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/EqCasosTpoParticipantes/Eliminar', cors(), (req, res) => {

    let emp = req.body;

    const sql = 'Call eqc_sp_Eliminar_EqCasos_tpoPart(?,?)';
    connection.query(sql, [
        emp.nCas_Id, emp.cCas_Tab_TipoEquipo
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                console.log("Id eliminar", emp.nCas_Id)
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

/* Fin Equipo Casos */

/* 2. Usuario */

app.get('/Usuario/Listar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call usu_sp_Listar_Usuarios()'
    connection.query(sql, [

    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Usuario/Perfil', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call usu_sp_Perfil_Usuarios(?)'
    connection.query(sql, [
        emp.nUsu_ID
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Usuario/Acceso', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call ua_sp_Listar_UsuarioAcceso(?)'
    connection.query(sql, [
        emp.nUsu_ID
    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Usuario/Insertar', cors(), (req, res) => {

    var emp = req.body;

    const sql = `SELECT * FROM Usuarios where cUsu_Login=? and bUsu_Activo=1`;// aregar estado
    connection.query(sql, [emp.cUsu_Login], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
            res.json({
                status: 501  // colocar el codigo para informar de login existente
            });
        } else {


            const sql = 'Call usu_sp_Insertar_Usuarios(?,?,?,?,?,?,?)';
            connection.query(sql, [
                emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

            ]

                , (error, result) => {
                    if (error) throw error;

                    if (result.length > 0) {
                        res.json(result[0]);
                    } else {
                        res.send('success');
                    }
                });

        }
    });

});

app.post('/Usuario/Actualizar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call us_sp_Actualizar_Usuarios(?,?,?,?,?,?,?,?)';
    connection.query(sql, [
        emp.nUsu_ID, emp.nRol_ID, emp.cUsu_Nombres, emp.cUsu_email, emp.cUsu_Fono, emp.cUsu_Login, emp.cUsu_Pass, emp.bUsu_Activo

    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Usuario/Eliminar', cors(), (req, res) => {

    let emp = req.body;
    const sql = 'Call usu_sp_Eliminar_Usuarios(?)';
    connection.query(sql, [
        emp.nUsu_ID

    ]

        , (error, result) => {
            if (error) throw error;

            if (result.length > 0) {
                res.json(result[0]);
            } else {
                res.send('success');
            }
        });
});

app.post('/Usuario/Login', cors(), (req, res) => {

    var emp = req.body;
    const sql = 'Call usu_sp_Login_Usuarios(?,?)';
    connection.query(sql, [
        emp.cUsu_email, emp.cUsu_Pass
    ], (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
            var obj = JSON.parse(JSON.stringify(result[0]));

            if (obj[0].ID == null) {
                const sql = `select * 
                        from Usuarios 
                        where cUsu_email=? and cUsu_Pass=?`;
                connection.query(sql, [emp.cUsu_email, emp.cUsu_Pass], (error, result) => {
                    if (error) throw error;
                    if (result.length > 0) {
                        console.log(result[0]);
                        var resobj = JSON.parse(JSON.stringify(result[0]));
                        console.log(resobj);
                        let objRes = [
                            RowDataPacket = {
                                CONDICION: 1,
                                ID: resobj.nUsu_Id,
                                NombreC: resobj.cUsu_Nombres,
                                Correo: resobj.cUsu_email,
                            }
                        ]
                        console.log("1", objRes);
                        res.json(objRes);

                    } else {
                        console.log("2", result[0]);
                        res.send('success');
                    }
                });

            } else {

                let objRes = [
                    RowDataPacket = {
                        CONDICION: 1,
                        ID: result[0][0].ID,
                        NombreC: result[0][0].NombreC,
                        Correo: result[0][0].Correo,
                    }
                ]
                res.json(objRes);
            }
        } else {
            res.send('success');
        }
    });
});

app.post('/Admin/Login', cors(), (req, res) => {

    var emp = req.body;
    const sql = 'Call Adm_sp_Login_Admarios(?,?)';
    connection.query(sql, [
        emp.cUsu_email, emp.cUsu_Pass
    ], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {
            console.log(result[0]);

            var obj = JSON.parse(JSON.stringify(result[0]));
            console.log(obj[0].ID);

            if (obj[0].ID == null) {
                console.log("vacio");
                const sql = `select * 
                        from Usuarios 
                        where cUsu_email=? and cUsu_Pass=?`;
                connection.query(sql, [emp.cUsu_email, emp.cUsu_Pass], (error, result) => {
                    if (error) throw error;
                    if (result.length > 0) {
                        console.log(result[0]);
                        var resobj = JSON.parse(JSON.stringify(result[0]));
                        console.log(resobj);
                        let objRes = [
                            RowDataPacket = {
                                CONDICION: 1,
                                ID: resobj.nUsu_Id,
                                NombreC: resobj.cUsu_Nombres,
                                Correo: resobj.cUsu_email,
                            }
                        ]

                        console.log("encontrado por email");
                        console.log(objRes);
                        res.json(objRes);

                    } else {
                        console.log("no encontro usuario segundo nivel");
                        console.log(result[0]);
                        res.send('success');
                    }
                });

            } else {
                console.log("encontro el usuario[");
                res.json(result[0]);
            }
        } else {

        }
    });
});

app.post('/Validar/LoginEstado', cors(), (req, res) => {

    var emp = req.body;

    const sql = 'Call usu_sp_validarLoginEstado(?)';
    connection.query(sql, [
        emp._nUsu_Id
    ], (error, result) => {
        if (error) throw error;

        if (result.length > 0) {
            res.json(result[0]);
            console.log(result[0]);
        } else {
            res.send('success');
        }
    });

});
