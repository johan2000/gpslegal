const express = require("express");
const bodyParser = require('body-parser');
const fs = require('fs')
var cors = require('cors');
const app = express();
const corsOptions = {
  origin: '*',
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200
}
const mysql = require('mysql');
app.use(cors(corsOptions));
app.use(bodyParser.json());
const PORT = 9002;
app.listen(PORT, () => console.log(`Puerto : ${PORT}`));

var Tesseract = require('tesseract.js')
var Jimp = require("jimp");

const puppeteer = require('puppeteer');
const {
  fade
} = require("jimp");

var connection = mysql.createConnection({
  host: '35.199.109.14',
  user: 'kokixx19',
  password: 'kokixx19',
  multipleStatements: true,
});


app.post('/Vincular/CEJ/Cuadernos', cors(), (req, res) => {
  console.time('loop');
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {

    const browser = await puppeteer.launch({
      headless: false
    }); // run browser
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(500)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 10
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 10
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 10
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 10
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 10
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 10
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 10
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0xFFFFFFFF).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);


              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(2500)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]

                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  //Cantidad de cuadernos o procesos
                  let nCuadernos = (await page.$$('#divDetalles > div')).length;
                  let JsonDataCuadernos = []



                  for (let i = 1; i <= nCuadernos; i++) {
                    await page.waitForSelector('#divDetalles > div:nth-child(' + i + ')');
                    let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
                    let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
                    let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
                    let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());
                    var data = {
                      CodigoC: xCodigoC,
                      CodigoExterno: xCodigoExterno,
                      Detalle: xDetalle,
                      Juzgado: xJuzgado,
                    }
                    JsonDataCuadernos.push(data)
                  }
                  console.timeEnd('loop');
                  res.status(200).json({
                    DataCuadernos: JsonDataCuadernos,
                    result: true
                  });
                  await browser.close();
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                }

              } catch (error) {
                res.status(500).json({
                  result: false,
                  mensaje: "Ups Error"
                });
                console.log(error)
                await browser.close();
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});

app.post('/Vincular/CEJ', cors(), (req, res) => {

  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");
  console.time(CodigoExterno);
  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {
    // declare function
    const browser = await puppeteer.launch({
      headless: true
    }); // run browser
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(500)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 10
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 10
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 10
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 10
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 10
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 10
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 10
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0xFFFFFFFF).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);

              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(2000)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]
                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  //Cantidad de cuadernos o procesos
                  let nCuadernos = (await page.$$('#divDetalles > div')).length;
                  let JsonDataCuadernos = []
                  let JsonDataCuadernosInformacion = []
                  let JsonDataMovimientos = []
                  let JsonDataNotificacion = []



                  for (let i = 1; i <= nCuadernos; i++) {
                    await page.waitForTimeout(2000)
                    await page.waitForSelector('#divDetalles > div:nth-child(' + i + ')');
                    let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
                    let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
                    let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
                    let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());

                    if (xCodigoExterno == CodigoExterno) {

                      var data = {
                        CodigoC: xCodigoC,
                        CodigoExterno: xCodigoExterno,
                        Detalle: xDetalle,
                        Juzgado: xJuzgado,
                      }
                      JsonDataCuadernos.push(data)

                      await page.waitForTimeout(2000)
                      await page.click('#divDetalles > div:nth-child(' + i + ') >  .celdCentro > form > button');

                      await page.waitForSelector('#collapseThree')
                      let CodigoExterno = await page.$eval('#gridRE > div:nth-child(1) > div.celdaGrid.celdaGridXe > b', e => e.innerText.trim());
                      let orgJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                      let disJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(4)', e => e.innerText.trim());
                      let juez = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                      let espLegal = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(4)', e => e.innerText.trim());
                      let fInicio = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(2)', e => e.innerText.trim());
                      let proceso = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(4)', e => e.innerText.trim());
                      let obs = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(2)', e => e.innerText.trim());
                      let esp = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(4)', e => e.innerText.trim());
                      let materia = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(2)', e => e.innerText.trim());
                      let estado = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(4)', e => e.innerText.trim());
                      let eProcesal = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(2)', e => e.innerText.trim());
                      let fConclusion = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(4)', e => e.innerText.trim());
                      let ubicacion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(2)', e => e.innerText.trim());
                      let mConclusion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(4)', e => e.innerText.trim());
                      let sumilla = await page.$eval('#gridRE > div:nth-child(9) > div:nth-child(2)', e => e.innerText.trim());
                      let xTipo = await page.$eval('#collapseTwo > div > div:nth-child(1) > div:nth-child(1)', e => e.innerText.trim());
                      let nParticipantes = (await page.$$('#collapseTwo > div > div')).length;
                      let strPart = "";
                      for (let i = 2; i <= nParticipantes; i++) {
                        let xPart = await page.$eval('#collapseTwo > div > div:nth-child(' + i + ')', e => e.innerText.trim());
                        if (nParticipantes == i) {
                          strPart += xPart
                        } else {
                          strPart += "(" + xTipo + ")" + xPart + "|"
                        }
                      }
                      var sPart = strPart.split("\n").join(" ");

                      var dataCabecera = {
                        CodigoC: xCodigoC,
                        CodigoExterno: CodigoExterno,
                        General: [{
                          OrganoJurisdiccional: orgJuris,
                          DistritoJudicial: disJuris,
                          Juez: juez,
                          EspecialistaLegal: espLegal,
                          FechadeInicio: fInicio,
                          Proceso: proceso,
                          Observacion: obs,
                          Especialidad: esp,
                          Materia: materia,
                          Estado: estado,
                          EtapaProcesal: eProcesal,
                          FechaConclusión: fConclusion,
                          Ubicación: ubicacion,
                          MotivoConclusión: mConclusion,
                          Sumilla: sumilla,
                          Participantes: sPart,
                        }]
                      }
                      JsonDataCuadernosInformacion.push(dataCabecera)
                      await page.waitForTimeout(5000)
                      let xMovimientos = (await page.$$('#collapseThree > div')).length - 1;
                      console.log(xMovimientos)

                      for (let i = 0; i < xMovimientos; i++) {
                        let CodigoMoviento = xMovimientos - i;
                        let xFechaResolucion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xResolucion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                        let xTNotificacion = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                        let xActo = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xFojas = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                        let xProveido = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                        let xSumilla = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xDescripcionUsuario = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                        let dataMovimiento = {
                          Moviento: CodigoMoviento,
                          CodigoC: xCodigoC,
                          CodigoExterno: CodigoExterno,
                          FechaResolucion: xFechaResolucion,
                          Resolucion: xResolucion,
                          TNotificacion: xTNotificacion,
                          Acto: xActo,
                          Fojas: xFojas,
                          Proveido: xProveido,
                          Sumilla: xSumilla,
                          ComentarioUsuario: xDescripcionUsuario,
                          cFechaDescarga: "Descargado el " + cFecha,
                          Notificacion: []
                        }
                        JsonDataMovimientos.push(dataMovimiento)
                        var xnNotificacion = (await page.$$('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div > div')).length;
                        for (let j = 0; j < xnNotificacion; j++) {
                          let CodigoNotificacion = xnNotificacion - j;
                          let CodigoMoviento = i;
                          let titulo = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div >h5', e => e.innerText.trim());
                          let xDestinatario = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                          let xFecha = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                          let xAnexos = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                          let xFormaEntrega = await page.$eval('#pnlSeguimiento' + (i + 1) + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + (j + 1) + ') > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

                          let dataNotificacion = {
                            CodigoNotificacion: CodigoNotificacion,
                            CodigoMoviento: CodigoMoviento,
                            CodigoExterno: xCodigoExterno,
                            titulo: titulo,
                            destinatario: xDestinatario,
                            fechaEnvio: xFecha,
                            anexos: xAnexos,
                            formaEntrega: xFormaEntrega,
                          }
                          JsonDataNotificacion.push(dataNotificacion)
                        }
                      }
                      await page.waitForTimeout(3000)
                      await page.click('#divCuerpo > div:nth-child(1) > a:nth-child(1) > img');
                    }
                  }
                  await browser.close()
                  console.timeEnd(CodigoExterno);
                  res.status(200).json({
                    DataCuadernos: JsonDataCuadernos,
                    DataGneral: JsonDataCuadernosInformacion,
                    DataMovimienntos: JsonDataMovimientos,
                    DataNotificacion: JsonDataNotificacion,
                    result: true,
                    mensaje: "Scraping realizado"
                  });
                  fs.unlinkSync(file)
                }

              } catch (error) {
                await browser.close()
                console.timeEnd(CodigoExterno);
                console.log(error);
                res.status(500).json({
                  result: false,
                  mensaje: "Ups Error"
                });
                fs.unlinkSync(file)
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});


app.post('/Scraping/Insertar', cors(), (req, res) => {

  let e = req.body;
  let data = [

    e.nCas_Id,
    JSON.stringify(e.cCav_Cuadernos).replace(/\//g, '').toString(),
    JSON.stringify(e.cCav_General).replace(/\//g, '').toString(),
    JSON.stringify(e.cCav_Movimientos).replace(/\//g, '').toString(),
    e.cCav_Archivo,
    e.cCav_Notas,

  ];

  const sql = 'Call usp_Casos_Movs_Insert(?,?,?,?,?,?)';
  connection.query(sql, data, (error, result) => {
    if (error) throw error;

    if (result.length > 0) {
      res.json(result[0]);
    } else {
      res.send('success');
    }
  });
});


app.post('/Vincular/CEJ/MasivoUpdate', cors(), (req, res) => {
  const {
    CodigoExterno
  } = req.body;
  console.log(CodigoExterno)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {
    // declare function
    const browser = await puppeteer.launch(({
      headless: false
    })); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(800)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 30
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 30
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 30
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 30
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 30
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 30
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 30
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0xFFFFFFFF).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);


              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(800)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                console.log("-------------------------------------------")
                if (Pasante == '') {
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                  Rompedor();

                } else {
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  await page.click('#divDetalles > div:nth-child(1) >  .celdCentro > form > button');
                  await page.waitForSelector("#gridRE")
                  let Asunto = await page.$eval('#gridRE > div:nth-child(9) > div.celdaGridxT', e => e.innerText.trim());

                  res.status(200).json({
                    Asunto: Asunto,
                    result: true
                  });
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                  // await browser.close()
                }

              } catch (error) {
                console.log(error)
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});



/*connection.query("CALL usp_Casos_Movs_Insert(?,?,?,?,?,?)", [
  Id,
  JSON.stringify(JsonDataCuadernos).replace(/\//g, ''),
  JSON.stringify(JsonDataCuadernosInformacion).replace(/\//g, ''),
  JSON.stringify(JsonDataMovimientos).replace(/\//g, ''),
  '',
  ''
], (error, result) => {
 
  if (error) throw error;
  if (result) {
    browser.close()
  } else if (result == undefined) {
  } else {
    console.log('error desconoceido: ', error)
  }
})*/

app.post('/Vincular/CorteSuprema', cors(), (req, res) => {

  const {
    CodSalaSuprema,
    CodRecurso,
    CodNRecurso,
    CodAnoRecurso,
    CodNExpediente,
    CodAnoxpediente,
    CodDistritoOrigen,
    CodApePaterno,
    CodApeMaterno,
    CodNombre,
  } = req.body;

  console.time("Test");
  (async () => {
    // declare function
    const browser = await puppeteer.launch(({
      headless: false
    })); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });

    await page.goto('https://apps.pj.gob.pe/cejSupremo/ConsultaExpediente.aspx');
    await page.select('[id=ddlSala]', CodSalaSuprema);
    await page.waitForTimeout(200)
    await page.select('[id=ddlMotIng]', CodRecurso);
    await page.waitForTimeout(200)
    await page.select('[id=ddlAnio]', CodAnoRecurso);
    await page.waitForTimeout(200)
    await page.select('[id=ddlAnio0]', CodAnoxpediente);
    await page.waitForTimeout(200)
    await page.select('[id=ddlDisOri]', CodDistritoOrigen);
    await page.waitForTimeout(200)

    await page.type('[id=txtNum]', CodNRecurso, {
      delay: 100
    });
    await page.type('[id=txtNum0]', CodNExpediente, {
      delay: 100
    });
    await page.type('[id=txtPat]', CodApePaterno, {
      delay: 100
    });
    await page.type('[id=txtMat]', CodApeMaterno, {
      delay: 100
    });
    await page.type('[id=txtNom]', CodNombre, {
      delay: 100
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#c_consultaexpediente_captcha1_CaptchaImage'); // declare a variable with an ElementHandle
        const nameImage = await element.screenshot({
          type: 'png',
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";

        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.color([{
            apply: 'desaturate',
            params: [150]
          }]).contrast(.999).posterize(500).opacity(.9999).write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);
              var ResultadoSplit1 = text.replace(/ /gi, "");
              var ResultadoSplit2 = ResultadoSplit1.replace(/I/gi, "");
              console.log("-------------------------------------------")
              await page.type('[id=TextBox1]', ResultadoSplit2.split(" "), {
                delay: 0
              });
              await page.click('#btnBuscar');
              await page.waitForTimeout(1200)
              let lblEstado = await page.$eval('#lblEstado', e => e.innerText.trim());
              await page.waitForTimeout(1200)
              try {
                if (lblEstado == 'Código CAPTCHA incorrecto') {
                  var Resultado = ResultadoSplit2.toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                  Rompedor();
                } else {
                  console.timeEnd("Test");
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)
                  await page.waitForTimeout(2000)
                  await page.click('#btnSeguimiento');
                  res.status(200).json({
                    data: Resultado,
                    result: true
                  });
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                }

              } catch (error) {

              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});

const config = {
  lang: "eng",
  oem: 1,
  psm: 3,
}


app.post('/Vincular/CEJ/Update', cors(), (req, res) => {
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {
    // declare function
    const browser = await puppeteer.launch(({
      headless: true
    })); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(480000);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(800)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 30
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 30
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 30
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 30
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 30
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 30
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 30
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0xFFFFFFFF).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);


              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(800)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]
                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  //scraping

                  //Cantidad de cuadernos o procesos
                  let nCuadernos = (await page.$$('#divDetalles > div')).length;
                  //    console.log(nCuadernos)
                  let JsonDataCuadernos = []
                  let JsonDataCuadernosInformacion = []
                  let JsonDataMovimientos = []
                  let JsonDataNotificacion = []

                  for (let i = 1; i <= nCuadernos; i++) {
                    await page.waitForTimeout(2000)
                    await page.waitForSelector('#divDetalles')
                    let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
                    let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());

                    if (xCodigoExterno == expediente) {
                      let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
                      let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());
                      var data = {
                        CodigoC: xCodigoC,
                        CodigoExterno: xCodigoExterno,
                        Detalle: xDetalle,
                        Juzgado: xJuzgado,
                      }
                      JsonDataCuadernos.push(data)




                      await page.waitForTimeout(800)
                      await page.click('#divDetalles > div:nth-child(' + i + ') >  .celdCentro > form > button');

                      /* Scriping  a informacion de cada cuarderno */
                      await page.waitForSelector('#collapseThree')
                      let CodigoExterno = await page.$eval('#gridRE > div:nth-child(1) > div.celdaGrid.celdaGridXe > b', e => e.innerText.trim());
                      let orgJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                      let disJuris = await page.$eval('#gridRE > div:nth-child(2) > div:nth-child(4)', e => e.innerText.trim());
                      let juez = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                      let espLegal = await page.$eval('#gridRE > div:nth-child(3) > div:nth-child(4)', e => e.innerText.trim());
                      let fInicio = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(2)', e => e.innerText.trim());
                      let proceso = await page.$eval('#gridRE > div:nth-child(4) > div:nth-child(4)', e => e.innerText.trim());
                      let obs = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(2)', e => e.innerText.trim());
                      let esp = await page.$eval('#gridRE > div:nth-child(5) > div:nth-child(4)', e => e.innerText.trim());
                      let materia = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(2)', e => e.innerText.trim());
                      let estado = await page.$eval('#gridRE > div:nth-child(6) > div:nth-child(4)', e => e.innerText.trim());
                      let eProcesal = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(2)', e => e.innerText.trim());
                      let fConclusion = await page.$eval('#gridRE > div:nth-child(7) > div:nth-child(4)', e => e.innerText.trim());
                      let ubicacion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(2)', e => e.innerText.trim());
                      let mConclusion = await page.$eval('#gridRE > div:nth-child(8) > div:nth-child(4)', e => e.innerText.trim());
                      let sumilla = await page.$eval('#gridRE > div:nth-child(9) > div:nth-child(2)', e => e.innerText.trim());
                      let xTipo = await page.$eval('#collapseTwo > div > div:nth-child(1) > div:nth-child(1)', e => e.innerText.trim());
                      let nParticipantes = (await page.$$('#collapseTwo > div > div')).length;
                      let strPart = "";
                      for (let i = 2; i <= nParticipantes; i++) {
                        let xPart = await page.$eval('#collapseTwo > div > div:nth-child(' + i + ')', e => e.innerText.trim());
                        if (nParticipantes == i) {
                          strPart += xPart
                        } else {
                          strPart += "(" + xTipo + ")" + xPart + "|"
                        }
                      }
                      var sPart = strPart.split("\n").join(" ");

                      var dataCabecera = {
                        CodigoC: xCodigoC,
                        CodigoExterno: CodigoExterno,
                        General: [{
                          OrganoJurisdiccional: orgJuris,
                          DistritoJudicial: disJuris,
                          Juez: juez,
                          EspecialistaLegal: espLegal,
                          FechadeInicio: fInicio,
                          Proceso: proceso,
                          Observacion: obs,
                          Especialidad: esp,
                          Materia: materia,
                          Estado: estado,
                          EtapaProcesal: eProcesal,
                          FechaConclusión: fConclusion,
                          Ubicación: ubicacion,
                          MotivoConclusión: mConclusion,
                          Sumilla: sumilla,
                          Participantes: sPart,
                        }]
                      }
                      JsonDataCuadernosInformacion.push(dataCabecera)

                      /* Scraping Movimientos */
                      await page.waitForTimeout(3000)
                      let xMovimientos = (await page.$$('#collapseThree > div')).length;
                      console.log("Cantidad x Movs ", CodigoExterno, xMovimientos)

                      for (let i = 1; i < xMovimientos; i++) {
                        let xFechaResolucion = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xResolucion = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                        let xTNotificacion = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                        let xActo = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xFojas = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                        let xProveido = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(3) > div:nth-child(2)', e => e.innerText.trim());
                        let xSumilla = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                        let xDescripcionUsuario = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

                        let dataMovimiento = {
                          CodigoC: xCodigoC,
                          IdMovimiento: i,
                          CodigoExterno: CodigoExterno,
                          FechaResolucion: xFechaResolucion,
                          Resolucion: xResolucion,
                          TNotificacion: xTNotificacion,
                          Acto: xActo,
                          Fojas: xFojas,
                          Proveido: xProveido,
                          Sumilla: xSumilla,
                          ComentarioUsuario: xDescripcionUsuario,
                          cFechaDescarga: "Descargado el " + cFecha,
                          Notificacion: []
                        }

                        var titulo;
                        var xDestinatario;
                        var xAnexos;
                        var xFormaEntrega;

                        let xnNotificacion = (await page.$$('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div > div')).length;

                        for (let j = 1; j < xnNotificacion; j++) {

                          titulo = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > h5', e => e.innerText.trim());
                          xDestinatario = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                          xFecha = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());
                          xAnexos = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2)', e => e.innerText.trim());
                          xFormaEntrega = await page.$eval('#pnlSeguimiento' + i + ' > div:nth-child(2) > div > div:nth-child(2)> div:nth-child(' + j + ') > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)', e => e.innerText.trim());

                          let dataNotificacion = {
                            IdMovimiento: i,
                            IdNotificacion: j,
                            CodigoExterno: CodigoExterno,
                            titulo: titulo,
                            destinatario: xDestinatario,
                            fechaEnvio: xFecha,
                            anexos: xAnexos,
                            formaEntrega: xFormaEntrega,
                          }
                          JsonDataNotificacion.push(dataNotificacion)
                        }
                        JsonDataNotificacion.push(dataMovimiento)
                      }

                    }

                    /* -------------------------------------------------------- */
                    await page.waitForTimeout(1500)
                  }

                  console.log("Cantidad Maxima de movimientos ", JsonDataMovimientos.length)

                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: JsonDataCuadernos,
                    DataGneral: JsonDataCuadernosInformacion,
                    DataMovimienntos: JsonDataMovimientos,
                    DataNotificacion: JsonDataNotificacion,
                    result: true,
                    mensaje: "Scraping realizado"
                  });
                  fs.unlinkSync(file)
                }

              } catch (error) {
                await browser.close()
                res.status(200).json({
                  DataCuadernos: [],
                  DataGneral: [],
                  DataMovimienntos: [],
                  result: false,
                  mensaje: error
                });
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});


app.post('/Vincular/CEJ/ARQ', cors(), (req, res) => {
  console.time('loop');
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  let cod_expediente = xcej[0];
  let cod_anio = xcej[1];
  let cod_incidente = xcej[2];
  let cod_distprov = xcej[3];
  let cod_organo = xcej[4];
  let cod_especialidad = xcej[5];
  let cod_instancia = xcej[6];

  (async () => {

    const browser = await puppeteer.launch({
      executablePath: "/usr/bin/chromium-browser",
      args: ['--no-sandbox']
    }); // run browser
    const page = await browser.newPage();
    await page.setDefaultTimeout(999999);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');
    await page.waitForSelector('#captcha_image');
    await page.click('#myTab > li:nth-child(2)')
    await page.waitForSelector('#divConsultar')

    await page.waitForTimeout(500)
    await page.type('[id=cod_expediente]', cod_expediente, {
      delay: 10
    });
    await page.type('[id=cod_anio]', cod_anio, {
      delay: 10
    });
    await page.type('[id=cod_incidente]', cod_incidente, {
      delay: 10
    });
    await page.type('[id=cod_distprov]', cod_distprov, {
      delay: 10
    });
    await page.type('[id=cod_organo]', cod_organo, {
      delay: 10
    });
    await page.type('[id=cod_especialidad]', cod_especialidad, {
      delay: 10
    });
    await page.type('[id=cod_instancia]', cod_instancia, {
      delay: 10
    });

    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 553,
            'y': 618,
            'width': 120,
            'height': 46.14
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0xFFFFFFFF).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);


              await page.type('[id=codigoCaptcha]', text.split(" "), {
                delay: 0
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(2500)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]

                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(" ").toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  //Cantidad de cuadernos o procesos
                  let nCuadernos = (await page.$$('#divDetalles > div')).length;
                  let JsonDataCuadernos = []

                  fs.mkdirSync('./Documentos/' + CodigoExterno + '/', {
                    recursive: true
                  });

                  for (let i = 1; i <= nCuadernos; i++) {
                    await page.waitForSelector('#divDetalles > div:nth-child(' + i + ')');
                    let xCodigoC = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentro.textResalt', e => e.innerText.trim());
                    let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
                    let xDetalle = await page.$eval('#divDetalles > div:nth-child(' + i + ') > div.celdCentroD > div.partesp', e => e.innerText.trim());
                    let xJuzgado = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(2) > b', e => e.innerText.trim());
                    var data = {
                      CodigoC: xCodigoC,
                      CodigoExterno: xCodigoExterno,
                      Detalle: xDetalle,
                      Juzgado: xJuzgado,
                    }
                    JsonDataCuadernos.push(data)

                    await page.waitForTimeout(2000)
                    await page.click('#divDetalles > div:nth-child(' + i + ') >  .celdCentro > form > button');

                    await page.waitForTimeout(5000)
                    let xMovimientos = (await page.$$('#collapseThree > div')).length - 1;
                    console.log(xMovimientos)
                    for (let i = 0; i < xMovimientos; i++) {
                      try {
                        await page.waitForTimeout(200)

                        let xhref = await page.$eval('#pnlSeguimiento' + (i + 1) + '> div:nth-child(2) > div > div > div:nth-child(4)> div > a', e => e.href.trim());
                        console.log(xhref)
                        console.log(__dirname)



                        //await page.goto(xhref);

                      } catch (error) {

                      }
                    }

                    await page.waitForTimeout(3000)
                    await page.click('#divCuerpo > div:nth-child(1) > a:nth-child(1) > img');

                  }

                  await page.waitForTimeout(1000)
                  console.timeEnd('loop');
                  res.status(200).json({
                    DataCuadernos: JsonDataCuadernos,
                    result: true
                  });
                  await browser.close()
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                }

              } catch (error) {
                res.status(500).json({
                  result: false,
                  mensaje: error
                });
                console.log(error)

                await browser.close();
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});


app.post('/Vincular/Indecopi', cors(), (req, res) => {
  let e = req.body;

  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
      ],
      headless: false
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(30000);
    await page.setViewport({
      width: 1366,
      height: 768
    });


    switch (e.comision) {
      case 1:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCPC.jsp?pIdAreaMenu=8');

            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });
            await page.select('[name=lstArea]', xCod[2]);
            await page.select('[name=lstTipoExpediente]', xCod[3]);
            await page.select('[name=lstLugarTramite]', xCod[4]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(5000)
              await page.waitForSelector('.resultados-item-res');
              let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
              let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

              let rpt = [];
              let datos = [];

              for (let i = 2; i <= ntrInfoCon; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                let str = value.split("\t")
                datos.push({
                  datos: str[1]
                });
              }

              for (let i = 2; i <= ntr; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                rpt.push(value);
              }

              let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.resultados-der', e => e.innerText.trim());

              console.log(rpt);
              //buscar el indice donde aparece el texto Denunciantes
              initDn = rpt.indexOf("Denunciantes");
              //buscar el indice donde aparece el texto Denunciados
              initDs = rpt.indexOf("Denunciados");

              var initAll = rpt.length;

              let denunciados = [];
              let denunciantes = [];

              let NExpediente = [{
                NExpediente: NExpedienteS,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                SAC: SAC,
                LugarT: LugarT,
                LugarP: LugarP,
              }];


              for (let j = initDn + 1; j < initDs; j++) {
                //denunciados["denunciado-" + j] = (rpt[j]);
                denunciados.push({
                  denunciado: rpt[j]
                })
              }
              console.log("denunciados : ", denunciados);
              for (let k = initDs + 1; k < initAll; k++) {
                //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
                denunciantes.push({
                  denunciante: rpt[k]
                })
              }
              console.log("denunciantes: ", denunciantes);

              let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
              //arr de resultados de notificacion
              let Notications = [];

              for (let i = 3; i <= nNotificacion; i++) {
                let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                try {
                  Notications.push({
                    fecha: fechaData,
                    detalle: actividad
                  });
                } catch (error) {
                  console.log(error);
                }
              }


              res.status(200).json({
                NExpediente: NExpediente,
                datos: datos,
                denunciantes: denunciantes,
                denunciados: denunciados,
                Notications: Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }

            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=8&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=8&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });

          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }


        }
        break;
      case 2:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaSumario.jsp?pIdAreaMenu=119');
            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });
            await page.select('[name=lstArea]', xCod[2]);
            await page.waitForTimeout(100)
            await page.select('[name=lstTipoExpediente]', xCod[3]);
            await page.waitForTimeout(100)
            await page.select('[name=lstLugarTramite]', xCod[4]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(1000)
              await page.waitForSelector('.resultados-item-res');

              let ntr = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody', e => e.rows.length);
              let ntrInfoCon = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody', e => e.rows.length);

              let rpt = [];
              let datos = [];

              for (let i = 2; i <= ntrInfoCon; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                let str = value.split("\t")
                datos.push({
                  datos: str[1]
                });
              }

              for (let i = 2; i <= ntr; i++) {
                let value = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(' + i + ')', e => e.innerText.trim());
                rpt.push(value);
              }

              let NExpedienteS = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr.resultados-item > td:nth-child(2) > b', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let SAC = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

              let NExpediente = [{
                NExpediente: NExpedienteS,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                SAC: SAC,
                LugarT: LugarT,
                LugarP: LugarP,
              }];

              console.log(rpt);
              //buscar el indice donde aparece el texto Denunciantes
              initDn = rpt.indexOf("Denunciantes");
              //buscar el indice donde aparece el texto Denunciados
              initDs = rpt.indexOf("Denunciados");

              var initAll = rpt.length;

              let denunciados = [];
              let denunciantes = [];




              for (let j = initDn + 1; j < initDs; j++) {
                //denunciados["denunciado-" + j] = (rpt[j]);
                denunciados.push({
                  denunciado: rpt[j]
                })
              }
              console.log("denunciados : ", denunciados);
              for (let k = initDs + 1; k < initAll; k++) {
                //denunciantes["denunciante-" + (k - initDs)] = (rpt[k]);
                denunciantes.push({
                  denunciante: rpt[k]
                })
              }
              console.log("denunciantes: ", denunciantes);

              let nNotificacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody', e => e.rows.length);
              //arr de resultados de notificacion
              let Notications = [];

              for (let i = 3; i <= nNotificacion; i++) {
                let fechaData = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let actividad = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                try {
                  Notications.push({
                    fecha: fechaData,
                    detalle: actividad
                  });
                } catch (error) {
                  console.log(error);
                }
              }
              res.status(200).json({
                NExpediente,
                datos: datos,
                denunciantes: denunciantes,
                denunciados: denunciados,
                Notications: Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoPS.jsp?pIdAreaMenu=119&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoPS.jsp?pIdAreaMenu=119&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });
          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }
        }
        break;
      case 3:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCCD.jsp?pIdAreaMenu=5');
            let xCod = e.cod.split("-")
            console.log(xCod)

            await page.type('[name=txtNroExpediente]', xCod[0], {
              delay: 0
            });
            await page.type('[name=txtAnioExpediente]', xCod[1], {
              delay: 0
            });
            await page.select('[name=lstLugarTramite]', xCod[2]);
            await page.click('[name=btnBuscar]');

            try {
              await page.waitForTimeout(800)
              let nExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td', e => e.innerText.trim());
              let tExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.resultados-der', e => e.innerText.trim());
              let Fpresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let Tpresentación = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let LugarT = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());
              let LugarP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.resultados-der', e => e.innerText.trim());
              let Acumulado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.resultados-der', e => e.innerText.trim());
              let Asistente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.resultados-der', e => e.innerText.trim());

              let FormaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td.resultados-der', e => e.innerText.trim());
              let FechaConclucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td.resultados-der', e => e.innerText.trim());
              let nResolucion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td.resultados-der', e => e.innerText.trim());

              let NExpediente = [{
                NExpediente: nExpediente,
                TipoExpediente: tExpediente,
                FechaPresentacion: Fpresentacion,
                Tpresentación: Tpresentación,
                LugarT: LugarT,
                LugarP: LugarP,
                Acumulado: Acumulado,
                Asistente: Asistente,
                FormaConclucion: FormaConclucion,
                FechaConclucion: FechaConclucion,
                nResolucion: nResolucion,
              }];

              let nPersonasNJ = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr')).length;

              var Datos = []
              for (let i = 2; i <= nPersonasNJ; i++) {
                let Nombre = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let DocumentoI = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                let DomicilioP = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3)', e => e.innerText.trim());
                let PDProvincia = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(4)', e => e.innerText.trim());
                Datos.push({
                  Nombre: Nombre,
                  DocumentoI: DocumentoI,
                  DomicilioP: DomicilioP,
                  PDProvincia: PDProvincia,
                });
              }

              var ArrayDenunciantes = Datos.findIndex(obj => obj.Nombre == "Denunciantes");
              var ArrayDenunciados = Datos.findIndex(obj => obj.Nombre == "Denunciados");
              var denunciados = []
              var denunciantes = []
              for (let j = ArrayDenunciantes + 1; j < ArrayDenunciados; j++) {
                denunciados.push(Datos[j])
              }
              for (let j = ArrayDenunciados + 1; j < Datos.length; j++) {
                denunciantes.push(Datos[j])
              }
              let Notications = [];
              let nNotificaciones = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr')).length;
              for (let i = 2; i < nNotificaciones; i++) {
                let fecha = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1)', e => e.innerText.trim());
                let detalle = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2)', e => e.innerText.trim());
                Notications.push({
                  fecha: fecha,
                  detalle: detalle
                });
              }


              res.status(200).json({
                NExpediente: NExpediente,
                denunciantes,
                denunciados,
                Notications,
                result: true
              });

            } catch (error) {
              res.status(200).json({
                mensaje: "No hay resultados coincidentes con los criterios de busqueda",
                result: false
              });
            }
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=5&pIdTipoPersonaMenu=10');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=5&pIdTipoPersonaMenu=9');
            switch (e.opcion2) {
              case 1:
                await page.type('[name=txtRazonSocial]', e.cod, {
                  delay: 0
                });
                break;

              case 2:
                let xCod = e.cod.split("-")
                await page.type('[name=txtApellidos]', xCod[0], {
                  delay: 0
                });
                await page.type('[name=txtNombres]', xCod[1], {
                  delay: 0
                });
            }
        }
        if (e.opcion2 > 0) {
          await page.click('[name=btnBuscar]');
          try {
            await page.waitForTimeout(800)
            let nExpediente = (await page.$$('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr')).length;

            let JsonDataExpediente = []
            for (let i = 3; i <= nExpediente; i++) {
              let xExpediente = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.innerText.trim());
              let xhref = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a', e => e.href.trim());
              let xDenunciado = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(2) > a', e => e.innerText.trim());
              let xFechaPresentacion = await page.$eval('#form1 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.centro-medio > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > table > tbody > tr:nth-child(' + i + ') > td:nth-child(3) > a', e => e.innerText.trim());
              var data = {
                Expediente: xExpediente,
                href: xhref,
                Denunciado: xDenunciado,
                FechaPresentacion: xFechaPresentacion,
              }
              JsonDataExpediente.push(data)
            }
            res.status(200).json({
              DataExpediente: JsonDataExpediente,
              result: true
            });
          } catch (error) {
            res.status(200).json({
              mensaje: "No hay resultados coincidentes con los criterios de busqueda",
              result: false
            });
          }



        }
        break;
      case 4:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCEB.jsp?pIdAreaMenu=4');
            await page.waitForTimeout(800)


            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=10');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCEB.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 5:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaCPC.jsp?pIdAreaMenu=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=14');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=6&pIdTipoPersonaMenu=10');
            break;
          case 4:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/denunciadoCPC.jsp?pIdAreaMenu=4&pIdTipoPersonaMenu=9');
        }
        break;
      case 6:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=6');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=6&pId2=2');
            break;
        }
      case 7:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=7');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=7&pId2=2');
            break;
        }
      case 8:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consulta.jsp?pId=3');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=3&pId2=2');
            break;
        }
      case 9:
        switch (e.opcion) {
          case 1:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Expedientes/consultaSPC.jsp?pId=8');
            break;
          case 2:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=1');
            break;
          case 3:
            await page.goto('http://servicio.indecopi.gob.pe/portalSAE/Personas/persona.jsp?pId=8&pId2=2');
            break;
        }
    }


    //await browser.close();

  })();
});


var expresionRegular = /\s*;\s*/;




app.post('/Vincular/CEJ/Html', cors(), (req, res) => {
  console.time('loop');
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  var cod_expediente = "";
  var cod_anio = "";
  var cod_incidente = "";
  var cod_distprov = "";
  var cod_organo = "";
  var cod_especialidad = "";
  var cod_instancia = "";

  var cod_distritof = "";
  var cod_instanciaf = "";
  var cod_especialidadf = "";
  var cod_aniof = "";
  var cod_expedientef = "";


  if (xcej.length == 7) {
    cod_expediente = xcej[0];
    cod_anio = xcej[1];
    cod_incidente = xcej[2];
    cod_distprov = xcej[3];
    cod_organo = xcej[4];
    cod_especialidad = xcej[5];
    cod_instancia = xcej[6];
  } else {
    cod_distritof = xcej[0];
    cod_instanciaf = xcej[1];
    cod_especialidadf = xcej[2];
    cod_aniof = xcej[3];
    cod_expedientef = xcej[4];
  }



  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
      ],
      headless: false,
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(95000);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');

    if (xcej.length == 7) {
      await page.waitForSelector('#captcha_image');
      await page.click('#myTab > li:nth-child(2)')
      await page.waitForSelector('#divConsultar')
      await page.waitForTimeout(500)

      await page.type('[id=cod_expediente]', cod_expediente, {
        delay: 10
      });
      await page.type('[id=cod_anio]', cod_anio, {
        delay: 10
      });
      await page.type('[id=cod_incidente]', cod_incidente, {
        delay: 10
      });
      await page.type('[id=cod_distprov]', cod_distprov, {
        delay: 10
      });
      await page.type('[id=cod_organo]', cod_organo, {
        delay: 10
      });
      await page.type('[id=cod_especialidad]', cod_especialidad, {
        delay: 10
      });
      await page.type('[id=cod_instancia]', cod_instancia, {
        delay: 10
      });
    } else {

      const option1 = (await page.$x(
        '//*[@id = "distritoJudicial"]/option[text() = "' + cod_distritof + '"]'
      ))[0];
      const value1 = await (await option1.getProperty('value')).jsonValue();
      await page.select('#distritoJudicial', value1);
      await page.waitForTimeout(500)

      const option2 = (await page.$x(
        '//*[@id = "organoJurisdiccional"]/option[text() = "' + cod_instanciaf + '"]'
      ))[0];
      const value2 = await (await option2.getProperty('value')).jsonValue();
      await page.select('#organoJurisdiccional', value2);
      await page.waitForTimeout(500)

      const option3 = (await page.$x(
        '//*[@id = "especialidad"]/option[text() = "' + cod_especialidadf + '"]'
      ))[0];
      const value3 = await (await option3.getProperty('value')).jsonValue();
      await page.select('#especialidad', value3);
      await page.waitForTimeout(500)

      await page.select('[id=anio]', cod_aniof);
      await page.waitForTimeout(100)
      await page.type('[id=numeroExpediente]', cod_expedientef, {
        delay: 10
      });
    }



    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 558,
            'y': 618,
            'width': 110,
            'height': 44
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.contrast(.999).posterize(500).opacity(.9999).greyscale().contrast(1).posterize(2).invert().write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);

              await page.type('[id=codigoCaptcha]', text.split(expresionRegular).toString().trim(), {
                delay: 5
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(2000)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]

                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {
                  var Resultado = text.split(expresionRegular).toString().trim();
                  console.log("Captcha no resuelto  ", Resultado)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForTimeout(800)
                  await page.waitForSelector("#divDetalles")
                  var Resultado = text.split(expresionRegular).toString().trim();
                  console.log("Captcha resuelto ", Resultado)

                  //Cantidad de cuadernos o procesos
                  let nCuadernos = (await page.$$('#divDetalles > div')).length;
                  var inner_html = ""

                  for (let i = 1; i <= nCuadernos; i++) {
                    await page.waitForTimeout(500)
                    let xCodigoExterno = await page.$eval('#divDetalles > div:nth-child(' + i + ') >  div.celdCentroD > div.divNroJuz > div:nth-child(1)', e => e.innerText.trim());
                    if (xCodigoExterno == CodigoExterno) {
                      await page.click('#divDetalles > div:nth-child(' + i + ') >  .celdCentro > form > button');

                      await page.waitForTimeout(3000)
                      inner_html = await page.$eval('#divCuerpo > div:nth-child(2) > div', e => e.innerHTML);

                      inner_html = inner_html.replace(/\n/g, "")
                      inner_html = inner_html.replace(/\t/g, "")
                      var regExp = ""
                      inner_html = inner_html.replace(/[\[\]\\\/]]/g, '"');


                      await page.waitForTimeout(500)
                      await page.click('#divCuerpo > div:nth-child(1) > a:nth-child(1) > img');
                    }
                  }
                  console.timeEnd('loop');
                  res.status(200).json({
                    html: inner_html.toString(),
                    result: true
                  });
                  await browser.close();
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                }

              } catch (error) {
                Rompedor();
                console.log(error)
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});



app.post('/test', cors(), (req, res) => {
  console.time('loop');
  const {
    CodigoExterno,
    cFecha
  } = req.body;
  console.log(CodigoExterno, cFecha)
  let expediente = CodigoExterno;
  let xcej = expediente.split("-");

  var cod_expediente = "";
  var cod_anio = "";
  var cod_incidente = "";
  var cod_distprov = "";
  var cod_organo = "";
  var cod_especialidad = "";
  var cod_instancia = "";

  var cod_distritof = "";
  var cod_instanciaf = "";
  var cod_especialidadf = "";
  var cod_aniof = "";
  var cod_expedientef = "";


  if (xcej.length == 7) {
    cod_expediente = xcej[0];
    cod_anio = xcej[1];
    cod_incidente = xcej[2];
    cod_distprov = xcej[3];
    cod_organo = xcej[4];
    cod_especialidad = xcej[5];
    cod_instancia = xcej[6];
  } else {
    cod_distritof = xcej[0];
    cod_instanciaf = xcej[1];
    cod_especialidadf = xcej[2];
    cod_aniof = xcej[3];
    cod_expedientef = xcej[4];
  }



  (async () => {

    const browser = await puppeteer.launch({
      args: [
        "--no-sandbox",
      ],
      headless: false,
    });
    const page = await browser.newPage();
    await page.setDefaultTimeout(95000);
    await page.setViewport({
      width: 1366,
      height: 768
    });
    await page.goto('https://cej.pj.gob.pe/cej/forms/busquedaform.html#tabs-2');

    if (xcej.length == 7) {
      await page.waitForSelector('#captcha_image');
      await page.click('#myTab > li:nth-child(2)')
      await page.waitForSelector('#divConsultar')
      await page.waitForTimeout(500)

      await page.type('[id=cod_expediente]', cod_expediente, {
        delay: 10
      });
      await page.type('[id=cod_anio]', cod_anio, {
        delay: 10
      });
      await page.type('[id=cod_incidente]', cod_incidente, {
        delay: 10
      });
      await page.type('[id=cod_distprov]', cod_distprov, {
        delay: 10
      });
      await page.type('[id=cod_organo]', cod_organo, {
        delay: 10
      });
      await page.type('[id=cod_especialidad]', cod_especialidad, {
        delay: 10
      });
      await page.type('[id=cod_instancia]', cod_instancia, {
        delay: 10
      });
    } else {

      const option1 = (await page.$x(
        '//*[@id = "distritoJudicial"]/option[text() = "' + cod_distritof + '"]'
      ))[0];
      const value1 = await (await option1.getProperty('value')).jsonValue();
      await page.select('#distritoJudicial', value1);
      await page.waitForTimeout(500)

      const option2 = (await page.$x(
        '//*[@id = "organoJurisdiccional"]/option[text() = "' + cod_instanciaf + '"]'
      ))[0];
      const value2 = await (await option2.getProperty('value')).jsonValue();
      await page.select('#organoJurisdiccional', value2);
      await page.waitForTimeout(500)

      const option3 = (await page.$x(
        '//*[@id = "especialidad"]/option[text() = "' + cod_especialidadf + '"]'
      ))[0];
      const value3 = await (await option3.getProperty('value')).jsonValue();
      await page.select('#especialidad', value3);
      await page.waitForTimeout(500)

      await page.select('[id=anio]', cod_aniof);
      await page.waitForTimeout(100)
      await page.type('[id=numeroExpediente]', cod_expedientef, {
        delay: 10
      });
    }



    function Rompedor() {
      (async () => {
        const element = await page.$('#divConsultar > div:nth-child(2) > div:nth-child(1) >#captcha_image');
        const nameImage = await element.screenshot({
          type: 'png',
          'clip': {
            'x': 545,
            'y': 617,
            'width': 100,
            'height': 38
          },
          encoding: "base64"
        });
        const buffer = Buffer.from(nameImage, "base64");
        const hoy = new Date()
        const fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + '-' + hoy.getHours() + '-' + hoy.getMinutes() + '-' + hoy.getSeconds();
        var file = __dirname + "/Captcha/Captcha" + fecha + ".png";
        Jimp.read(buffer, function (err, image) {
          if (err) throw err;
          image.background(0x0012faff).brightness(-.30).contrast(1).posterize(1).write(file, function (err, image) {
            if (err) throw err;
            (async () => {
              const {
                data: {
                  text
                }
              } = await Tesseract.recognize(file);

              await page.type('[id=codigoCaptcha]', text, {
                delay: 5
              });
              await page.click('#consultarExpedientes');

              try {
                await page.waitForTimeout(2000)
                const CaptchaPass = await page.$$eval('#codCaptchaError[style]', y => y.map(x => x.getAttribute('style')));
                const CaptchaError = await page.$$eval('#mensajeNoExisteExpedientes[style]', y => y.map(x => x.getAttribute('style')));
                const Pasante = CaptchaPass[0]
                const Pasante2 = CaptchaError[0]

                console.log("-------------------------------------------")
                if (Pasante2 == 'color: rgb(255, 0, 0);') {
                  await browser.close()
                  res.status(200).json({
                    DataCuadernos: [],
                    DataGneral: [],
                    DataMovimienntos: [],
                    result: true,
                    mensaje: "No se encontraron registros con los datos ingresados."
                  });
                  fs.unlinkSync(file)
                } else if (Pasante == '') {

                  console.log("Captcha no resuelto  ", text)
                  fs.unlinkSync(file)
                  Rompedor();
                } else {
                  await page.waitForTimeout(800)
                  await page.waitForSelector("#divDetalles")
                  console.log("Captcha resuelto ", text)


                  res.status(200).json({
                    result: true
                  });
                  await browser.close();
                  fs.unlinkSync(file)
                  console.log('Archivo generado  Eliminado')
                }

              } catch (error) {
                Rompedor();
                console.log(error)
              }
            })();
          });
        });
      })();
    }
    Rompedor();
  })();
});


app.post('/test2', cors(), async (req, res) => {
  (async () => {
    var fullUrl = req.protocol + '://' + req.get('host');
    ConexionDB(fullUrl)
    setTimeout(() => {
      connection.query("SELECT *FROM Maestros", (error, result) => {
        if (error) throw error;
        if (result.length > 0) {
          res.json(result);
        } else {
          res.send('success');
        }
      })
    }, 500);
  })();
});

async function ConexionDB(url) {
  connection.changeUser({ database: '_GPSLegal' })
  console.log(url)
  connection.query("call su_sp_ObtenerBaseDatos(?)", [url], (error, result) => {
    if (result[0][0].cSus_CadenaConexion == 'No existe suscriptor') {
    } else {
      connection.changeUser({ database: result[0][0].cSus_CadenaConexion }, function (err) {
        if (err) throw err;
        console.log('Base de datos Conectada: ', result[0][0].cSus_CadenaConexion);
      });
    }

  })
}

//#1zirobotz0