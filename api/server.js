const express = require("express");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/maestros.router.js")(app);
require("./routes/casos.router.js")(app);
const PORT = process.env.PORT || 9000;
app.listen(PORT, () => {
  console.log(`el server esta corriendo en el puerto: ${PORT}.`);
});
